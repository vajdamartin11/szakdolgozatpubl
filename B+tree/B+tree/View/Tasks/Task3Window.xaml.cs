﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace B_tree.View.Tasks
{
    /// <summary>
    /// Interaction logic for Task3Window.xaml
    /// </summary>
    public partial class Task3Window : Window
    {
        public Task3Window()
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }
    }
}
