﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace B_tree.View.Examples
{
    /// <summary>
    /// Interaction logic for Example1Window.xaml
    /// </summary>
    public partial class Example1Window : Window
    {
        public Example1Window()
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }
    }
}
