﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace B_tree.View
{
    /// <summary>
    /// Interaction logic for ExamplesWindow.xaml
    /// </summary>
    public partial class ExamplesWindow : Window
    {
        public ExamplesWindow()
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }
    }
}
