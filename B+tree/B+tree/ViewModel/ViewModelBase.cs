﻿using B_tree.Persistence;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace B_tree.ViewModel
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        #region Private data members
        private Int32 _order; //Fokszám
        private Int32 _nodeWidth; // 1 node szélesség igénye
        private Double _pointerWidth; // 1 mutató celle szélessége
        private Int32 _leafCount; //levelek száma
        private Node _root; // A megjelítendő gyökér
        private Int32 _maxHeight; //teljes magasság
        private Int32 _cellWidth; // cella szélessége
        private Int32 _pixelsBetween2NodesY; // 2 node közötti magasság eltérés
        private Int32 _leafIndex; // Levél kirajzolásához

        #endregion

        #region Properties

        /// <summary>
        /// Fákat megjelenítő felület magasságának lekérdezése, vagy beállítása.
        /// </summary>
        public Int32 HeightCanvas { get; set; }

        /// <summary>
        /// Fákat megjelenítő felület szélességének lekérdezése,vagy beállítása.
        /// </summary>
        public Int32 WidthCanvas { get; set; }

        /// <summary>
        /// Kulcsok gyűjtemény lekérdezése.
        /// </summary>
        public ObservableCollection<PlanKey> Keys { get; set; }

        /// <summary>
        /// Nyilak gyűjtemény lekérdezése.
        /// </summary>
        public ObservableCollection<PlanArrow> Arrows { get; set; }

        /// <summary>
        /// Mutató gyűjtemény lekérdezése.
        /// </summary>
        public ObservableCollection<PlanPointer> Pointers { get; set; }

        /// <summary>
        /// Előző állapot kulcs gyűjtemény lekérdezése.
        /// </summary>
        public ObservableCollection<PlanKey> KeysSecondCanvas { get; set; }

        /// <summary>
        /// Előző állapot nyilak gyűjtemény lekérdezése.
        /// </summary>
        public ObservableCollection<PlanArrow> ArrowsSecondCanvas { get; set; }

        /// <summary>
        /// Előző állapot mutató gyűjtemény lekérdezése.
        /// </summary>
        public ObservableCollection<PlanPointer> PointersSecondCanvas { get; set; }

        #endregion
        /// <summary>
        /// Nézetmodell ősosztály példányosítása.
        /// </summary>
        protected ViewModelBase() { }

        /// <summary>
        /// Tulajdonság változásának eseménye.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Tulajdonság változása ellenőrzéssel.
        /// </summary>
        /// <param name="propertyName">Tulajdonság neve.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] String propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        /// <summary>
        /// Ablakok alapbeállításai.
        /// </summary>
        protected void SetupCanvas(Int32 order)
        {
            _order = order;
            Keys = new ObservableCollection<PlanKey>();
            Pointers = new ObservableCollection<PlanPointer>();
            Arrows = new ObservableCollection<PlanArrow>();
            KeysSecondCanvas = new ObservableCollection<PlanKey>();
            PointersSecondCanvas = new ObservableCollection<PlanPointer>();
            ArrowsSecondCanvas = new ObservableCollection<PlanArrow>();
            WidthCanvas = 900;
            HeightCanvas = 600;

            _nodeWidth = _order * 30;
            _pixelsBetween2NodesY = 100;
            _cellWidth = 30;
            _pointerWidth = (double)(_nodeWidth - 30) / (double)_order;
        }

        /// <summary>
        /// Megjelenítés frissítése.
        /// </summary>
        protected void Update(Node r, Int32 h)
        {
            Int32 x = WidthCanvas / 2 - _nodeWidth / 2;
            _leafIndex = 0;
            _root = r;
            _maxHeight = h;
            Arrows.Clear();
            Keys.Clear();
            Pointers.Clear();
            _leafCount = 0;
            CountLeaf(_root);
            SetCanvasSize();
            List<Int32> xList = new List<Int32>();
            if (!_root.IsLeaf)
            {
                foreach (Node helpN in _root.Children)
                    xList.Add(UpdateNode(helpN));
                if (xList.Count % 2 == 0)
                {
                    Int32 x1 = xList[xList.Count / 2];
                    Int32 x2 = xList[(xList.Count / 2) - 1];
                    x = (x1 + x2) / 2;
                }
                else
                {
                    x = xList[xList.Count / 2];
                }
            }
            NodePlanKeys(_root, x);
            NodePlanPointers(_root, x);
            NodePlanArrowsFrom(_root, x);
        }

        /// <summary>
        /// Az előző állapot frissítése.
        /// </summary>
        protected void UpdateSecondCanvas()
        {
            KeysSecondCanvas.Clear();
            ArrowsSecondCanvas.Clear();
            PointersSecondCanvas.Clear();
            foreach (PlanKey p in Keys)
            {
                KeysSecondCanvas.Add(p);

            }
            foreach (PlanArrow p in Arrows)
            {
                ArrowsSecondCanvas.Add(p);
            }
            foreach (PlanPointer p in Pointers)
            {
                PointersSecondCanvas.Add(p);
            }
            OnPropertyChanged("KeysSecondCanvas");
            OnPropertyChanged("ArrowsSecondCanvas");
            OnPropertyChanged("PointersSecondCanvas");
        }

        #region Helper methods
        /// <summary>
        /// Rekurzió, az Update hívja meg, node-k megjelenítésének feldolgozásáért felel.
        /// </summary>
        private Int32 UpdateNode(Node n)
        {

            List<Int32> xList = new List<Int32>();
            Int32 x = 0;
            if (n.IsLeaf)
            {
                x = UpdateLeaf(n);
            }
            else
            {
                if (n.Children[0].IsLeaf)
                {
                    foreach (Node helpN in n.Children)
                        xList.Add(UpdateLeaf(helpN));
                    if (xList.Count % 2 == 0)
                    {
                        Int32 x1 = xList[xList.Count / 2];
                        Int32 x2 = xList[(xList.Count / 2) - 1];
                        x = (x1 + x2) / 2;
                    }
                    else
                    {
                        x = xList[xList.Count / 2];
                    }
                    NodePlanPointers(n, x);
                    NodePlanKeys(n, x);
                    NodePlanArrowsTo(n, x);
                    NodePlanArrowsFrom(n, x);
                    return x;
                }


                else
                {
                    foreach (Node helpN in n.Children)
                        xList.Add(UpdateNode(helpN));
                    if (xList.Count % 2 == 0)
                    {
                        Int32 x1 = xList[xList.Count / 2];
                        Int32 x2 = xList[(xList.Count / 2) - 1];
                        x = (x1 + x2) / 2;
                    }
                    else
                    {
                        x = xList[xList.Count / 2];
                    }
                    NodePlanPointers(n, x);
                    NodePlanKeys(n, x);
                    NodePlanArrowsTo(n, x);
                    NodePlanArrowsFrom(n, x);
                    return x;
                }
            }
            return x;

        }

        /// <summary>
        /// Levelek megjelenítéséért felel.
        /// </summary>
        private Int32 UpdateLeaf(Node n)
        {
            Int32 x = WidthCanvas / (_leafCount + 1);
            x = x / _leafCount * _leafIndex + x * _leafIndex;
            NodePlanPointers(n, x);
            NodePlanKeys(n, x);
            NodePlanArrowsTo(n, x);
            _leafIndex++;
            return x;
        }

        /// <summary>
        /// A megjelenítési felületek méretei változhatnak ha túl sok node van, itt változik ha szükséges
        /// </summary>
        private void SetCanvasSize()
        {
            Int32 spaceBetweenNodesWidth = 10;
            Int32 spaceBetweenNodesHeight = 155;
            Int32 RequiredWidth = _leafCount * _nodeWidth + _leafCount * spaceBetweenNodesWidth + 30;
            Int32 RequiredHeight = _maxHeight * spaceBetweenNodesHeight;
            if (RequiredWidth > WidthCanvas)
            {
                WidthCanvas = RequiredWidth;
                OnPropertyChanged("WidthCanvas");
            }
            if (RequiredHeight > HeightCanvas)
            {
                HeightCanvas = RequiredHeight;
                OnPropertyChanged("HeightCanvas");
            }
        }

        /// <summary>
        /// A node-k mutatóinak megjelenítéséért felel
        /// </summary>
        private void NodePlanPointers(Node n, Int32 x)
        {
            for (Int32 j = 0; j < _order; j++)
            {
                Double helpDouble = _cellWidth - _pointerWidth;

                String outline = "Black";
                if (n.ColoredIndex == j)
                    outline = "Red";
                if (n.ColoredIndex == -3)
                {
                    outline = "LightSalmon";
                }
                if (j < n.Children.Count || (n.IsLeaf && n.Keys.Count > j))
                    Pointers.Add(new PlanPointer
                    {
                        Transform = _pointerWidth / 4 + 1,
                        PointerWidth = _pointerWidth,
                        Filled = true,
                        X = x + (j + 1) * _pointerWidth + helpDouble,
                        Y = (n.Height - 1) * _pixelsBetween2NodesY + 40 + (n.Height + 1) * 40,
                        Color = outline,
                        Animation = n.Animation
                    });
                else
                    Pointers.Add(new PlanPointer
                    {
                        Transform = _pointerWidth / 4 + 1,
                        PointerWidth = _pointerWidth,
                        Filled = false,
                        X = x + (j + 1) * _pointerWidth + helpDouble,
                        Y = (n.Height - 1) * _pixelsBetween2NodesY + 40 + (n.Height + 1) * 40,
                        Color = outline,
                        Animation = n.Animation
                    });
            }
        }


        /// <summary>
        /// A node-k kulcsainak megjelenítéséért felel
        /// </summary>
        private void NodePlanKeys(Node n, Int32 x)
        {
            for (Int32 j = 0; j < _order; j++)
            {
                String outline = "Black";
                if (n.ColoredIndex == j && n.IsLeaf)
                    outline = "Red";
                if (n.ColoredIndex == -3)
                {
                    outline = "LightSalmon";
                }
                if (j < n.Keys.Count)
                    Keys.Add(new PlanKey
                    {
                        KeyOnWindow = n.Keys[j].ToString(),
                        X = x + (j + 1) * _cellWidth,
                        Y = (n.Height - 1) * _pixelsBetween2NodesY + 20 + (n.Height + 1) * 40,
                        Color = outline,
                        Animation = n.Animation
                    });
                else if (j + 1 != _order)
                    Keys.Add(new PlanKey
                    {
                        KeyOnWindow = "",
                        X = x + (j + 1) * _cellWidth,
                        Y = (n.Height - 1) * _pixelsBetween2NodesY + 20 + (n.Height + 1) * 40,
                        Color = outline,
                        Animation = n.Animation
                    });
                else if (_order == j + 1 && n.Keys.Count == j + 1)
                    Keys.Add(new PlanKey
                    {
                        KeyOnWindow = n.Keys[j].ToString(),
                        X = x / _leafCount * _leafIndex + x * (_leafIndex) + (j + 1) * _cellWidth,
                        Y = (n.Height - 1) * _pixelsBetween2NodesY + 20 + (n.Height + 1) * 40,
                        Color = outline,
                        Animation = n.Animation
                    });
            }
        }

        /// <summary>
        /// A node-k összekötéseinek megjelenítéséért felel, ahová mutat a nyíl
        /// </summary>
        private void NodePlanArrowsTo(Node n, Int32 x)
        {
            Arrows.Add(new PlanArrow { Parent = n.Parent, YTo = (n.Height - 1) * _pixelsBetween2NodesY + 20 + (n.Height + 1) * 40, XTo = x + _nodeWidth / 2 + 15 });
        }
        /// <summary>
        /// A node-k összekötéseinek megjelenítéséért felel, ahonnan mutat a nyíl
        /// </summary>
        private void NodePlanArrowsFrom(Node n, Int32 x)
        {
            Int32 j = 0;
            foreach (PlanArrow a in Arrows)
            {
                if (a.Parent == n)
                {
                    a.XFrom = x + _pointerWidth / 2 + j * _pointerWidth + 30;
                    a.YFrom = (n.Height - 1) * _pixelsBetween2NodesY + 50 + (n.Height + 1) * 40;
                    j++;
                }
            }

        }

        /// <summary>
        /// A levelek számának visszaadása.
        /// </summary>
        private void CountLeaf(Node n)
        {
            if (n.IsLeaf)
            {
                _leafCount++;
                return;
            }
            if (n.Children.Count > 0)
            {
                foreach (Node node in n.Children)
                {
                    CountLeaf(node);
                }
            }

        }
        #endregion
    }
}
