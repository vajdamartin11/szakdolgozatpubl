﻿using System;

namespace B_tree.ViewModel
{
    public class OrderViewModel : ViewModelBase
    {

        //Minimális fokszám
        private readonly Int32 MIN_ORDER = 4;
        //Maximális fokszám
        private readonly Int32 MAX_ORDER = 10;

        #region Properties
        /// <summary>
        /// Fokszám beállítása, vagy lekérdezése.
        /// </summary>
        public Int32 Order { get; set; }

        /// <summary>
        /// A fokszám ablak esetén, oké parancs lekérdezése.
        /// </summary>
        public DelegateCommand OKCommand { get; private set; }

        /// <summary>
        /// A fokszám ablak esetén, vissza parancs lekérdezése.
        /// </summary>
        public DelegateCommand CancelCommand { get; private set; }
        #endregion

        #region Events
        /// <summary>
        /// Oké eseménye.
        /// </summary>
        public event EventHandler<Boolean> OKClicked;
        /// <summary>
        /// Vissza eseménye.
        /// </summary>
        public event EventHandler CancelClicked;
        #endregion

        #region Constructors
        /// <summary>
        /// OrderViewModel példányosítása
        /// </summary>
        public OrderViewModel()
        {
            OKCommand = new DelegateCommand(o => OnOKClicked());
            CancelCommand = new DelegateCommand(o => OnCancelClicked());
        }
        #endregion

        #region Event methods

        /// <summary>
        /// Oké eseménykiváltása.
        /// </summary>
        private void OnOKClicked()
        {
            Boolean numberIsOK = Order >= MIN_ORDER && Order <= MAX_ORDER;

            if (OKClicked != null)
                OKClicked(this, numberIsOK);
        }

        /// <summary>
		/// Vissza eseménykiváltása.
		/// </summary>
        private void OnCancelClicked()
        {
            if (CancelClicked != null)
                CancelClicked(this, EventArgs.Empty);
        }
        #endregion
    }
}
