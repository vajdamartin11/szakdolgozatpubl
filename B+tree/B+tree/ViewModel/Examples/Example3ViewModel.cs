﻿using B_tree.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Threading;

namespace B_tree.ViewModel.Examples
{
    public class Example3ViewModel : ViewModelBase
    {
        #region Private data members
        private BpTree _model; // Fa amin példát mutatjuk
        private Int32 _order; // fokszám
        private Int32 _speedFactor; //sebesség
        private Boolean _skipFirstAction; // Előző állapot első akciót kihagyása
        private DispatcherTimer _animationTimer; //Műveletek időzítése
        private List<Model.Action> _actionList; // műveletsor amit elvégzünk
        private List<Int32> _keyList; // kulcsok amikkel a műveleteket végezzük
        private List<String> _commentList; // A megjegyzések listái
        private Int32 _loadIndex; // Aktuális példa indexe
        private List<String> _operationsList; //Eddig elvégzett műveletek kulccsal
        private Int32 _operationIndex; //Hányadik műveletet végeztük

        private Model.Action _currentAction; // aktuális akció
        private Boolean _animationOver; // vége van az adott műveletnek

        private Int32 _key; // Kulcs amivel épp a műveletet végezzük
        #endregion

        #region Properties
        /// <summary>
        /// Eddig elvégzett műveletek kulccsal lekérdezése, vagy beállítása.
        /// </summary>
        public BindingList<String> OperationsList { get; set; }

        /// <summary>
        /// A megjegyzésben található szöveg lekérdezése vagy beállítása.
        /// </summary>
        public String Comment { get; set; }

        /// <summary>
        /// Az előrelépés gomb aktívságának lekérdezése vagy beállítása.
        /// </summary>
        public Boolean IsEnabledStepNext { get; set; }

        /// <summary>
        /// A visszalépés gomb aktívságának lekérdezése vagy beállítása.
        /// </summary>
        public Boolean IsEnabledStepBack { get; set; }

        /// <summary>
        /// Az adott példa ablak esetén, előrelépés a példában parancs lekérdezése.
        /// </summary>
        public DelegateCommand NextStepCommand { get; private set; }

        /// <summary>
        /// Az adott példa ablak esetén, visszalépés a példában parancs lekérdezése.
        /// </summary>
        public DelegateCommand BackStepCommand { get; private set; }

        /// <summary>
        /// Az adott példa ablak esetén, vissza a példákra parancs lekérdezése.
        /// </summary>
        public DelegateCommand ExamplesCommand { get; private set; }

        /// <summary>
        /// Az adott példa ablak esetén, vissza a menübe parancs lekérdezése.
        /// </summary>
        public DelegateCommand BackCommand { get; private set; }
        #endregion

        #region Events

        /// <summary>
        /// Vissza a példákhoz eseménye.
        /// </summary>
        public EventHandler Example3ToExamples;

        /// <summary>
        /// Vissza a menübe eseménye.
        /// </summary>
        public EventHandler Example3ToMain;

        /// <summary>
        /// Vége a példának eseménye.
        /// </summary>
        public EventHandler Example3End;
        #endregion

        #region Constructors

        /// <summary>
        /// Example3ViewModel példányosítása
        /// </summary>
        public Example3ViewModel()
        {
            _loadIndex = 0;
            _order = 5;
            _model = new BpTree(_order);
            _animationOver = true;

            SetupCanvas(_order);
            Update(_model.Root, _model.MaxHeight);
            SetExample();

            IsEnabledStepNext = true;
            IsEnabledStepBack = false;
            OnPropertyChanged("IsEnabled");
            _speedFactor = 1200;

            _animationTimer = new DispatcherTimer();
            _animationTimer.Interval = TimeSpan.FromMilliseconds(_speedFactor);
            _animationTimer.Tick += DoAction;

            _operationIndex = 1;
            _operationsList = new List<String>();
            OperationsList = new BindingList<String>();

            NextStepCommand = new DelegateCommand(new Action<object>(o => OnNextStepClicked()));
            BackStepCommand = new DelegateCommand(new Action<object>(o => OnBackStepClicked()));
            ExamplesCommand = new DelegateCommand(new Action<object>(o => OnExample3ToExamples()));
            BackCommand = new DelegateCommand(new Action<object>(o => OnExample3ToMain()));

            Init();
        }
        #endregion
        #region Private methods

        /// <summary>
        /// Felépítjük a fát
        /// </summary>
        private void Init()
        {
            for (Int32 i = 0; i < 4; i++)
            {
                _currentAction = Model.Action.Insert;
                _key = 4 * (i + 1);
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }

                UpdateSecondCanvas();
                Update(_model.Root, _model.MaxHeight);
            }
        }

        /// <summary>
        /// A példa beállítása
        /// </summary>
        private void SetExample()
        {
            _keyList = new List<Int32>() { 20, 24, 28, 32, 36, 40, 44, 48, 52, 44, 28, 32, 36 };
            _actionList = new List<Model.Action>();
            _commentList = new List<String>();
            for (Int32 i = 0; i < 9; i++)
            {
                _actionList.Add(Model.Action.Insert);
            }
            for (Int32 i = 0; i < 4; i++)
            {
                _actionList.Add(Model.Action.Delete);
            }
            SetCommentList();
        }

        /// <summary>
        /// Megjegyzések beállítása
        /// </summary>
        private void SetCommentList()
        {
            _commentList.Add("Ebben a példában 5-ös fokszámra nézünk példát, mely kicsit így eltérő lesz az eddigiektől. " +
                "Beszúrjuk a 20-at, mivel több, mint fokszám-1 kulcs van (4, 8 | 12, 16, 20), hasad a levél. A (12, 16, 20) minimuma fog felmenni az új gyökérbe hasító kulcsnak.");
            _commentList.Add("Beszúrjuk a 24-et, mivel nem több, mint fokszám-1 kulcs van (12, 16, 20, 24), nincs teendőnk.");
            _commentList.Add("Beszúrjuk a 28-at, mivel több, mint fokszám-1 kulcs van (12, 16 | 20, 24, 28), hasad a levél. A (20, 24, 28) minimuma fog felmenni hasító kulcsnak.");
            _commentList.Add("Beszúrjuk a 32-öt, mivel nem több, mint fokszám-1 kulcs van (20, 24, 28, 32), nincs teendőnk.");
            _commentList.Add("Beszúrjuk a 36-ot, mivel több, mint fokszám-1 kulcs van (20, 24 | 28, 32, 36), hasad a levél. A (28, 32, 36) minimuma fog felmenni hasító kulcsnak.");
            _commentList.Add("Beszúrjuk a 40-et, mivel nem több, mint fokszám-1 kulcs van (28, 32, 36, 40), nincs teendőnk.");
            _commentList.Add("Beszúrjuk a 44-et, mivel több, mint fokszám-1 kulcs van (28, 32 | 36, 40, 44), hasad a levél. A (36, 40, 44) minimuma fog felmenni hasító kulcsnak.");
            _commentList.Add("Beszúrjuk a 48-at, mivel nem több, mint fokszám-1 kulcs van (36, 40, 44, 48), nincs teendőnk.");
            _commentList.Add("Beszúrjuk a 52-öt, mivel több, mint fokszám-1 kulcs van (36, 40 | 44, 48, 52), hasad a levél. A (44, 48, 52) minimuma fog felmenni hasítókulcsnak." +
                " Mivel a szülőben fokszám-nál több gyerek található az is hasad ( 12, 20 | 28, 36, 44). A (28, 36, 44) minimuma fog felmenni az új gyökérbe hasító kulcsnak, töröljük " +
                "a szülőből és csak a nagyszülőben lesz benne. A fa magassága nő");
            _commentList.Add("Töröljük a 44-et, mivel nem kevesebb, mint fokszám/2 (alsó egész) kulcs van ( - ,28, 32), nincs teendőnk.");
            _commentList.Add("Töröljük a 28-at, mivel kevesebb, mint fokszám/2 (alsó egész) kulcs van (28, 32) , testvértől nem tudunk kulcsot átvenni." +
                "Ezért egyesül egy testvér csúccsal (36,40). Az ezeket összekötő hasító kulcsot töröljük a szülőből. Nincs teendőnk van legalább fokszám/2 (alsó egész) gyerek a csúcsban.");
            _commentList.Add("Töröljük a 32-öt, mivel nem kevesebb, mint fokszám/2 (alsó egész) kulcs van ( -, 36, 40), nincs teendőnk.");
            _commentList.Add("Töröljük a 36-ot, mivel kevesebb, mint fokszám/2 (alsó egész) kulcs van ( - ,40) , testvértől nem tudunk kulcsot átvenni, egyesül a két testvér levél (40,48,52)." +
                "A szülőben törlődik a hozzájuk tartozó hasító kulcs (44). Nem lesz elég gyereke, de a testvér csúcstól tud átvenni egy gyereket (20,24). Nagyszülőből lejön a hasító kulcs(28), testvértől felmegy a (20) nagyszülőbe.");
        }

        /// <summary>
        /// léptetjük a példát
        /// </summary>
        private void Step()
        {
            if (_animationOver)
            {
                if (_loadIndex == _keyList.Count)
                {
                    if (Example3End != null)
                        Example3End(this, EventArgs.Empty);
                    IsEnabledStepNext = false;
                    OnPropertyChanged("IsEnabledStepNext");
                    IsEnabledStepBack = true;
                    OnPropertyChanged("IsEnabledStepBack");
                }

                else
                {
                    _currentAction = _actionList[_loadIndex];
                    _key = _keyList[_loadIndex];
                    Comment = _commentList[_loadIndex];
                    SetOperationsList();
                    OnPropertyChanged("Comment");
                    _animationTimer.Start();
                }
            }
        }

        /// <summary>
        /// A műveleteket hozzáadja a listába
        /// </summary>
        private void SetOperationsList()
        {
            _operationsList.Reverse();
            if (_currentAction == Model.Action.Insert)
                _operationsList.Add(_operationIndex++.ToString() + ". Beszúrtuk: " + _key);
            if (_currentAction == Model.Action.Delete)
                _operationsList.Add(_operationIndex++.ToString() + ". Töröltük: " + _key);
            if (_currentAction == Model.Action.Search)
                _operationsList.Add(_operationIndex++.ToString() + ". Kerestük: " + _key);
            _operationsList.Reverse();

            OperationsList = new BindingList<String>(_operationsList);
            OnPropertyChanged("OperationsList");
        }

        /// <summary>
        /// Művelet elvégzése
        /// </summary>
        private void DoAction(object sender, EventArgs e)
        {
            _animationOver = false;
            Boolean ThereAction = _model.ActionController(_currentAction, _key);

            if (_currentAction != Model.Action.DoAction)
                _currentAction = Model.Action.DoAction;

            if (!_skipFirstAction)
                UpdateSecondCanvas();

            _skipFirstAction = true;

            if (!ThereAction)
            {
                _animationOver = true;
                _skipFirstAction = false;
                IsEnabledStepNext = true;
                _loadIndex++;
                if (_loadIndex > 0)
                    IsEnabledStepBack = true;
                OnPropertyChanged("IsEnabledStepNext");
                OnPropertyChanged("IsEnabledStepBack");
                _animationTimer.Stop();
            }

            Update(_model.Root, _model.MaxHeight);
        }
        #endregion
        #region Event methods
        /// <summary>
		/// A példában előrelépés.
		/// </summary>
        private void OnNextStepClicked()
        {
            IsEnabledStepNext = false;
            IsEnabledStepBack = false;
            OnPropertyChanged("IsEnabledStepNext");
            OnPropertyChanged("IsEnabledStepBack");
            Step();
        }
        /// <summary>
		/// A példában visszalépés.
		/// </summary>
        private void OnBackStepClicked()
        {
            IsEnabledStepNext = false;
            IsEnabledStepBack = false;
            OnPropertyChanged("IsEnabledStepNext");
            OnPropertyChanged("IsEnabledStepBack");

            _operationIndex = 1;
            _operationsList = new List<String>();
            OperationsList = new BindingList<String>();
            OnPropertyChanged("OperationsList");
            if (_loadIndex == 1)
            {
                _loadIndex--;
                _model = new BpTree(_order);
                _animationOver = true;
                Init();
                Comment = "";
                OnPropertyChanged("");
                Update(_model.Root, _model.MaxHeight);
                UpdateSecondCanvas();
            }
            else if (_loadIndex - 2 > -1)
            {
                _model = new BpTree(_order);
                _loadIndex--;
                Init();
                Comment = _commentList[_loadIndex - 1];
                OnPropertyChanged("Comment");
                if (_loadIndex == 1)
                {
                    Update(_model.Root, _model.MaxHeight);
                    UpdateSecondCanvas();
                }

                for (Int32 i = 0; i < _loadIndex; i++)
                {
                    _currentAction = _actionList[i];
                    _key = _keyList[i];
                    SetOperationsList();
                    while (_model.ActionController(_currentAction, _key))
                    {
                        if (_currentAction != Model.Action.DoAction)
                            _currentAction = Model.Action.DoAction;

                    }
                    if (_loadIndex == 1)
                        Update(_model.Root, _model.MaxHeight);
                    else
                    {
                        UpdateSecondCanvas();
                        Update(_model.Root, _model.MaxHeight);
                    }
                }
            }

            IsEnabledStepNext = true;
            if (_loadIndex > 0)
                IsEnabledStepBack = true;
            OnPropertyChanged("IsEnabledStepNext");
            OnPropertyChanged("IsEnabledStepBack");
        }

        /// <summary>
		/// Vissza a példákhoz eseménykiváltása.
		/// </summary>
        private void OnExample3ToExamples()
        {
            if (Example3ToExamples != null)
                Example3ToExamples(this, EventArgs.Empty);
        }
        /// <summary>
		/// Vissza a menübe eseménykiváltása.
		/// </summary>
        private void OnExample3ToMain()
        {
            if (Example3ToMain != null)
                Example3ToMain(this, EventArgs.Empty);
        }
        #endregion
        
        
    }
}
