﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Threading;
using B_tree.Model;

namespace B_tree.ViewModel.Examples
{
    public class Example1ViewModel : ViewModelBase
    {
        #region Private data members
        private BpTree _model; // Fa amin példát mutatjuk
        private Int32 _order; // fokszám
        private Int32 _speedFactor; //sebesség
        private Boolean _skipFirstAction; // Előző állapot első akciót kihagyása
        private DispatcherTimer _animationTimer; //Műveletek időzítése
        private List<Model.Action> _actionList; // műveletsor amit elvégzünk
        private List<Int32> _keyList; // kulcsok amikkel a műveleteket végezzük
        private List<String> _commentList; // A megjegyzések listái
        private Int32 _loadIndex; // Aktuális példa indexe
        private List<String> _operationsList; //Eddig elvégzett műveletek kulccsal
        private Int32 _operationIndex; //Hányadik műveletet végeztük

        private Model.Action _currentAction; // aktuális akció
        private Boolean _animationOver; // vége van az adott műveletnek

        private Int32 _key; // Kulcs amivel épp a műveletet végezzük
        #endregion

        #region Properties
        /// <summary>
        /// Eddig elvégzett műveletek kulccsal lekérdezése, vagy beállítása.
        /// </summary>
        public BindingList<String> OperationsList { get; set; }

        /// <summary>
        /// A megjegyzésben található szöveg lekérdezése, vagy beállítása.
        /// </summary>
        public String Comment { get; set; }

        /// <summary>
        /// Az előrelépés gomb aktívságának lekérdezése, vagy beállítása.
        /// </summary>
        public Boolean IsEnabledStepNext { get; set; }

        /// <summary>
        /// A visszalépés gomb aktívságának lekérdezése, vagy beállítása.
        /// </summary>
        public Boolean IsEnabledStepBack { get; set; }

        /// <summary>
        /// Az adott példa ablak esetén, előrelépés a példában parancs lekérdezése.
        /// </summary>
        public DelegateCommand NextStepCommand { get; private set; }

        /// <summary>
        /// Az adott példa ablak esetén, visszalépés a példában parancs lekérdezése.
        /// </summary>
        public DelegateCommand BackStepCommand { get; private set; }

        /// <summary>
        /// Az adott példa ablak esetén, vissza a példákra parancs lekérdezése.
        /// </summary>
        public DelegateCommand ExamplesCommand { get; private set; }

        /// <summary>
        /// Az adott példa ablak esetén, vissza a menübe parancs lekérdezése.
        /// </summary>
        public DelegateCommand BackCommand { get; private set; }
        #endregion

        #region Events

        /// <summary>
        /// Vissza a példákra eseménye.
        /// </summary>
        public EventHandler Example1ToExamples;

        /// <summary>
        /// Vissza a menübe eseménye.
        /// </summary>
        public EventHandler Example1ToMain;

        /// <summary>
        /// A példának vége eseménye.
        /// </summary>
        public EventHandler Example1End;
        #endregion

        #region Constructors

        /// <summary>
        /// Example1ViewModel példányosítása
        /// </summary>
        public Example1ViewModel()
        {
            _loadIndex = 0;
            _order = 4;
            _model = new BpTree(_order);
            _animationOver = true;

            SetupCanvas(_order);
            Update(_model.Root, _model.MaxHeight); ;
            SetExample();

            IsEnabledStepNext = true;
            IsEnabledStepBack = false;
            OnPropertyChanged("IsEnabled");
            _speedFactor = 1200;

            _animationTimer = new DispatcherTimer();
            _animationTimer.Interval= TimeSpan.FromMilliseconds(_speedFactor);
            _animationTimer.Tick += DoAction;

            _operationIndex = 1;
            _operationsList = new List<String>();
            OperationsList = new BindingList<String>();

            NextStepCommand = new DelegateCommand(new Action<object>(o => OnNextStepClicked()));
            BackStepCommand = new DelegateCommand(new Action<object>(o => OnBackStepClicked()));
            ExamplesCommand = new DelegateCommand(new Action<object>(o => OnExample1ToExamples()));
            BackCommand = new DelegateCommand(new Action<object>(o => OnExample1ToMain()));
        }
        #endregion

        #region Private methods

        /// <summary>
        /// A példa beállítása
        /// </summary>
        private void SetExample()
        {
            _keyList = new List<Int32>() { 1,4,16,25,9,20,13,15,10,11,12,13,15,1 };
            _actionList = new List<Model.Action>();
            _commentList = new List<String>();
            for(Int32 i = 0; i < 11; i++)
            {
                _actionList.Add(Model.Action.Insert);
            }
            for (Int32 i = 0; i < 3; i++)
            {
                _actionList.Add(Model.Action.Delete);
            }
            SetCommentList();
        }

        /// <summary>
        /// Megjegyzések beállítása
        /// </summary>
        private void SetCommentList()
        {
            _commentList.Add("Kezdetben az egyetlen levél az maga a gyökér. Ebben az egy esetben engedjük meg,hogy a levelen fokszám/2 -nél (alsó egész) kevesebb kulcs szerepeljen." +
                " Beszúrjuk az 1-et, mivel nem több, mint fokszám-1 kulcs van, nincs más teendőnk.");
            _commentList.Add("Beszúrjuk a 4-et, mivel nem több, mint fokszám-1 kulcs van, nincs más teendőnk.");
            _commentList.Add("Beszúrjuk a 16-ot, mivel nem több, mint fokszám-1 kulcs van, nincs más teendőnk.");
            _commentList.Add("Beszúrjuk a 25-öt, mivel több, mint fokszám-1 kulcs van (1, 4 | 16, 25), hasad a levél. A (16, 25) minimuma fog felmenni az új gyökérbe hasító kulcsnak. A fa magassága nő.");
            _commentList.Add("Beszúrjuk a 9-et, mivel nem több, mint fokszám-1 kulcs van (1, 4, 9), nincs más teendőnk.");
            _commentList.Add("Beszúrjuk a 20-at, mivel nem több, mint fokszám-1 kulcs van (16, 20, 25), nincs más teendőnk.");
            _commentList.Add("Beszúrjuk a 13-at, mivel több, mint fokszám-1 kulcs van (1, 4 | 9, 13), hasad a levél. A (9, 13) minimuma fog felmenni hasító kulcsnak.");
            _commentList.Add("Beszúrjuk a 15-öt, mivel nem több, mint fokszám-1 kulcs van ( 9, 13, 15 ), nincs más teendőnk.");
            _commentList.Add("Beszúrjuk a 10-et, mivel több, mint fokszám-1 kulcs van ( 9, 10 | 13, 15 ), hasad a levél. A (13, 15) minimuma fog felmenni hasító kulcsnak.");
            _commentList.Add("Beszúrjuk a 11-et, mivel nem több, mint fokszám-1 kulcs van ( 9, 10, 11 ), nincs más teendőnk.");
            _commentList.Add("Beszúrjuk a 12-öt, mivel több, mint fokszám-1 kulcs van ( 9, 10 | 11, 12 ), hasad a levél. A (11, 12) minimuma fog felmenni hasító kulcsnak." +
                "Mivel a szülőben fokszámnál több gyerek található az is hasad ( 9, 11 | 13, 16 ). A (13, 16) minimuma fog felmenni az új gyökérbe hasító kulcsnak. A fa magassága nő");
            _commentList.Add("Töröljük a 13-at, mivel kevesebb, mint fokszám/2 (alsó egész) kulcs van ( - ,15) , ezért megnézzük, hátha a testvérének minimumnál több kulcsa van." +
                " Mivel ez igaz átvesszük a legkisebb kulcsot (16) és a testvér első kulcsa(20) fog felmenni új hasító kulcsnak az előző helyett.");
            _commentList.Add("Töröljük a 15-öt, mivel kevesebb, mint fokszám/2 (alsó egész) kulcs van ( - ,16), megpróbálunk a testvér csúcstól átvenni kulcsot, de most nem tudunk, ezért egyesül a két testvér levél." +
                " A szülő hasító kulcsa törlődik (20), és már csak 1 gyereke van, de a testvér csúcstól tud átvenni egyet (11,12). A nagyszülőtől lemegy a 13-as hasító kulcs, a 11 meg felmegy hasító kulcsnak a nagyszülőbe.");
            _commentList.Add("Töröljük az 1-et, mivel kevesebb, mint fokszám/2 (alsó egész) kulcs van ( - ,4), megpróbálunk a testvér csúcstól átvenni kulcsot, nem tudunk, ezért egyesül a két testvér levél." +
                " A szülő hasító kulcsa törlődik (9), és már csak 1 gyereke van, testvértől nem tud átvenni, ezért egyesül a két testvér csúcs. A nagyszülőben is törlődik a megfelelő hasítókulcs(11)." +
                "Mely egyben a gyökér, már csak 1 gyereke van, ez lesz az új gyökér. A fa magassága csökken.");
        }


        /// <summary>
        /// léptetjük a példát
        /// </summary>
        private void Step()
        {
            if (_animationOver)
            {
                if (_loadIndex == _keyList.Count)
                {
                    if (Example1End != null)
                        Example1End(this, EventArgs.Empty);
                    IsEnabledStepNext = false;
                    OnPropertyChanged("IsEnabledStepNext");
                    IsEnabledStepBack = true;
                    OnPropertyChanged("IsEnabledStepBack");
                }

                else
                {
                    _currentAction = _actionList[_loadIndex];
                    _key = _keyList[_loadIndex];
                    Comment = _commentList[_loadIndex];
                    SetOperationsList();
                    OnPropertyChanged("Comment");
                    _animationTimer.Start();
                }
            }
        }

        /// <summary>
        /// A műveleteket hozzáadja a listába
        /// </summary>
        private void SetOperationsList()
        {
            _operationsList.Reverse();
            if (_currentAction == Model.Action.Insert)
                _operationsList.Add(_operationIndex++.ToString() + ". Beszúrtuk: " + _key);
            if (_currentAction == Model.Action.Delete)
                _operationsList.Add(_operationIndex++.ToString() + ". Töröltük: " + _key);
            if (_currentAction == Model.Action.Search)
                _operationsList.Add(_operationIndex++.ToString() + ". Kerestük: " + _key);
            _operationsList.Reverse();

            OperationsList = new BindingList<String>(_operationsList);
            OnPropertyChanged("OperationsList");
        }

        /// <summary>
        /// Művelet elvégzése
        /// </summary>
        private void DoAction(object sender, EventArgs e)
        {
            _animationOver = false;
            Boolean ThereAction = _model.ActionController(_currentAction, _key);

            if (_currentAction != Model.Action.DoAction)
                _currentAction = Model.Action.DoAction;

            if (!_skipFirstAction)
                UpdateSecondCanvas();

            _skipFirstAction = true;

            if (!ThereAction)
            {
                _animationOver = true;
                _skipFirstAction = false;
                IsEnabledStepNext = true;
                _loadIndex++;
                if (_loadIndex > 0)
                    IsEnabledStepBack = true;
                OnPropertyChanged("IsEnabledStepNext");
                OnPropertyChanged("IsEnabledStepBack");
                _animationTimer.Stop();
                
            }

            Update(_model.Root, _model.MaxHeight); ;
        }
        #endregion
        #region Event methods

        /// <summary>
		/// A példában előrelépés.
		/// </summary>
        private void OnNextStepClicked()
        {
            IsEnabledStepNext = false;
            IsEnabledStepBack = false;
            OnPropertyChanged("IsEnabledStepNext");
            OnPropertyChanged("IsEnabledStepBack");
            Step();
        }

        /// <summary>
		/// A példában visszalépés.
		/// </summary>
        private void OnBackStepClicked()
        {
            IsEnabledStepNext = false;
            IsEnabledStepBack = false;
            OnPropertyChanged("IsEnabledStepNext");
            OnPropertyChanged("IsEnabledStepBack");

            _operationIndex = 1;
            _operationsList = new List<String>();
            OperationsList = new BindingList<String>();
            OnPropertyChanged("OperationsList");
            if (_loadIndex == 1)
            {
                _loadIndex--;
                _model = new BpTree(_order);
                _animationOver = true;
                Comment = "";
                OnPropertyChanged("");
                Update(_model.Root, _model.MaxHeight);
                UpdateSecondCanvas();
            }
            else if (_loadIndex -2 > -1)
            {
                _model = new BpTree(_order);
                _loadIndex--;
                Comment = _commentList[_loadIndex - 1];
                OnPropertyChanged("Comment");
                if(_loadIndex == 1)
                {
                    Update(_model.Root, _model.MaxHeight);
                    UpdateSecondCanvas();
                }
                    
                for (Int32 i = 0; i < _loadIndex;i++)
                {
                    _currentAction = _actionList[i];
                    _key = _keyList[i];
                    SetOperationsList();
                    while (_model.ActionController(_currentAction, _key))
                    {
                        if (_currentAction != Model.Action.DoAction)
                            _currentAction = Model.Action.DoAction;
                            
                    }
                    if(_loadIndex == 1)
                        Update(_model.Root, _model.MaxHeight);
                    else
                    {
                        UpdateSecondCanvas();
                        Update(_model.Root, _model.MaxHeight);
                    }
                }
            }

            IsEnabledStepNext = true;
            if(_loadIndex > 0)
                IsEnabledStepBack = true;
            OnPropertyChanged("IsEnabledStepNext");
            OnPropertyChanged("IsEnabledStepBack");
        }

        /// <summary>
		/// Vissza a példákhoz eseménykiváltása.
		/// </summary>
        private void OnExample1ToExamples()
        {
            if (Example1ToExamples != null)
                Example1ToExamples(this, EventArgs.Empty);
        }

        /// <summary>
		/// Vissza a menübe eseménykiváltása.
		/// </summary>
        private void OnExample1ToMain()
        {
            if (Example1ToMain != null)
                Example1ToMain(this, EventArgs.Empty);
        }
        #endregion

    }
}
