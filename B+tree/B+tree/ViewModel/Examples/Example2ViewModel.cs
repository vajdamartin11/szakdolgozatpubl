﻿using B_tree.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Threading;

namespace B_tree.ViewModel.Examples
{
    public class Example2ViewModel : ViewModelBase
    {
        #region Private data members
        private BpTree _model; // Fa amin példát mutatjuk
        private Int32 _order; // fokszám
        private Int32 _speedFactor; //sebesség
        private Boolean _skipFirstAction; // Előző állapot első akciót kihagyása
        private DispatcherTimer _animationTimer; //Műveletek időzítése
        private List<Model.Action> _actionList; // műveletsor amit elvégzünk
        private List<Int32> _keyList; // kulcsok amikkel a műveleteket végezzük
        private List<String> _commentList; // A megjegyzések listái
        private Int32 _loadIndex; // Aktuális példa indexe
        private List<String> _operationsList; //Eddig elvégzett műveletek kulccsal
        private Int32 _operationIndex; //Hányadik műveletet végeztük

        private Model.Action _currentAction; // aktuális akció
        private Boolean _animationOver; // vége van az adott műveletnek

        private Int32 _key; // Kulcs amivel épp a műveletet végezzük
        #endregion

        #region Properties
        /// <summary>
        /// Eddig elvégzett műveletek kulccsal lekérdezése, vagy beállítása.
        /// </summary>
        public BindingList<String> OperationsList { get; set; }

        /// <summary>
        /// A megjegyzésben található szöveg lekérdezése vagy beállítása.
        /// </summary>
        public String Comment { get; set; }

        /// <summary>
        /// Az előrelépés gomb aktívságának lekérdezése vagy beállítása.
        /// </summary>
        public Boolean IsEnabledStepNext { get; set; }

        /// <summary>
        /// A visszalépés gomb aktívságának lekérdezése vagy beállítása.
        /// </summary>
        public Boolean IsEnabledStepBack { get; set; }

        /// <summary>
        /// Az adott példa ablak esetén, előrelépés a példában parancs lekérdezése.
        /// </summary>
        public DelegateCommand NextStepCommand { get; private set; }

        /// <summary>
        /// Az adott példa ablak esetén, visszalépés a példában parancs lekérdezése.
        /// </summary>
        public DelegateCommand BackStepCommand { get; private set; }

        /// <summary>
        /// Az adott példa ablak esetén, vissza a példákra parancs lekérdezése.
        /// </summary>
        public DelegateCommand ExamplesCommand { get; private set; }

        /// <summary>
        /// Az adott példa ablak esetén, vissza a menübe parancs lekérdezése.
        /// </summary>
        public DelegateCommand BackCommand { get; private set; }
        #endregion


        #region Events

        /// <summary>
        /// Vissza a példákhoz eseménye.
        /// </summary>
        public EventHandler Example2ToExamples;

        /// <summary>
        /// Vissza a menübe eseménye.
        /// </summary>
        public EventHandler Example2ToMain;

        /// <summary>
        /// Vége a példának eseménye.
        /// </summary>
        public EventHandler Example2End;
        #endregion

        #region Constructors
        /// <summary>
        /// Example2ViewModel példányosítása
        /// </summary>
        public Example2ViewModel()
        {
            _loadIndex = 0;
            _order = 4;
            _model = new BpTree(_order);
            _animationOver = true;

            SetupCanvas(_order);
            Update(_model.Root, _model.MaxHeight);
            SetExample();

            IsEnabledStepNext = true;
            IsEnabledStepBack = false;
            OnPropertyChanged("IsEnabled");
            _speedFactor = 1200;

            _animationTimer = new DispatcherTimer();
            _animationTimer.Interval = TimeSpan.FromMilliseconds(_speedFactor);
            _animationTimer.Tick += DoAction;

            _operationIndex = 1;
            _operationsList = new List<String>();
            OperationsList = new BindingList<String>();

            NextStepCommand = new DelegateCommand(new Action<object>(o => OnNextStepClicked()));
            BackStepCommand = new DelegateCommand(new Action<object>(o => OnBackStepClicked()));
            ExamplesCommand = new DelegateCommand(new Action<object>(o => OnExample2ToExamples()));
            BackCommand = new DelegateCommand(new Action<object>(o => OnExample2ToMain()));

            Init();
        }
        #endregion
        #region Private methods

        /// <summary>
        /// Felépítjük a fát
        /// </summary>
        private void Init()
        {
            for(Int32 i = 0; i< 15; i++)
            {
                _currentAction = Model.Action.Insert;
                _key = 3* (i+1);
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }

                UpdateSecondCanvas();
                Update(_model.Root, _model.MaxHeight); ;
            }
        }

        /// <summary>
        /// A példa beállítása
        /// </summary>
        private void SetExample()
        {
            _keyList = new List<Int32>() { 48,21,33,24,27,30,9 };
            _actionList = new List<Model.Action>();
            _commentList = new List<String>();

            _actionList.Add(Model.Action.Insert);
            for (Int32 i = 0; i < 6; i++)
            {
                _actionList.Add(Model.Action.Delete);
            }
            SetCommentList();
        }

        /// <summary>
        /// Megjegyzések beállítása
        /// </summary>
        private void SetCommentList()
        {
            _commentList.Add("Beszúrjuk a 48-at, mivel több, mint fokszám-1 kulcs van (39, 42 | 45, 48), hasad a levél. A (45, 48) minimuma fog felmenni hasító kulcsnak." +
                " A szülő szintén megtelt (27, 33 | 39, 45), ezért hasad a csúcs, ugyanúgy (39,45)-ből a 39 felmegy a nagyszülőbe hasító kulcsként, annyi különbséggel, hogy (39, 45) csúcsban már nem marad ott a 39-es hasító kulcs.");
            _commentList.Add("Töröljük a 21-et, mivel kevesebb, mint fokszám/2 (alsó egész) kulcs van ( - ,24) , testvértől nem tudunk kulcsot átvenni." +
                "Ezért egyesül egy testvér csúccsal (27,30). Az ezeket összekötő hasító kulcsot töröljük a szülőből. Nincs teendőnk, van legalább fokszám/2 (alsó egész) gyerek a csúcsban.");
            _commentList.Add("Töröljük a 33-at, mivel kevesebb, mint fokszám/2 (alsó egész) kulcs van ( - ,36) , testvértől most tudunk kulcsot átvenni (30)." +
                "Az így kapott második levél minimuma megy fel hasító kulcsnak, az őket elválasztó eddig hasító kulcs helyére.");
            _commentList.Add("Töröljük a 24-et, mivel kevesebb, mint fokszám/2 (alsó egész) kulcs van ( - ,27) , testvértől nem tudunk kulcsot átvenni, egyesül a két testvér levél (27,30,36)." +
                "A szülőben törlődik a hozzájuk tartozó hasító kulcs (30). Nem lesz elég gyereke, de a testvér csúcstól tud átvenni egy gyereket (15,18). Nagyszülőből lejön a hasító kulcs(21), testvértől felmegy a (15).");
            _commentList.Add("Töröljük a 27-et, mivel nem kevesebb, mint fokszám/2 (alsó egész) kulcs van ( - ,30, 36), nincs teendőnk");
            _commentList.Add("Töröljük a 30-at, mivel kevesebb, mint fokszám/2 (alsó egész) kulcs van ( -, 36), testvértől nem tudtunk kucslot átvenni, egyesül a két testvér levél (15,18,36)." +
                "A szülőben törlődik a hozzájuk tartozó hasító kulcs (21). Nem lesz elég gyereke, testvértől sem tud átvenni. Összeolvad egy testvér csúccsal (jobb oldalival), nagyszülőből lejön az a hasító kulcs" +
                " ,ami eddig elválasztotta a két testvér csúcsot");
            _commentList.Add("Töröljük a 9-et, mivel kevesebb, mint fokszám/2 (alsó egész) kulcs van ( -, 12), testvértől nem tudtunk kucslot átvenni, egyesül a két testvér levél (3,6,12)." +
                " 24 törlésénél látottak figyelhetők meg itt is csak másik irányból. A szülőnek nem lesz elég gyereke, átvesz a testvér csúcstól egyet(15,18,36). A nagyszülőből lejön a hasító kulcs(15)." +
                " A testvértől felmegy a 39.");
        }

        /// <summary>
        /// léptetjük a példát
        /// </summary>

        private void Step()
        {
            if (_animationOver)
            {
                if (_loadIndex == _keyList.Count)
                {
                    if (Example2End != null)
                        Example2End(this, EventArgs.Empty);
                    IsEnabledStepNext = false;
                    OnPropertyChanged("IsEnabledStepNext");
                    IsEnabledStepBack = true;
                    OnPropertyChanged("IsEnabledStepBack");
                }

                else
                {
                    _currentAction = _actionList[_loadIndex];
                    _key = _keyList[_loadIndex];
                    Comment = _commentList[_loadIndex];
                    SetOperationsList();
                    OnPropertyChanged("Comment");
                    _animationTimer.Start();
                }
            }
        }

        /// <summary>
        /// A műveleteket hozzáadja a listába
        /// </summary>
        private void SetOperationsList()
        {
            _operationsList.Reverse();
            if (_currentAction == Model.Action.Insert)
                _operationsList.Add(_operationIndex++.ToString() + ". Beszúrtuk: " + _key);
            if (_currentAction == Model.Action.Delete)
                _operationsList.Add(_operationIndex++.ToString() + ". Töröltük: " + _key);
            if (_currentAction == Model.Action.Search)
                _operationsList.Add(_operationIndex++.ToString() + ". Kerestük: " + _key);
            _operationsList.Reverse();

            OperationsList = new BindingList<String>(_operationsList);
            OnPropertyChanged("OperationsList");
        }

        /// <summary>
        /// Művelet elvégzése
        /// </summary>
        private void DoAction(object sender, EventArgs e)
        {
            _animationOver = false;
            Boolean ThereAction = _model.ActionController(_currentAction, _key);

            if (_currentAction != Model.Action.DoAction)
                _currentAction = Model.Action.DoAction;

            if (!_skipFirstAction)
                UpdateSecondCanvas();

            _skipFirstAction = true;

            if (!ThereAction)
            {
                _animationOver = true;
                _skipFirstAction = false;
                IsEnabledStepNext = true;
                _loadIndex++;
                if (_loadIndex > 0)
                    IsEnabledStepBack = true;
                OnPropertyChanged("IsEnabledStepNext");
                OnPropertyChanged("IsEnabledStepBack");
                _animationTimer.Stop();
            }

            Update(_model.Root, _model.MaxHeight); ;
        }
        #endregion

        #region Event methods
        /// <summary>
		/// A példában előrelépés.
		/// </summary>
        private void OnNextStepClicked()
        {
            IsEnabledStepNext = false;
            IsEnabledStepBack = false;
            OnPropertyChanged("IsEnabledStepNext");
            OnPropertyChanged("IsEnabledStepBack");
            Step();
        }


        /// <summary>
		/// A példában visszalépés.
		/// </summary>
        private void OnBackStepClicked()
        {
            IsEnabledStepNext = false;
            IsEnabledStepBack = false;
            OnPropertyChanged("IsEnabledStepNext");
            OnPropertyChanged("IsEnabledStepBack");

            _operationIndex = 1;
            _operationsList = new List<String>();
            OperationsList = new BindingList<String>();
            OnPropertyChanged("OperationsList");
            if (_loadIndex == 1)
            {
                _loadIndex--;
                _model = new BpTree(_order);
                _animationOver = true;
                Init();
                Comment = "";
                OnPropertyChanged("");
                Update(_model.Root, _model.MaxHeight);
                UpdateSecondCanvas();
            }
            else if (_loadIndex - 2 > -1)
            {
                _model = new BpTree(_order);
                _loadIndex--;
                Init();
                Comment = _commentList[_loadIndex - 1];
                OnPropertyChanged("Comment");
                if (_loadIndex == 1)
                {
                    Update(_model.Root, _model.MaxHeight);
                    UpdateSecondCanvas();
                }

                for (Int32 i = 0; i < _loadIndex; i++)
                {
                    _currentAction = _actionList[i];
                    _key = _keyList[i];
                    SetOperationsList();
                    while (_model.ActionController(_currentAction, _key))
                    {
                        if (_currentAction != Model.Action.DoAction)
                            _currentAction = Model.Action.DoAction;

                    }
                    if (_loadIndex == 1)
                        Update(_model.Root, _model.MaxHeight);
                    else
                    {
                        UpdateSecondCanvas();
                        Update(_model.Root, _model.MaxHeight);
                    }
                }
            }

            IsEnabledStepNext = true;
            if (_loadIndex > 0)
                IsEnabledStepBack = true;
            OnPropertyChanged("IsEnabledStepNext");
            OnPropertyChanged("IsEnabledStepBack");
        }
        /// <summary>
		/// Vissza a példákra eseménykiváltása.
		/// </summary>
        private void OnExample2ToExamples()
        {
            if (Example2ToExamples != null)
                Example2ToExamples(this, EventArgs.Empty);
        }
        /// <summary>
		/// Vissza a menübe eseménykiváltása.
		/// </summary>
        private void OnExample2ToMain()
        {
            if (Example2ToMain != null)
                Example2ToMain(this, EventArgs.Empty);
        }
        #endregion
        

    }
}
