﻿using System;


namespace B_tree.ViewModel
{
    public class PlanPointer
    {
        /// <summary>
        /// A mutató cellának szélességének lekérdezése, vagy beállítása.
        /// </summary>
        public Double PointerWidth { get; set; }

        /// <summary>
        /// A mutató cellának eltolásának lekérdezése, vagy beállítása.
        /// </summary>
        public Double Transform { get; set; }

        /// <summary>
        /// Vízszintes koordináta lekérdezése, vagy beállítása.
        /// </summary>
        public Double X { get; set; }

        /// <summary>
        /// Függőleges koordináta lekérdezése, vagy beállítása.
        /// </summary>
        public Double Y { get; set; }

        /// <summary>
        /// Van-e mutató(van kör a cellában vagy nincs) lekérdezése, vagy beállítása.
        /// </summary>
        public Boolean Filled { get; set; }

        /// <summary>
        /// A mutató cellának színe lekérdezése, vagy beállítása.
        /// </summary>
        public String Color { get; set; }

        /// <summary>
        /// Az adott mutató cellának kell-e animáció lekérdezése, vagy beállítása.
        /// </summary>
        public Boolean Animation { get; set; }


    }
}
