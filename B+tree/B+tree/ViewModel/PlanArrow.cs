﻿using System;
using B_tree.Persistence;

namespace B_tree.ViewModel
{
    public class PlanArrow
    {
        public Node Parent { get; set; }

        /// <summary>
        /// Vízszintes koordináta ahonnan indul lekérdezése, vagy beállítása.
        /// </summary>
        public Double XFrom { get; set; }

        /// <summary>
        /// Függőleges koordináta ahonnan indul lekérdezése, vagy beállítása.
        /// </summary>
        public Double YFrom { get; set; }

        /// <summary>
        /// Vízszintes koordináta ahová mutat lekérdezése, vagy beállítása.
        /// </summary>
        public Double XTo { get; set; }

        /// <summary>
        /// Függőleges koordináta ahová mutat lekérdezése, vagy beállítása.
        /// </summary>
        public Double YTo { get; set; }
    }
}
