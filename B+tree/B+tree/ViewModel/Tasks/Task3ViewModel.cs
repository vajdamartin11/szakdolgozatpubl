﻿using B_tree.Model;
using System;
using System.Collections.Generic;

namespace B_tree.ViewModel.Tasks
{
    public enum BpTreeTask { Bracketed, InsertAndBracketed, DeleteAndBracketed, InsertAndLeafBracketed, DeleteAndLeafBracketed, InsertHeight, DeleteHeight, InsertIndexLeaf, Replace }
    public class Task3ViewModel : ViewModelBase
    {
        #region Private data members
        private BpTree _model; // fa amit megjelenítünk
        private BpTree _modelSolution; // a megoldást tartalmazó fa
        private Int32 _order; // fokszám
        private Model.Action _currentAction;// aktuális művelet/akció
        private Int32 _key; // kulcs amivel a műveletet végezzük
        private String _solution; // megoldás
        private List<Int32> _keyList; // kulcsok, amiket beszúrunk vagy törlünk
        private List<Int32> _keyListAfterDelete; // kulcsok amik a fában maradtak
        private List<Model.Action> _actionList; // mikor milyen akciót szeretnénk majd
        private String _solutionComment; // A megoldás utáni megjegyzés
        private BpTreeTask _currentTask; //Aktuális feladat, amit kigeneráltunk 
        private Random _rnd; // Véletlen
        #endregion
        #region Properties
        /// <summary>
        /// A tipp és megoldás gombok elérhetőek-e lekérdezése, vagy beállítása.
        /// </summary>
        public Boolean IsEnabled { get; set; }

        /// <summary>
        /// A következő feladat gomb elérhető-e lekérdezése, vagy beállítása.
        /// </summary>
        public Boolean IsEnabledNext { get; set; }

        /// <summary>
        /// A megjegyzés szövegének lekérdezése, vagy beállítása.
        /// </summary>
        public String Comment { get; set; }

        /// <summary>
        /// A tipp lekérdezése, vagy beállítása.
        /// </summary>
        public String Tips { get; set; }

        /// <summary>
        /// Az adott feladat esetén, tipp megadása parancs lekérdezése.
        /// </summary>
        public DelegateCommand TipCommand { get; private set; }

        /// <summary>
        /// Az adott feladat esetén, megoldás megadása parancs lekérdezése.
        /// </summary>
        public DelegateCommand SolutionCommand { get; private set; }

        /// <summary>
        /// Az adott feladat esetén, következő feladat parancs lekérdezése.
        /// </summary>
        public DelegateCommand NextTaskCommand { get; private set; }

        /// <summary>
        /// Az adott feladat esetén, vissza a feladatokra parancs lekérdezése.
        /// </summary>
        public DelegateCommand TasksCommand { get; private set; }

        /// <summary>
        /// Az adott feladat esetén, vissza a menübe parancs lekérdezése.
        /// </summary>
        public DelegateCommand BackCommand { get; private set; }
        #endregion

        #region Events

        /// <summary>
        /// A tipp megadásának eseménye.
        /// </summary>
        public EventHandler<Boolean> TipClicked;

        /// <summary>
        /// A megoldás megadásának eseménye.
        /// </summary>
        public EventHandler<String> SolutionClicked;

        /// <summary>
        /// Vissza a feladatokra eseménye.
        /// </summary>
        public EventHandler Task3ToTasks;

        /// <summary>
        /// Vissza a menübe eseménye.
        /// </summary>
        public EventHandler Task3ToMain;
        #endregion


        #region Constructors
        /// <summary>
        /// Task3ViewModel példányosítása
        /// </summary>
        public Task3ViewModel()
        {
            _rnd = new Random();
            _actionList = new List<Model.Action>();
            IsEnabled = true;
            IsEnabledNext = true;
            _order = 4;
            _model = new BpTree(_order);
            _modelSolution = new BpTree(_order);
            _keyList = new List<Int32>();
            _keyListAfterDelete = new List<Int32>();


            SetupCanvas(_order);

            GenerateTask();


            TipCommand = new DelegateCommand(new Action<object>(o => OnTipClicked()));
            SolutionCommand = new DelegateCommand(new Action<object>(o => OnSolutionClicked()));
            TasksCommand = new DelegateCommand(new Action<object>(o => OnTask3ToTasks()));
            BackCommand = new DelegateCommand(new Action<object>(o => OnTask3ToMain()));
            NextTaskCommand = new DelegateCommand(new Action<object>(o => OnNextClick()));
        }

        #endregion

        #region Private methods
        /// <summary>
        /// A feladat generálása
        /// </summary>
        private void GenerateTask()
        {
            _currentTask = SelectTask(_rnd.Next(9));
            _order = _rnd.Next(4, 7);
            SetupCanvas(_order);
            OnPropertyChanged("Keys");
            OnPropertyChanged("Arrows");
            OnPropertyChanged("Pointers");
            CreateBpTree();

            switch (_currentTask)
            {
                case BpTreeTask.Bracketed:
                    Bracketed();
                    break;
                case BpTreeTask.InsertAndBracketed:
                    InsertAndBracketed();
                    break;
                case BpTreeTask.DeleteAndBracketed:
                    DeleteAndBracketed();
                    break;
                case BpTreeTask.InsertAndLeafBracketed:
                    InsertAndLeafBracketed();
                    break;
                case BpTreeTask.DeleteAndLeafBracketed:
                    DeleteAndLeafBracketed();
                    break;
                case BpTreeTask.InsertHeight:
                    InsertHeight();
                    break;
                case BpTreeTask.DeleteHeight:
                    DeleteHeight();
                    break;
                case BpTreeTask.InsertIndexLeaf:
                    InsertIndexLeaf();
                    break;
                case BpTreeTask.Replace:
                    Replace();
                    break;
            }

            Update(_model.Root, _model.MaxHeight);
        }

        /// <summary>
        /// Zárójeles feladat beállítása
        /// </summary>
        private void Bracketed()
        {
            _solution = _modelSolution.Bracketed();
            Comment = "Adja meg a ZÁRÓJELES alakot. A kulcsokat vesszővel elválasztva adja majd meg ((1,3,5) alakban).";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a " + _solution;
        }

        /// <summary>
        /// Beszúrás utáni állapot zárójeles feladat beállítása
        /// </summary>
        private void InsertAndBracketed()
        {
            _currentAction = Model.Action.Insert;
            _key = 0;
            while (_key == 0 || _keyList.Contains(_key))
                _key = _rnd.Next(1, 200);
            while (_modelSolution.ActionController(_currentAction, _key))
            {
                if (_currentAction != Model.Action.DoAction)
                    _currentAction = Model.Action.DoAction;
            }
            _solution = _model.Bracketed();
            Comment = "A " + _key.ToString() +" BESZÚRÁSA után, hogyan alakul a ZÁRÓJELES alak? A beszúrás követő állapot zárójeles alakját adja meg! A kulcsokat vesszővel elválasztva adja majd meg ((1,3,5) alakban).";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a " + _solution;
        }

        /// <summary>
        /// Törlés utáni állapot zárójeles feladat beállítása
        /// </summary>
        private void DeleteAndBracketed()
        {
            _currentAction = Model.Action.Delete;
            _key = 0;
            while (_key == 0 || !_keyListAfterDelete.Contains(_key))
                _key = _keyListAfterDelete[_rnd.Next(0, _keyListAfterDelete.Count)];
            while (_modelSolution.ActionController(_currentAction, _key))
            {
                if (_currentAction != Model.Action.DoAction)
                    _currentAction = Model.Action.DoAction;
            }
            _solution = _modelSolution.Bracketed();
            Comment = "A " + _key.ToString() + " TÖRLÉSE után, hogyan alakul a ZÁRÓJELES alak? A beszúrás követő állapot zárójeles alakját adja meg! A kulcsokat vesszővel elválasztva adja majd meg ((1,3,5) alakban).";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a " + _solution;
        }

        /// <summary>
        /// Beszúrás utáni levelek állapotai zárójeles feladata beállítása
        /// </summary>
        private void InsertAndLeafBracketed()
        {
            _currentAction = Model.Action.Insert;
            _key = 0;
            while (_key == 0 || _keyList.Contains(_key))
                _key = _rnd.Next(1, 200);
            while (_modelSolution.ActionController(_currentAction, _key))
            {
                if (_currentAction != Model.Action.DoAction)
                    _currentAction = Model.Action.DoAction;
            }

            _solution = _modelSolution.GetLeavesBracketedForm();
            Comment = "A "+ _key.ToString() +" BESZÚRÁSA után, hogyan alakulnak a LEVELEK? A beszúrás követő állapot LEVELEINEK zárójeles alakját adja meg! A kulcsokat vesszővel elválasztva adja majd meg, a leveleket ne válassza el egymástól ((1,2,3)(4,5,6)...).";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a " + _solution;
        }

        /// <summary>
        /// Törlés utáni levelek állapotai zárójeles feladata beállítása
        /// </summary>
        private void DeleteAndLeafBracketed()
        {
            _currentAction = Model.Action.Delete;
            _key = 0;
            while (_key == 0 || !_keyListAfterDelete.Contains(_key))
                _key = _keyListAfterDelete[_rnd.Next(0, _keyListAfterDelete.Count)];
            while (_modelSolution.ActionController(_currentAction, _key))
            {
                if (_currentAction != Model.Action.DoAction)
                    _currentAction = Model.Action.DoAction;
            }

            _solution = _modelSolution.GetLeavesBracketedForm();
            Comment = "A "+ _key.ToString()+ " TÖRLÉSE után, hogyan alakulnak a LEVELEK? A beszúrás követő állapot LEVELEINEK zárójeles alakját adja meg! A kulcsokat vesszővel elválasztva adja majd meg, a leveleket ne válassza el egymástól ((1,2,3)(4,5,6)...).";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a " + _solution;
        }

        /// <summary>
        /// Beszúrás után a famagasság változása feladat beállítása
        /// </summary>
        private void InsertHeight()
        {
            _currentAction = Model.Action.Insert;
            _key = 0;
            while (_key == 0 || _keyList.Contains(_key))
                _key = _rnd.Next(1, 200);
            while (_modelSolution.ActionController(_currentAction, _key))
            {
                if (_currentAction != Model.Action.DoAction)
                    _currentAction = Model.Action.DoAction;
            }
            _solution = (_model.MaxHeight != _modelSolution.MaxHeight) ? "igen" : "nem";
            Comment = "Változik a fa magassága a " + _key.ToString() + " BESZÚRÁSA után?(Igen / Nem választ adjon!)";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a " + _solution;
        }

        /// <summary>
        /// Törlés után a famagasság változása feladat beállítása
        /// </summary>
        private void DeleteHeight()
        {
            _currentAction = Model.Action.Delete;
            _key = 0;
            while (_key == 0 || !_keyListAfterDelete.Contains(_key))
                _key = _keyListAfterDelete[_rnd.Next(0, _keyListAfterDelete.Count)];
            while (_modelSolution.ActionController(_currentAction, _key))
            {
                if (_currentAction != Model.Action.DoAction)
                    _currentAction = Model.Action.DoAction;
            }
            _solution = (_model.MaxHeight != _modelSolution.MaxHeight) ? "igen" : "nem";
            _solution.ToLower();
            Comment = "Változik a fa magassága a "+_key.ToString()+ " TÖRLÉSE után?(Igen / Nem választ adjon!)";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a " +_solution;
        }

        /// <summary>
        /// Hányadik levélbe szúrodik feladat beállítása
        /// </summary>
        private void InsertIndexLeaf()
        {
            _currentAction = Model.Action.Insert;
            _key = 0;
            while (_key == 0 || _keyList.Contains(_key))
                _key = _rnd.Next(1, 200);
            while (_modelSolution.ActionController(_currentAction, _key))
            {
                if (_currentAction != Model.Action.DoAction)
                    _currentAction = Model.Action.DoAction;
            }
            _solution = _modelSolution.GetLeafIndexAfterInsert(_key).ToString();
            Comment = "A " + _key.ToString() + " BESZÚRÁSA után (FIGYELEM! történhet VÁGÁS is) hányadik levélbe kerül a kulcs?(Egy számot adjon meg!)";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a " + _solution;
        }

        /// <summary>
        /// Behelyesítéső feladat beállítása
        /// </summary>
        private void Replace()
        {
            Int32 ind = _rnd.Next(1, _modelSolution.GetNodeIndex());
            _model.TaskTree(_modelSolution.MaxHeight, _modelSolution.Root,ind);
            Update(_model.Root, _model.MaxHeight);
            _solution = _modelSolution.GetNodeWithIndex(ind);
            Comment = "Adja meg az eltérő színű csúcs kulcsait/hasítókulcsait zárójelben, vesszővel elválasztva ((1,2,3) alakban). A zárójeles alak: " + _modelSolution.Bracketed();
            
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a "+ _solution;
        }

        /// <summary>
        /// Feladathoz szükséges fa beállítása
        /// </summary>
        private void CreateBpTree()
        {
            _model = new BpTree(_order);
            _modelSolution = new BpTree(_order);
            List<Int32> tempList = new List<Int32>();
            Int32 keysCount = _rnd.Next(3 * _order, 5 * _order);
            _actionList = new List<Model.Action>();
            _keyList = new List<Int32>();
            _keyListAfterDelete = new List<Int32>();
            for (Int32 i = 0; i < keysCount; i++)
            {
                Int32 t = 0;
                while (t == 0 || _keyList.Contains(t))
                    t = _rnd.Next(1, 200);
                _keyList.Add(t);
                _actionList.Add(Model.Action.Insert);
            }
            _keyListAfterDelete.AddRange(_keyList);
            keysCount = _rnd.Next(0, 4);
            for (Int32 i = 0; i < keysCount; i++)
            {
                Int32 t = 0;
                while (t == 0 || !_keyList.Contains(t) || tempList.Contains(t))
                    t = _keyList[_rnd.Next(0, _keyList.Count)];
                tempList.Add(t);
                _keyListAfterDelete.Remove(t);
                _actionList.Add(Model.Action.Delete);
            }
            _keyList.AddRange(tempList);
            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = _actionList[i];
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }
                _currentAction = _actionList[i];
                while (_modelSolution.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }
            }
        }

        /// <summary>
        /// A feladat kiválasztása a véletlen generálás után
        /// </summary>
        /// <param name="rnd">A generált véletlen szám</param>
        /// <returns>A feladattal tér vissza</returns>
        private BpTreeTask SelectTask(Int32 rnd)
        {
            switch(rnd)
            {
                case 0:
                    return BpTreeTask.Bracketed;
                case 1:
                    return BpTreeTask.InsertAndBracketed;
                case 2:
                    return BpTreeTask.DeleteAndBracketed;
                case 3:
                    return BpTreeTask.InsertAndLeafBracketed;
                case 4:
                    return BpTreeTask.DeleteAndLeafBracketed;
                case 5:
                    return BpTreeTask.InsertHeight;
                case 6:
                    return BpTreeTask.InsertAndLeafBracketed;
                case 7:
                    return BpTreeTask.InsertIndexLeaf;
                case 8:
                    return BpTreeTask.Replace;
            }
            return BpTreeTask.Bracketed;
        }

        #endregion

        #region Event methods

        /// <summary>
		/// A tipp megadására eseménykiváltása.
		/// </summary>
        private void OnTipClicked()
        {
            Boolean TipIsOK = _solution == Tips.ToLower();
            if (TipIsOK)
            {
                IsEnabled = false;
                OnPropertyChanged("IsEnabled");
                Update(_modelSolution.Root, _modelSolution.MaxHeight);
                Comment += _solutionComment;
                OnPropertyChanged("Comment");
            }

            if (TipClicked != null)
                TipClicked(this, TipIsOK);
        }

        /// <summary>
		/// A megoldás megadására eseménykiváltása.
		/// </summary>
        private void OnSolutionClicked()
        {
            IsEnabled = false;
            OnPropertyChanged("IsEnabled");
            String Answer = _solution;
            Update(_modelSolution.Root, _modelSolution.MaxHeight);
            Comment += _solutionComment;
            OnPropertyChanged("Comment");
            if (SolutionClicked != null)
                SolutionClicked(this, Answer);
        }

        /// <summary>
		/// Vissza a feladatokra eseménykiváltása.
		/// </summary>
        private void OnTask3ToTasks()
        {
            if (Task3ToTasks != null)
                Task3ToTasks(this, EventArgs.Empty);
        }

        /// <summary>
		/// Vissza a menübe eseménykiváltása.
		/// </summary>
        private void OnTask3ToMain()
        {
            if (Task3ToMain != null)
                Task3ToMain(this, EventArgs.Empty);
        }

        /// <summary>
		/// A következő (új) feladatra lépés.
		/// </summary>
        private void OnNextClick()
        {
            GenerateTask();
            IsEnabled = true;
            OnPropertyChanged("IsEnabled");
            
        }
        #endregion
    }
}
