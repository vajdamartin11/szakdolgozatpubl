﻿using B_tree.Model;
using System;
using System.Collections.Generic;

namespace B_tree.ViewModel.Tasks
{
    public class Task1ViewModel : ViewModelBase 
    {
        #region Private data members
        private BpTree _model; // fa amit megjelenítünk
        private BpTree _modelSolution; // a megoldást tartalmazó fa
        private Int32 _order; // fokszám
        private Int32 _taskIndex; // aktuális feladat indexe
        private Model.Action _currentAction; // aktuális művelet/akció
        private Int32 _key; // kulcs amivel a műveletet végezzük
        private String _solution; // megoldás
        private List<Int32> _keyList; // kulcsok, amiket beszúrunk vagy törlünk
        private String _solutionComment; // A megoldás utáni megjegyzés
        #endregion

        #region Properties

        /// <summary>
        /// A tipp és megoldás gombok elérhetőek-e lekérdezése, vagy beállítása.
        /// </summary>
        public Boolean IsEnabled { get; set; }

        /// <summary>
        /// A következő feladat gomb elérhető-e lekérdezése, vagy beállítása.
        /// </summary>
        public Boolean IsEnabledNext { get; set; }

        /// <summary>
        /// Az előző feladat gomb elérhető-e lekérdezése, vagy beállítása.
        /// </summary>
        public Boolean IsEnabledBack { get; set; }

        /// <summary>
        /// A megjegyzés szövegének lekérdezése, vagy beállítása.
        /// </summary>
        public String Comment { get; set; }

        /// <summary>
        /// A tipp lekérdezése, vagy beállítása.
        /// </summary>
        public String Tips { get; set; }

        /// <summary>
        /// Az adott feladat esetén, tipp megadása parancs lekérdezése.
        /// </summary>
        public DelegateCommand TipCommand { get; private set; }

        /// <summary>
        /// Az adott feladat esetén, megoldás megadása parancs lekérdezése.
        /// </summary>
        public DelegateCommand SolutionCommand { get; private set; }

        /// <summary>
        /// Az adott feladat esetén, következő feladat parancs lekérdezése.
        /// </summary>
        public DelegateCommand NextTaskCommand { get; private set; }

        /// <summary>
        /// Az adott feladat esetén, előző feladat parancs lekérdezése.
        /// </summary>
        public DelegateCommand PreviousTaskCommand { get; private set; }

        /// <summary>
        /// Az adott feladat esetén, vissza a feladatokra parancs lekérdezése.
        /// </summary>
        public DelegateCommand TasksCommand { get; private set; }

        /// <summary>
        /// Az adott feladat esetén, vissza a menübe parancs lekérdezése.
        /// </summary>
        public DelegateCommand BackCommand { get; private set; }
        #endregion

        #region Events


        /// <summary>
        /// A tipp megadásának eseménye.
        /// </summary>
        public EventHandler<Boolean> TipClicked;

        /// <summary>
        /// A megoldás megadásának eseménye.
        /// </summary>
        public EventHandler<String> SolutionClicked;

        /// <summary>
        /// Vissza a feladatokra eseménye.
        /// </summary>
        public EventHandler Task1ToTasks;

        /// <summary>
        /// Vissza a menübe eseménye.
        /// </summary>
        public EventHandler Task1ToMain;
        #endregion

        #region Constructors
        /// <summary>
        /// Task1ViewModel példányosítása
        /// </summary>
        public Task1ViewModel()
        {
            IsEnabledNext = true;
            IsEnabled = true;
            IsEnabledBack = false;
            _order = 4;
            _model = new BpTree(_order);
            _modelSolution = new BpTree(_order);
            _taskIndex = 0;
            _keyList = new List<Int32>();
            SetupCanvas(_order);
            Task1();

            TipCommand = new DelegateCommand(new Action<object>(o => OnTipClicked()));
            SolutionCommand = new DelegateCommand(new Action<object>(o => OnSolutionClicked()));
            TasksCommand = new DelegateCommand(new Action<object>(o => OnTask1ToTasks()));
            BackCommand = new DelegateCommand(new Action<object>(o => OnTask1ToMain()));
            NextTaskCommand = new DelegateCommand(new Action<object>(o => OnNextClick()));
            PreviousTaskCommand = new DelegateCommand(new Action<object>(o => OnPreviousClick()));
        }
        #endregion
        #region Private methods

        /// <summary>
        /// Első feladat beállítása
        /// </summary>
        private void Task1()
        {
            _model = new BpTree(_order);
            _modelSolution = new BpTree(_order);
            Update(_model.Root, _model.MaxHeight);
            _solution = "2";
            Comment = "Mennyi kulcsnak kell lennie legalább egy levélen, ha a fokszám 4?(Egy számot adjon meg!)";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a 2, mivel fokszám/2 alsó egész számú kulcsnak kell lennie minimum";
        }

        /// <summary>
        /// Második feladat beállítása
        /// </summary>
        private void Task2()
        {
            _model = new BpTree(_order);
            _modelSolution = new BpTree(_order);
            Update(_model.Root, _model.MaxHeight);
            _solution = "3";
            Comment = "Mennyi kulcs lehet legfeljebb egy levélen, ha a fokszám 4?(Egy számot adjon meg!)";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a 3, mivel fokszám-1 számú kulcs lehet maximum";
        }

        /// <summary>
        /// Harmadik feladat beállítása
        /// </summary>
        private void Task3()
        {
            _keyList = new List<Int32>() { 2, 7, 8, 12, 20, 23, 26 };
            _model = new BpTree(_order);
            _modelSolution = new BpTree(_order);
            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }
                _currentAction = Model.Action.Insert;
                while (_modelSolution.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }
                Update(_model.Root, _model.MaxHeight);
            }
            _currentAction = Model.Action.Insert;
            _key = 30;
            while(_modelSolution.ActionController(_currentAction,_key))
            {
                if (_currentAction != Model.Action.DoAction)
                    _currentAction = Model.Action.DoAction;
            }
            _solution = (_model.MaxHeight != _modelSolution.MaxHeight) ? "igen" : "nem";
            Comment = "Változik a fa magassága a 30 beszúrása után?(Igen / Nem választ adjon!)";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a nem, mivel a gyökérben még pont fokszám számú gyerek van.";
        }

        /// <summary>
        /// Negyedik feladat beállítása
        /// </summary>
        private void Task4()
        {
            _keyList = new List<Int32>() { 8, 15, 13, 5, 20, 3, 6, 10, 17, 9 };
            _model = new BpTree(_order);
            _modelSolution = new BpTree(_order);
            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }
                _currentAction = Model.Action.Insert;
                while (_modelSolution.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }
                Update(_model.Root, _model.MaxHeight);
            }
            _solution = _model.GetSplitCount().ToString();
            Comment = "Hányszor kellett vágnunk a fában, ha tudjuk, hogy nem volt törlés? Beszúrás sorrendje: 8, 15, 13, 5, 20, 3, 6, 10, 17, 9 (Egy számot adjon meg!)";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz az 5, 4-szer telik be levél és ezért vágjuk őket és egyszer a szülő(gyökér) is betelik ami +1 vágást jelent.";
        }

        /// <summary>
        /// Ötödik feladat beállítása
        /// </summary>
        private void Task5()
        {
            _keyList = new List<Int32>() { 2, 7, 8, 12, 20, 23, 26 };
            _model = new BpTree(_order);
            _modelSolution = new BpTree(_order);
            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key) )
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }
                _currentAction = Model.Action.Insert;
                while (_modelSolution.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }
                Update(_model.Root, _model.MaxHeight);
            }
            _currentAction = Model.Action.Delete;
            _key = 12;
            while (_modelSolution.ActionController(_currentAction, _key))
            {
                if (_currentAction != Model.Action.DoAction)
                    _currentAction = Model.Action.DoAction;
            }
            _solution = "nem";
            Comment = "Összeolvad valamelyik levél a 12 törlése után?(Igen / Nem választ adjon!)";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a Nem, bár kevés kulcs lesz a levélben, de tud átvenni kulcsot a testvérétől.";
        }

        /// <summary>
        /// Hatodik feladat beállítása
        /// </summary>
        private void Task6()
        {
            _keyList = new List<Int32>() { 8, 15, 13, 5, 20, 3, 6, 10, 17, 9 };
            _model = new BpTree(_order);
            _modelSolution = new BpTree(_order);
            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }
                _currentAction = Model.Action.Insert;
                while (_modelSolution.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }
                Update(_model.Root, _model.MaxHeight);
            }
            _currentAction = Model.Action.Delete;
            _key = 13;
            while (_modelSolution.ActionController(_currentAction, _key))
            {
                if (_currentAction != Model.Action.DoAction)
                    _currentAction = Model.Action.DoAction;
            }
            _solution = "(13)";
            Comment = "13 törlése esetén mi kerül szülő (17-es) hasító kulcsa helyére. (A választ zárójelek között adja meg, ilyen formában (11), ha több hasító kulcs van (11,30) )";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a (13), a gyökérből fog lejönni a hasítókulcs, átvesz egy gyereket a testvértől.";
        }

        /// <summary>
        /// Hetedik feladat beállítása
        /// </summary>
        private void Task7()
        {
            _keyList = new List<Int32>() { 8, 15, 13, 5, 20, 3, 6, 10, 17, 9 };
            _model = new BpTree(_order);
            _modelSolution = new BpTree(_order);
            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }
                _currentAction = Model.Action.Insert;
                while (_modelSolution.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }
                Update(_model.Root, _model.MaxHeight);
            }
            _solution = _model.Bracketed();
            Comment = "Adja meg a zárójeles alakot. A kulcsokat vesszővel elválasztva adja majd meg ((1,3,5) alakban).";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a " + _model.Bracketed();
        }

        /// <summary>
        /// Nyolcadik feladat beállítása
        /// </summary>
        private void Task8()
        {
            _keyList = new List<Int32>() { 8, 15, 13, 5, 20, 3, 6, 10, 17, 9 ,13 };
            _model = new BpTree(_order);
            _modelSolution = new BpTree(_order);
            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = i < 10 ? Model.Action.Insert : Model.Action.Delete;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }
                _currentAction = i < 10 ? Model.Action.Insert : Model.Action.Delete;
                while (_modelSolution.ActionController(_currentAction, _key))
                {
                    if (_currentAction != Model.Action.DoAction)
                        _currentAction = Model.Action.DoAction;
                }

            }
            Update(_model.Root, _model.MaxHeight);
            _currentAction = Model.Action.Insert;
            _key = 18;
            while (_modelSolution.ActionController(_currentAction, _key))
            {
                if (_currentAction != Model.Action.DoAction)
                    _currentAction = Model.Action.DoAction;
            }

            _solution = _modelSolution.Bracketed();
            Comment = "A 13 beszúrása után, hogyan alakul a zárójeles alak? A beszúrás követő állapot zárójeles alakját adja meg! A kulcsokat vesszővel elválasztva adja majd meg ((1,3,5) alakban).";
            OnPropertyChanged("Comment");
            _solutionComment = " A helyes válasz a " + _modelSolution.Bracketed();
        }
        #endregion
        #region Event methods

        /// <summary>
        /// A tipp megadására eseménykiváltása.
        /// </summary>
        private void OnTipClicked()
        {
            Boolean TipIsOK = _solution == Tips.ToLower();
            if(TipIsOK)
            {
                IsEnabled = false;
                OnPropertyChanged("IsEnabled");
                Update(_modelSolution.Root, _modelSolution.MaxHeight);
                Comment += _solutionComment;
                OnPropertyChanged("Comment");
            }

            if (TipClicked != null)
                TipClicked(this, TipIsOK);
        }

        /// <summary>
		/// A megoldás megadására eseménykiváltása.
		/// </summary>
        private void OnSolutionClicked()
        {
            IsEnabled = false;
            OnPropertyChanged("IsEnabled");
            String Answer = _solution;
            Update(_modelSolution.Root, _modelSolution.MaxHeight);
            Comment += _solutionComment;
            OnPropertyChanged("Comment");
            if (SolutionClicked != null)
                SolutionClicked(this, Answer);
        }

        /// <summary>
		/// Vissza a feladatokra eseménykiváltása.
		/// </summary>
        private void OnTask1ToTasks()
        {
            if (Task1ToTasks != null)
                Task1ToTasks(this, EventArgs.Empty);
        }

        /// <summary>
		/// Vissza a menübe eseménykiváltása.
		/// </summary>
        private void OnTask1ToMain()
        {
            if (Task1ToMain != null)
                Task1ToMain(this, EventArgs.Empty);
        }

        /// <summary>
		/// A következő feladatra lépés.
		/// </summary>
        private void OnNextClick()
        {
            IsEnabledBack = true;
            OnPropertyChanged("IsEnabledBack");
            if (_taskIndex < 7)
            {
                IsEnabled = true;
                OnPropertyChanged("IsEnabled");
                _taskIndex++;
                switch (_taskIndex)
                {
                    case 1:
                        Task2();
                        break;
                    case 2:
                        Task3();
                        break;
                    case 3:
                        Task4();
                        break;
                    case 4:
                        Task5();
                        break;
                    case 5:
                        Task6();
                        break;
                    case 6:
                        Task7();
                        break;
                    case 7:
                        Task8();
                        IsEnabledNext = false;
                        OnPropertyChanged("IsEnabledNext");
                        break;
                }
            }
            
        }

        /// <summary>
		/// Az előző feladatra lépés.
		/// </summary>
        private void OnPreviousClick()
        {
            IsEnabledNext = true;
            OnPropertyChanged("IsEnabledNext");
            if (_taskIndex > 0)
            {
                IsEnabled = true;
                OnPropertyChanged("IsEnabled");
                _taskIndex--;
                switch (_taskIndex)
                {
                    case 0:
                        IsEnabledBack = false;
                        OnPropertyChanged("IsEnabledBack");
                        Task1();
                        break;
                    case 1:
                        Task2();
                        break;
                    case 2:
                        Task3();
                        break;
                    case 3:
                        Task4();
                        break;
                    case 4:
                        Task5();
                        break;
                    case 5:
                        Task6();
                        break;
                    case 6:
                        Task7();
                        break;
                }
            }
        }
        #endregion
    }
}
