﻿using System;

namespace B_tree.ViewModel
{
    public class PlanKey
    {
        /// <summary>
        /// A felületen megjelenő kulcs lekérdezése, vagy beállítása.
        /// </summary>
        public String KeyOnWindow { get; set; }

        /// <summary>
        /// Vízszintes koordináta lekérdezése, vagy beállítása.
        /// </summary>
        public Int32 X { get; set; }

        /// <summary>
        /// Függőleges koordináta lekérdezése, vagy beállítása.
        /// </summary>
        public Int32 Y { get; set; }

        /// <summary>
        /// A Kulcs cella színe lekérdezése, vagy beállítása.
        /// </summary>
        public String Color { get; set; }

        /// <summary>
        /// Az adott mutató cellának kell-e animáció lekérdezése, vagy beállítása.
        /// </summary>
        public Boolean Animation { get; set; }
    }
}
