﻿using System;

namespace B_tree.ViewModel
{
    public class TasksViewModel : ViewModelBase
    {

        #region Properties
        /// <summary>
        /// A feladatra lépés parancs lekérdezése.
        /// </summary>
        public DelegateCommand Task1Command { get; private set; }

        /// <summary>
        /// A feladatra lépés parancs lekérdezése.
        /// </summary>
        public DelegateCommand Task2Command { get; private set; }

        /// <summary>
        /// A feladatra lépés parancs lekérdezése.
        /// </summary>
        public DelegateCommand Task3Command { get; private set; }

        /// <summary>
        /// A visszalépés parancs lekérdezése.
        /// </summary>
        public DelegateCommand BackCommand { get; private set; }
        #endregion

        #region Events
        /// <summary>
        /// Feladat1 eseménye.
        /// </summary>
        public EventHandler GoToTask1;

        /// <summary>
        /// Feladat2 eseménye.
        /// </summary>
        public EventHandler GoToTask2;

        /// <summary>
        /// Feladat3 eseménye.
        /// </summary>
        public EventHandler GoToTask3;

        /// <summary>
        /// Vissza a menübe eseménye.
        /// </summary>
        public EventHandler TasksToMain;
        #endregion

        #region Constructors

        /// <summary>
        /// Feladatok nézetmodell példányosítása.
        /// </summary>
        public TasksViewModel()
        {
            Task1Command = new DelegateCommand(new Action<object>(o => OnGoToTask1()));
            Task2Command = new DelegateCommand(new Action<object>(o => OnGoToTask2()));
            Task3Command = new DelegateCommand(new Action<object>(o => OnGoToTask3()));
            BackCommand = new DelegateCommand(new Action<object>(o => OnTasksToMain()));
        }
        #endregion

        #region Event methods

        /// <summary>
        /// Feladat1 eseménykiváltása.
        /// </summary>
        private void OnGoToTask1()
        {
            if (GoToTask1 != null)
                GoToTask1(this, EventArgs.Empty);
        }


        /// <summary>
        /// Feladat2 eseménykiváltása.
        /// </summary>
        private void OnGoToTask2()
        {
            if (GoToTask2 != null)
                GoToTask2(this, EventArgs.Empty);
        }

        /// <summary>
        /// Feladat3 eseménykiváltása.
        /// </summary>
        private void OnGoToTask3()
        {
            if (GoToTask3 != null)
                GoToTask3(this, EventArgs.Empty);
        }


        /// <summary>
        /// Vissza a menübe eseménykiváltása.
        /// </summary>
        private void OnTasksToMain()
        {
            if (TasksToMain != null)
                TasksToMain(this, EventArgs.Empty);
        }
        #endregion
    }
}
