﻿using System;

namespace B_tree.ViewModel
{
    public class MainWindowViewModel
    {

        #region Properties

        /// <summary>
        /// A főmenü esetén, a példák kiválasztásához parancs lekérdezése.
        /// </summary>
        public DelegateCommand ExamplesCommand { get; private set; }

        /// <summary>
        /// A főmenü esetén, a feladatok kiválasztásához parancs lekérdezése.
        /// </summary>
        public DelegateCommand TasksCommand { get; private set; }

        /// <summary>
        /// A főmenü esetén, a fokszám megadásához parancs lekérdezése.
        /// </summary>
        public DelegateCommand OrderCommand { get; private set; }
        #endregion


        #region Events

        /// <summary>
        /// A példák kiválasztásához lépés eseménye.
        /// </summary>
        public EventHandler GoToExamples;

        /// <summary>
        /// A feladatok kiválasztásához lépés eseménye.
        /// </summary>
        public EventHandler GoToTasks;

        /// <summary>
        /// A fokszám megadásához lépés eseménye.
        /// </summary>
        public EventHandler GoToOrder;
        #endregion

        #region Constructors
        /// <summary>
        /// MainWindowViewModel példányosítása
        /// </summary>
        public MainWindowViewModel()
        {
            ExamplesCommand = new DelegateCommand(new Action<object>(o => OnGoToExamples()));
            TasksCommand = new DelegateCommand(new Action<object>(o => OnGoToTasks()));
            OrderCommand = new DelegateCommand(new Action<object>(o => OnGoToOrder()));
        }
        #endregion

        #region Event methods

        /// <summary>
		/// A példákra lépés eseménykiváltása.
		/// </summary>
        private void OnGoToExamples()
        {
            if (GoToExamples != null)
                GoToExamples(this, EventArgs.Empty);
        }

        /// <summary>
		/// A feladatokra lépés eseménykiváltása.
		/// </summary>
        private void OnGoToTasks()
        {
            if (GoToTasks != null)
                GoToTasks(this, EventArgs.Empty);
        }

        /// <summary>
		/// A fokszám kiváltására lépés eseménykiváltása.
		/// </summary>
        private void OnGoToOrder()
        {
            if (GoToOrder != null)
                GoToOrder(this, EventArgs.Empty);
        }
        #endregion
    }
}
