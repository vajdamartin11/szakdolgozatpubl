﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Threading;

using B_tree.Model;
using B_tree.Persistence;

namespace B_tree.ViewModel
{
    public class BuildTreeViewModel : ViewModelBase
    {
        #region Private date members
        private BpTree _model; // B+Fa
        private IDataAccess _dataAccess; // adatelérés

        private Int32 _speedFactor = 1000; // animáció sebessége
        private Int32 _order; // fokszám
        private Boolean _skipFirstAction; // Előző állapot csak a 2. akciótól frissül
        private DispatcherTimer _timer; // "animáció" időzítője
        private Int32 _key; // kulcs
        private Model.Action _currentAction; //aktuális akció
        private List<String> _operationsList; //Eddig elvégzett műveletek kulccsal
        private Int32 _operationIndex; //Hányadik műveletet végeztük
        #endregion

        #region Properties
        /// <summary>
        /// Eddig elvégzett műveletek kulccsal lekérdezése, vagy beállítása.
        /// </summary>
        public BindingList<String> OperationsList {get;set;}

        /// <summary>
        /// A gombok aktívságának lekérdezése, vagy beállítása.
        /// </summary>
        public Boolean IsEnabled { get; set; }

        /// <summary>
        /// A kulcs lekérdezése, vagy beállítása.
        /// </summary>
        public Int32 Key
        {
            get
            {
                return _key;
            }
            set
            {
                if(value != _key)
                {
                    _key = value; OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// A sebesség szövegének lekérdezése, vagy beállítása.
        /// </summary>
        public String Speed { get; set; }


        /// <summary>
        /// A fa építés esetén, a mentésképként parancs lekérdezése.
        /// </summary>
        public DelegateCommand SavePictureCommand { get; private set; }

        /// <summary>
        /// A fa építés esetén, a mentés parancs lekérdezése.
        /// </summary>
        public DelegateCommand SaveCommand { get; private set; }

        /// <summary>
        /// A fa építés esetén, a betöltés parancs lekérdezése.
        /// </summary>
        public DelegateCommand LoadCommand { get; private set; }

        /// <summary>
        /// A fa építés esetén, új fa parancs lekérdezése.
        /// </summary>
        public DelegateCommand NewTreeCommand { get; private set; }

        /// <summary>
        /// A fa építés esetén, a beszúrás parancs lekérdezése.
        /// </summary>
        public DelegateCommand InsertCommand { get; private set; }

        /// <summary>
        /// A fa építés esetén, a törlés parancs lekérdezése.
        /// </summary>
        public DelegateCommand DeleteCommand { get; private set; }

        /// <summary>
        /// A fa építés esetén, a keresés parancs lekérdezése.
        /// </summary>
        public DelegateCommand SearchCommand { get; private set; }

        /// <summary>
        /// A fa építés esetén, a gyorsítás parancs lekérdezése.
        /// </summary>
        public DelegateCommand FasterCommand { get; private set; }

        /// <summary>
        /// A fa építés esetén, a lassítás parancs lekérdezése.
        /// </summary>
        public DelegateCommand SlowerCommand { get; private set; }

        /// <summary>
        /// A fa építés esetén, a vissza menübe parancs lekérdezése.
        /// </summary>
        public DelegateCommand BackCommand { get; private set; }
        #endregion

        #region Events

        /// <summary>
        /// Képként kimentés eseménye.
        /// </summary>
        public EventHandler SavePictureClicked;

        /// <summary>
        /// Új fa eseménye.
        /// </summary>
        public EventHandler NewTreeClicked;

        /// <summary>
        /// Vissza a menübe eseménye.
        /// </summary>
        public EventHandler BuildTreeToMain;

        /// <summary>
        /// Helytelen kulcs megadásának eseménye.
        /// </summary>
        public EventHandler WrongInput;

        /// <summary>
        /// Attól függően, hogy törlünk vagy hozzáadunk kulcsot, szepel-e vagy sem a fában a kulcs eseménye.
        /// </summary>
        public EventHandler<String> WrongKey;

        /// <summary>
        /// Mentés eseménye.
        /// </summary>
        public EventHandler SaveClicked;

        /// <summary>
        /// Betöltés eseménye.
        /// </summary>
        public EventHandler LoadClicked;

        /// <summary>
        /// Túl sok kulcs eseménye.
        /// </summary>
        public EventHandler TooManyKeys;
        #endregion

        #region Constructors
        /// <summary>
        /// BuildTreeViewModel példányosítása
        /// </summary>
        /// <param name="order">Fokszám</param>
        /// <param name="dataAccess">Adatelér</param>
        public BuildTreeViewModel(Int32 order, IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
            IsEnabled = true;
            _order = order;
            _model = new BpTree(order);
            

            SetupCanvas(_order);
            Update(_model.Root, _model.MaxHeight);

            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromMilliseconds(_speedFactor);
            _timer.Tick += BTAdvanceTime;
            Speed = "1.0 Mp/lépés";
            OnPropertyChanged("Speed");


            _operationIndex = 1;
            _operationsList = new List<String>();
            OperationsList = new BindingList<String>();

            SavePictureCommand = new DelegateCommand(new Action<object>(o => OnSavePictureClicked()));
            NewTreeCommand = new DelegateCommand(new Action<object>(o => OnNewTreeClicked()));
            InsertCommand = new DelegateCommand(new Action<object>(o => OnInsertClicked()));
            DeleteCommand = new DelegateCommand(new Action<object>(o => OnDeleteClicked()));
            SearchCommand = new DelegateCommand(new Action<object>(o => OnSearchClicked()));
            FasterCommand = new DelegateCommand(new Action<object>(o => OnFasterClicked()));
            SlowerCommand = new DelegateCommand(new Action<object>(o => OnSlowerClicked()));
            BackCommand = new DelegateCommand(new Action<object>(o => OnBuildTreeToMain()));
            SaveCommand = new DelegateCommand(new Action<object>(o => OnSaveClicked()));
            LoadCommand = new DelegateCommand(new Action<object>(o => OnLoadClicked()));
        }
        #endregion

        #region Public methods


        /// <summary>
        /// A gyökér lekérdezése
        /// </summary>
        /// <returns>Az aktuális gyökérrel tér vissza</returns>
        public Node GetRoot()
        {
            return _model.Root;
        }
        /// <summary>
        /// Fokszám elkérdezése
        /// </summary>
        /// <returns>Aktuális fokszámmal tér vissza</returns>
        public Int32 GetOrder()
        {
            return _order;
        }
        /// <summary>
        /// A fájl megnyitása, beolvasás
        /// </summary>
        /// <param name="fname">Elérési útvonal</param>
        /// <returns>Kivétel üzenete</returns>
        public String OpenFile(String path)
        {
            try
            {
                LoadTree Tree = Task.Run(() =>
                    _dataAccess.ReadFrom(path)).Result;
                BpTreeOpened(Tree);
            }
            catch(Exception e)
            {
                return e.Message;
            }
            return "";
        }
        #endregion

        #region Event methods


        /// <summary>
		/// A betöltés eseménykiváltása.
		/// </summary>
        private void OnLoadClicked()
        {
            if (LoadClicked != null)
                LoadClicked(this, EventArgs.Empty);
        }

        /// <summary>
		/// A mentés eseménykiváltása.
		/// </summary>
        private void OnSaveClicked()
        {
            if (SaveClicked != null)
                SaveClicked(this, EventArgs.Empty);
        }

        /// <summary>
		/// A képként kimentés eseménykiváltása.
		/// </summary>
        private void OnSavePictureClicked()
        {
            if (SavePictureClicked != null)
                SavePictureClicked(this, EventArgs.Empty);
        }

        /// <summary>
		/// Új fa kérésének eseménykiváltása.
		/// </summary>
        private void OnNewTreeClicked()
        {
            if (NewTreeClicked != null)
                NewTreeClicked(this, EventArgs.Empty);
        }

        /// <summary>
		/// A beszúrás eseménykiváltása.
		/// </summary>
        private void OnInsertClicked()
        {
            if (!(Key > 0 && Key < 10000))
            {
                if (WrongInput != null)
                    WrongInput(this, EventArgs.Empty);
                return;
            }
            if(_model.CheckKey(Key))
            {
                if (WrongKey != null)
                    WrongKey(this, "Már szerepel ez a kulcs a fában!");
                return;
            }
            if(_model.GetKeyCount() > 200)
            {
                if (TooManyKeys != null)
                    TooManyKeys(this, EventArgs.Empty);
                return;
            }
            IsEnabled = false;
            OnPropertyChanged("IsEnabled");
            _currentAction = Model.Action.Insert;
            _operationsList.Reverse();
            _operationsList.Add(_operationIndex++.ToString() +". Beszúrtuk: " + _key);
            _operationsList.Reverse();

            OperationsList = new BindingList<String>(_operationsList);
            OnPropertyChanged("OperationsList");
            _timer.Start();
        }

        /// <summary>
		/// A törlés eseménykiváltása.
		/// </summary>
        private void OnDeleteClicked()
        {
            if (!(Key > 0 && Key < 10000))
            {
                if (WrongInput != null)
                    WrongInput(this, EventArgs.Empty);
                return;
            }
            if (!_model.CheckKey(Key))
            {
                if (WrongKey != null)
                    WrongKey(this, "Nem szerepel a fában ez a kulcs!");
                return;
            }
            IsEnabled = false;
            OnPropertyChanged("IsEnabled");
            _currentAction = Model.Action.Delete;
            _operationsList.Reverse();
            _operationsList.Add(_operationIndex++.ToString() + ". Töröltük: " + _key);
            _operationsList.Reverse();

            OperationsList = new BindingList<String>(_operationsList);
            OnPropertyChanged("OperationsList");
            _timer.Start();
        }

        /// <summary>
		/// A keresés eseménykiváltása.
		/// </summary>
        private void OnSearchClicked()
        {
            if (!(Key > 0 && Key < 10000))
            {
                if (WrongInput != null)
                    WrongInput(this, EventArgs.Empty);
                return;
            }
            if (!_model.CheckKey(Key))
            {
                if (WrongKey != null)
                    WrongKey(this, "Nem szerepel a fában ez a kulcs!");
                return;
            }

            IsEnabled = false;
            OnPropertyChanged("IsEnabled");
            _currentAction = Model.Action.Search;

            _operationsList.Reverse();
            _operationsList.Add(_operationIndex++.ToString() + ". Kerestük: " + _key);
            _operationsList.Reverse();

            OperationsList = new BindingList<String>(_operationsList);
            OnPropertyChanged("OperationsList");
            _timer.Start();
        }
        /// <summary>
		/// A gyorsítás eseménykiváltása.
		/// </summary>
        private void OnFasterClicked()
        {
            if (_speedFactor != 400)
            {
                _speedFactor -= 200;
                _timer.Interval = TimeSpan.FromMilliseconds(_speedFactor);
            }
            Speed = "" + Convert.ToString(_speedFactor / 1000) + "." + Convert.ToString(_speedFactor % 1000 / 100) + " Mp/lépés";
            OnPropertyChanged("Speed");
        }
        /// <summary>
		/// A lassítás eseménykiváltása.
		/// </summary>
        private void OnSlowerClicked()
        {
            if (_speedFactor != 5000)
            {
                _speedFactor += 200;
                _timer.Interval = TimeSpan.FromMilliseconds(_speedFactor);
            }
            Speed = "" + Convert.ToString(_speedFactor / 1000) + "." + Convert.ToString(_speedFactor % 1000 / 100) + " Mp/lépés";
            OnPropertyChanged("Speed");
        }
        /// <summary>
		/// Vissza a menübe eseménykiváltása.
		/// </summary>
        private void OnBuildTreeToMain()
        {
            if (BuildTreeToMain != null)
                BuildTreeToMain(this, EventArgs.Empty);
        }
        #endregion
        #region Private methods

        /// <summary>
        /// Betöltés utáni fa frissítése
        /// </summary>
        /// <param name="tree">A betöltéshez szükséges adatok(a gyökér és a fokszám)</param>
        private void BpTreeOpened(LoadTree tree)
        {
            _order = tree.Order;
            _model = new BpTree(_order);
            _model.Load(tree.Root);

            SetupCanvas(_order);
            OnPropertyChanged("Keys");
            OnPropertyChanged("Arrows");
            OnPropertyChanged("Pointers");
            Update(_model.Root, _model.MaxHeight);
        }
        /// <summary>
        /// Az időzítő álltali léptetés
        /// </summary>
        private void BTAdvanceTime(object sender, EventArgs e)
        {
            Boolean ThereAction = _model.ActionController(_currentAction, Key);

            if(_currentAction != Model.Action.DoAction)
                _currentAction = Model.Action.DoAction;

            if (!_skipFirstAction)
                UpdateSecondCanvas();

            _skipFirstAction = true;

            if (!ThereAction)
            {
                _skipFirstAction = false;
                IsEnabled = true;
                OnPropertyChanged("IsEnabled");
                _timer.Stop();
            }

            Update(_model.Root, _model.MaxHeight);
            Key = 0;
            OnPropertyChanged("Key");
        }
        #endregion
    }
}
