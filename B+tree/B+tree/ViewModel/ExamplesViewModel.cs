﻿using System;

namespace B_tree.ViewModel
{
    public class ExamplesViewModel : ViewModelBase
    {

        #region Properties

        /// <summary>
        /// A példák esetén, az adott példára lépés parancs lekérdezése.
        /// </summary>
        public DelegateCommand Example1Command { get; private set; }

        /// <summary>
        /// A példák esetén, az adott példára lépés parancs lekérdezése.
        /// </summary>
        public DelegateCommand Example2Command { get; private set; }

        /// <summary>
        /// A példák esetén, az adott példára lépés parancs lekérdezése.
        /// </summary>
        public DelegateCommand Example3Command { get; private set; }

        /// <summary>
        /// A példák esetén, a vissza a menübe parancs lekérdezése.
        /// </summary>
        public DelegateCommand BackCommand { get; private set; }
        #endregion


        #region Events

        /// <summary>
        /// Az adott példára lépés eseménye.
        /// </summary>
        public EventHandler GoToExample1;

        /// <summary>
        /// Az adott példára lépés eseménye.
        /// </summary>
        public EventHandler GoToExample2;

        /// <summary>
        /// Az adott példára lépés eseménye.
        /// </summary>
        public EventHandler GoToExample3;

        /// <summary>
        /// Vissza a menübe eseménye.
        /// </summary>
        public EventHandler ExamplesToMain;
        #endregion

        #region Constructors
        /// <summary>
        /// ExamplesViewModel példányosítása
        /// </summary>
        public ExamplesViewModel()
        {
            Example1Command = new DelegateCommand(new Action<object>(o => OnGoToExample1()));
            Example2Command = new DelegateCommand(new Action<object>(o => OnGoToExample2()));
            Example3Command = new DelegateCommand(new Action<object>(o => OnGoToExample3()));
            BackCommand = new DelegateCommand(new Action<object>(o => OnExamplesToMain()));
        }
        #endregion

        #region Event methods

        /// <summary>
		/// Az adott példára lépés eseménykiváltása.
		/// </summary>
        private void OnGoToExample1()
        {
            if (GoToExample1 != null)
                GoToExample1(this, EventArgs.Empty);
        }

        /// <summary>
		/// Az adott példára lépés eseménykiváltása.
		/// </summary>
        private void OnGoToExample2()
        {
            if (GoToExample2 != null)
                GoToExample2(this, EventArgs.Empty);
        }
        /// <summary>
		/// Az adott példára lépés eseménykiváltása.
		/// </summary>
        private void OnGoToExample3()
        {
            if (GoToExample3 != null)
                GoToExample3(this, EventArgs.Empty);
        }

        /// <summary>
		/// Vissza a menübe eseménykiváltása.
		/// </summary>
        private void OnExamplesToMain()
        {
            if (ExamplesToMain != null)
                ExamplesToMain(this, EventArgs.Empty);
        }
        #endregion
    }
}
