﻿using System;
using System.Data;
using System.Windows;

using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;
using System.Windows.Media;

using B_tree.ViewModel;
using B_tree.ViewModel.Tasks;
using B_tree.ViewModel.Examples;
using B_tree.View;
using B_tree.View.Tasks;
using B_tree.View.Examples;
using B_tree.Persistence;

namespace B_tree
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private IDataAccess _dataAccess;

        private MainWindowViewModel _mainWindowVM;
        private BuildTreeViewModel _buildTreeVM;
        private ExamplesViewModel _examplesVM;
        private Example1ViewModel _example1VM;
        private Example2ViewModel _example2VM;
        private Example3ViewModel _example3VM;
        private OrderViewModel _orderVM;
        private TasksViewModel _tasksVM;
        private Task1ViewModel _task1VM;
        private Task2ViewModel _task2VM;
        private Task3ViewModel _task3VM;

        private MainWindow _mainWindowView;
        private BuildTreeWindow _buildTreeWindowView;
        private ExamplesWindow _examplesWindowView;
        private Example1Window _example1WindowView;
        private Example2Window _example2WindowView;
        private Example3Window _example3WindowView;
        private OrderWindow _orderWindowView;
        private TasksWindow _tasksWindowView;
        private Task1Window _task1WindowView;
        private Task2Window _task2WindowView;
        private Task3Window _task3WindowView;
        
        /// <summary>
        /// Alkalmazás példányosítása
        /// </summary>
        public App()
        {
            Startup += App_Startup;
        }

        private void App_Startup(object sender, StartupEventArgs e)
        {
            _dataAccess = new FileDataAccess();
            _mainWindowVM = new MainWindowViewModel();
            _mainWindowView = new MainWindow();

            _mainWindowView.DataContext = _mainWindowVM;

            _mainWindowVM.GoToExamples += VM_MainToExamples;
            _mainWindowVM.GoToTasks += VM_MainToTasks;
            _mainWindowVM.GoToOrder += VM_MainToOrder;

            _mainWindowView.Show();
        }

        #region MainTo
        /// <summary>
        /// Főmenüből példákra lépés eseménykezelője.
        /// </summary>
        private void VM_MainToExamples(object sender, EventArgs e)
        {
            if(_examplesWindowView == null)
            {
                _examplesWindowView = new ExamplesWindow();
                _examplesVM = new ExamplesViewModel();
                _examplesWindowView.DataContext = _examplesVM;

                _examplesVM.GoToExample1 += VM_ExamplesToExample1;
                _examplesVM.GoToExample2 += VM_ExamplesToExample2;
                _examplesVM.GoToExample3 += VM_ExamplesToExample3;
                _examplesVM.ExamplesToMain += VM_ExamplesToMain;
            }

            _examplesWindowView.Show();
            _mainWindowView.Close();
            _mainWindowView = null;
            _mainWindowVM = null;
        }

        /// <summary>
        /// Főmenüből feladatokra lépés eseménykezelője.
        /// </summary>
        private void VM_MainToTasks(object sender, EventArgs e)
        {
            if (_tasksWindowView == null)
            {
                _tasksWindowView = new TasksWindow();
                _tasksVM = new TasksViewModel();
                _tasksWindowView.DataContext = _tasksVM;

                _tasksVM.GoToTask1 += VM_TasksToTask1;
                _tasksVM.GoToTask2 += VM_TasksToTask2;
                _tasksVM.GoToTask3 += VM_TasksToTask3;
                _tasksVM.TasksToMain += VM_TasksToMain;
            }

            _tasksWindowView.Show();
            _mainWindowView.Close();
            _mainWindowView = null;
            _mainWindowVM = null;
        }

        /// <summary>
        /// Főmenüből fokszám megadására lépés eseménykezelője.
        /// </summary>
        private void VM_MainToOrder(object sender, EventArgs e)
        {
            if (_orderVM == null)
            {
                _orderWindowView = new OrderWindow();
                _orderVM = new OrderViewModel();
                _orderWindowView.DataContext = _orderVM;

                _orderVM.OKClicked += VM_OrderToBuildTree;
                _orderVM.CancelClicked += VM_OrderCancelClicked;

            }

            _orderWindowView.Show();
            _mainWindowView.Close();
            _mainWindowView = null;
            _mainWindowVM = null;
        }
        #endregion

        #region ExamplesTo

        /// <summary>
        /// Példákból az adott példára lépés eseménykezelője.
        /// </summary>
        private void VM_ExamplesToExample1(object sender, EventArgs e)
        {
            if (_example1WindowView == null)
            {
                _example1WindowView = new Example1Window();
                _example1VM = new Example1ViewModel();
                _example1WindowView.DataContext = _example1VM;

                _example1VM.Example1ToExamples += VM_Example1ToExamples;
                _example1VM.Example1ToMain += VM_Example1ToMain;
                _example1VM.Example1End += VM_ExampleEnd;
            }

            _example1WindowView.Show();
            _examplesWindowView.Close();
            _examplesWindowView = null;
            _examplesVM = null;
        }

        /// <summary>
        /// Példákból az adott példára lépés eseménykezelője.
        /// </summary>
        private void VM_ExamplesToExample2(object sender, EventArgs e)
        {
            if (_example2WindowView == null)
            {
                _example2WindowView = new Example2Window();
                _example2VM = new Example2ViewModel();
                _example2WindowView.DataContext = _example2VM;

                _example2VM.Example2ToExamples += VM_Example2ToExamples;
                _example2VM.Example2ToMain += VM_Example2ToMain;
                _example2VM.Example2End += VM_ExampleEnd;
            }

            _example2WindowView.Show();
            _examplesWindowView.Close();
            _examplesWindowView = null;
            _examplesVM = null;

        }

        /// <summary>
        /// Példákból az adott példára lépés eseménykezelője.
        /// </summary>
        private void VM_ExamplesToExample3(object sender, EventArgs e)
        {
            if (_example3WindowView == null)
            {
                _example3WindowView = new Example3Window();
                _example3VM = new Example3ViewModel();
                _example3WindowView.DataContext = _example3VM;

                _example3VM.Example3ToExamples += VM_Example3ToExamples;
                _example3VM.Example3ToMain += VM_Example3ToMain;
                _example3VM.Example3End += VM_ExampleEnd;
            }

            _example3WindowView.Show();
            _examplesWindowView.Close();
            _examplesWindowView = null;
            _examplesVM = null;
        }
        #endregion

        #region TasksTo

        /// <summary>
        /// Feladatokból az adott feladatra lépés eseménykezelője.
        /// </summary>
        private void VM_TasksToTask1(object sender, EventArgs e)
        {
            if (_task1WindowView == null)
            {
                _task1WindowView = new Task1Window();
                _task1VM = new Task1ViewModel();
                _task1WindowView.DataContext = _task1VM;

                _task1VM.Task1ToTasks += VM_Task1ToTasks;
                _task1VM.Task1ToMain += VM_Task1ToMain;
                _task1VM.SolutionClicked += VM_TasksSolution;
                _task1VM.TipClicked += VM_TasksTip;
            }

            _task1WindowView.Show();
            _tasksWindowView.Close();
            _tasksWindowView = null;
            _tasksVM = null;
        }

        /// <summary>
        /// Feladatokból az adott feladatra lépés eseménykezelője.
        /// </summary>
        private void VM_TasksToTask2(object sender, EventArgs e)
        {
            if (_task2WindowView == null)
            {
                _task2WindowView = new Task2Window();
                _task2VM = new Task2ViewModel();
                _task2WindowView.DataContext = _task2VM;

                _task2VM.Task2ToTasks += VM_Task2ToTasks;
                _task2VM.Task2ToMain += VM_Task2ToMain;
                _task2VM.SolutionClicked += VM_TasksSolution;
                _task2VM.TipClicked += VM_TasksTip;
            }

            _task2WindowView.Show();
            _tasksWindowView.Close();
            _tasksWindowView = null;
            _tasksVM = null;
        }

        /// <summary>
        /// Feladatokból az adott feladatra lépés eseménykezelője.
        /// </summary>
        private void VM_TasksToTask3(object sender, EventArgs e)
        {
            if (_task3WindowView == null)
            {
                _task3WindowView = new Task3Window();
                _task3VM = new Task3ViewModel();
                _task3WindowView.DataContext = _task3VM;

                _task3VM.Task3ToTasks += VM_Task3ToTasks;
                _task3VM.Task3ToMain += VM_Task3ToMain;
                _task3VM.SolutionClicked += VM_TasksSolution;
                _task3VM.TipClicked += VM_TasksTip;
            }

            _task3WindowView.Show();
            _tasksWindowView.Close();
            _tasksWindowView = null;
            _tasksVM = null;
        }

        #endregion

        #region OrderTo
        /// <summary>
        /// Fokszám megadásáról a faépítésére lépés eseménykezelője.
        /// </summary>
        private void VM_OrderToBuildTree(object sender, Boolean numberIsOK)
        {
            if (!numberIsOK)
            {
                MessageBox.Show("A megadott fokszám nem megfelelő. Az elfogadott fokszám: 4 és 10 közötti egész számok.",
                    "Helytelen fokszám", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (_buildTreeWindowView == null)
                {
                    _buildTreeWindowView = new BuildTreeWindow();
                    _buildTreeVM = new BuildTreeViewModel(_orderVM.Order, _dataAccess);
                    _buildTreeWindowView.DataContext = _buildTreeVM;

                    _buildTreeVM.SavePictureClicked += VM_SavePictureClicked;
                    _buildTreeVM.NewTreeClicked += VM_NewTreClicked;
                    _buildTreeVM.BuildTreeToMain += VM_BuildTreeToMain;
                    _buildTreeVM.WrongInput += VM_WrongInput;
                    _buildTreeVM.WrongKey += VM_WrongKey;
                    _buildTreeVM.SaveClicked += VM_SaveClicked;
                    _buildTreeVM.LoadClicked += VM_LoadClicked;
                    _buildTreeVM.TooManyKeys += VM_TooManyKeys;
                }
                
                _buildTreeWindowView.Show();
                _orderWindowView.Close();
                _orderWindowView = null;
                _orderVM = null;
            }
        }

        /// <summary>
        /// Fokszám megadásáról a főmenübe visszalépés eseménykezelője.
        /// </summary>
        private void VM_OrderCancelClicked(object sender, EventArgs e)
        {
            if(_mainWindowView == null)
            {
                _mainWindowVM = new MainWindowViewModel();
                _mainWindowView = new MainWindow();

                _mainWindowView.DataContext = _mainWindowVM;

                _mainWindowVM.GoToExamples += VM_MainToExamples;
                _mainWindowVM.GoToTasks += VM_MainToTasks;
                _mainWindowVM.GoToOrder += VM_MainToOrder;
            }
           

            _mainWindowView.Show();
            _orderWindowView.Close();
            _orderWindowView = null;
            _orderVM = null;
        }
        #endregion

        #region BackToMain

        /// <summary>
        /// Faépítésből vissza a főmenübe eseménykezelője.
        /// </summary>
        private void VM_BuildTreeToMain(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_mainWindowView == null)
                {
                    _mainWindowVM = new MainWindowViewModel();
                    _mainWindowView = new MainWindow();

                    _mainWindowView.DataContext = _mainWindowVM;

                    _mainWindowVM.GoToExamples += VM_MainToExamples;
                    _mainWindowVM.GoToTasks += VM_MainToTasks;
                    _mainWindowVM.GoToOrder += VM_MainToOrder;
                }


                _mainWindowView.Show();
                _buildTreeWindowView.Close();
                _buildTreeWindowView = null;
                _buildTreeVM = null;
            }
            else return;
            
        }

        /// <summary>
        /// Adott példáról vissza a főmenübe eseménykezelője.
        /// </summary>
        private void VM_Example1ToMain(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_mainWindowView == null)
                {
                    _mainWindowVM = new MainWindowViewModel();
                    _mainWindowView = new MainWindow();

                    _mainWindowView.DataContext = _mainWindowVM;

                    _mainWindowVM.GoToExamples += VM_MainToExamples;
                    _mainWindowVM.GoToTasks += VM_MainToTasks;
                    _mainWindowVM.GoToOrder += VM_MainToOrder;
                }


                _mainWindowView.Show();
                _example1WindowView.Close();
                _example1WindowView = null;
                _example1VM = null;
            }
            else return;
        }

        /// <summary>
        /// Adott példáról vissza a főmenübe eseménykezelője.
        /// </summary>
        private void VM_Example2ToMain(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_mainWindowView == null)
                {
                    _mainWindowVM = new MainWindowViewModel();
                    _mainWindowView = new MainWindow();

                    _mainWindowView.DataContext = _mainWindowVM;

                    _mainWindowVM.GoToExamples += VM_MainToExamples;
                    _mainWindowVM.GoToTasks += VM_MainToTasks;
                    _mainWindowVM.GoToOrder += VM_MainToOrder;
                }


                _mainWindowView.Show();
                _example2WindowView.Close();
                _example2WindowView = null;
                _example2VM = null;
            }
            else return;

        }

        /// <summary>
        /// Adott példáról vissza a főmenübe eseménykezelője.
        /// </summary>
        private void VM_Example3ToMain(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_mainWindowView == null)
                {
                    _mainWindowVM = new MainWindowViewModel();
                    _mainWindowView = new MainWindow();

                    _mainWindowView.DataContext = _mainWindowVM;

                    _mainWindowVM.GoToExamples += VM_MainToExamples;
                    _mainWindowVM.GoToTasks += VM_MainToTasks;
                    _mainWindowVM.GoToOrder += VM_MainToOrder;
                }


                _mainWindowView.Show();
                _example3WindowView.Close();
                _example3WindowView = null;
                _example3VM = null;
            }
            else return;

        }

        /// <summary>
        /// Adott feladatról vissza a főmenübe eseménykezelője.
        /// </summary>
        private void VM_Task1ToMain(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_mainWindowView == null)
                {
                    _mainWindowVM = new MainWindowViewModel();
                    _mainWindowView = new MainWindow();

                    _mainWindowView.DataContext = _mainWindowVM;

                    _mainWindowVM.GoToExamples += VM_MainToExamples;
                    _mainWindowVM.GoToTasks += VM_MainToTasks;
                    _mainWindowVM.GoToOrder += VM_MainToOrder;
                }


                _mainWindowView.Show();
                _task1WindowView.Close();
                _task1WindowView = null;
                _task1VM = null;
            }
            else return;

        }

        /// <summary>
        /// Adott feladatról vissza a főmenübe eseménykezelője.
        /// </summary>
        private void VM_Task2ToMain(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_mainWindowView == null)
                {
                    _mainWindowVM = new MainWindowViewModel();
                    _mainWindowView = new MainWindow();

                    _mainWindowView.DataContext = _mainWindowVM;

                    _mainWindowVM.GoToExamples += VM_MainToExamples;
                    _mainWindowVM.GoToTasks += VM_MainToTasks;
                    _mainWindowVM.GoToOrder += VM_MainToOrder;
                }


                _mainWindowView.Show();
                _task2WindowView.Close();
                _task2WindowView = null;
                _task2VM = null;
            }
            else return;

        }

        /// <summary>
        /// Adott feladatról vissza a főmenübe eseménykezelője.
        /// </summary>
        private void VM_Task3ToMain(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_mainWindowView == null)
                {
                    _mainWindowVM = new MainWindowViewModel();
                    _mainWindowView = new MainWindow();

                    _mainWindowView.DataContext = _mainWindowVM;

                    _mainWindowVM.GoToExamples += VM_MainToExamples;
                    _mainWindowVM.GoToTasks += VM_MainToTasks;
                    _mainWindowVM.GoToOrder += VM_MainToOrder;
                }


                _mainWindowView.Show();
                _task3WindowView.Close();
                _task3WindowView = null;
                _task3VM = null;
            }
            else return;

        }

        /// <summary>
        /// Feladatoktól vissza a főmenübe eseménykezelője.
        /// </summary>
        private void VM_TasksToMain(object sender, EventArgs e)
        {
            if (_mainWindowView == null)
            {
                _mainWindowVM = new MainWindowViewModel();
                _mainWindowView = new MainWindow();

                _mainWindowView.DataContext = _mainWindowVM;

                _mainWindowVM.GoToExamples += VM_MainToExamples;
                _mainWindowVM.GoToTasks += VM_MainToTasks;
                _mainWindowVM.GoToOrder += VM_MainToOrder;
            }


            _mainWindowView.Show();
            _tasksWindowView.Close();
            _tasksWindowView = null;
            _tasksVM = null;
        }

        /// <summary>
        /// Példáktól vissza a főmenübe eseménykezelője.
        /// </summary>
        private void VM_ExamplesToMain(object sender, EventArgs e)
        {
            if (_mainWindowView == null)
            {
                _mainWindowVM = new MainWindowViewModel();
                _mainWindowView = new MainWindow();

                _mainWindowView.DataContext = _mainWindowVM;

                _mainWindowVM.GoToExamples += VM_MainToExamples;
                _mainWindowVM.GoToTasks += VM_MainToTasks;
                _mainWindowVM.GoToOrder += VM_MainToOrder;
            }


            _mainWindowView.Show();
            _examplesWindowView.Close();
            _examplesWindowView = null;
            _examplesVM = null;
        }
        #endregion

        #region BackToExamples

        /// <summary>
        /// Adott példáról vissza a példákhoz eseménykezelője.
        /// </summary>
        private void VM_Example1ToExamples(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_examplesWindowView == null)
                {
                    _examplesVM = new ExamplesViewModel();
                    _examplesWindowView = new ExamplesWindow();

                    _examplesWindowView.DataContext = _examplesVM;

                    _examplesVM.GoToExample1 += VM_ExamplesToExample1;
                    _examplesVM.GoToExample2 += VM_ExamplesToExample2;
                    _examplesVM.GoToExample3 += VM_ExamplesToExample3;
                    _examplesVM.ExamplesToMain += VM_ExamplesToMain;
                }


                _examplesWindowView.Show();
                _example1WindowView.Close();
                _example1WindowView = null;
                _example1VM = null;
            }
            else return;
        }

        /// <summary>
        /// Adott példáról vissza a példákhoz eseménykezelője.
        /// </summary>
        private void VM_Example2ToExamples(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_examplesWindowView == null)
                {
                    _examplesVM = new ExamplesViewModel();
                    _examplesWindowView = new ExamplesWindow();

                    _examplesWindowView.DataContext = _examplesVM;

                    _examplesVM.GoToExample1 += VM_ExamplesToExample1;
                    _examplesVM.GoToExample2 += VM_ExamplesToExample2;
                    _examplesVM.GoToExample3 += VM_ExamplesToExample3;
                    _examplesVM.ExamplesToMain += VM_ExamplesToMain;
                }


                _examplesWindowView.Show();
                _example2WindowView.Close();
                _example2WindowView = null;
                _example2VM = null;
            }
            else return;

        }

        /// <summary>
        /// Adott példáról vissza a példákhoz eseménykezelője.
        /// </summary>
        private void VM_Example3ToExamples(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_examplesWindowView == null)
                {
                    _examplesVM = new ExamplesViewModel();
                    _examplesWindowView = new ExamplesWindow();

                    _examplesWindowView.DataContext = _examplesVM;

                    _examplesVM.GoToExample1 += VM_ExamplesToExample1;
                    _examplesVM.GoToExample2 += VM_ExamplesToExample2;
                    _examplesVM.GoToExample3 += VM_ExamplesToExample3;
                    _examplesVM.ExamplesToMain += VM_ExamplesToMain;
                }


                _examplesWindowView.Show();
                _example3WindowView.Close();
                _example3WindowView = null;
                _example3VM = null;
            }
            else return;

        }
        #endregion

        #region BackToTasks
        /// <summary>
        /// Adott feladatról vissza a feladatokhoz eseménykezelője.
        /// </summary>
        private void VM_Task1ToTasks(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_tasksWindowView == null)
                {
                    _tasksVM = new TasksViewModel();
                    _tasksWindowView = new TasksWindow();

                    _tasksWindowView.DataContext = _tasksVM;

                    _tasksVM.GoToTask1 += VM_TasksToTask1;
                    _tasksVM.GoToTask2 += VM_TasksToTask2;
                    _tasksVM.GoToTask3 += VM_TasksToTask3;
                    _tasksVM.TasksToMain += VM_TasksToMain;
                }


                _tasksWindowView.Show();
                _task1WindowView.Close();
                _task1WindowView = null;
                _task1VM = null;
            }
            else return;

        }

        /// <summary>
        /// Adott feladatról vissza a feladatokhoz eseménykezelője.
        /// </summary>
        private void VM_Task2ToTasks(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_tasksWindowView == null)
                {
                    _tasksVM = new TasksViewModel();
                    _tasksWindowView = new TasksWindow();

                    _tasksWindowView.DataContext = _tasksVM;

                    _tasksVM.GoToTask1 += VM_TasksToTask1;
                    _tasksVM.GoToTask2 += VM_TasksToTask2;
                    _tasksVM.GoToTask3 += VM_TasksToTask3;
                    _tasksVM.TasksToMain += VM_TasksToMain;
                }


                _tasksWindowView.Show();
                _task2WindowView.Close();
                _task2WindowView = null;
                _task2VM = null;
            }
            else return;
        }

        /// <summary>
        /// Adott feladatról vissza a feladatokhoz eseménykezelője.
        /// </summary>
        private void VM_Task3ToTasks(object sender, EventArgs e)
        {
            String msg = "Biztosan ki akar lépni?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_tasksWindowView == null)
                {
                    _tasksVM = new TasksViewModel();
                    _tasksWindowView = new TasksWindow();

                    _tasksWindowView.DataContext = _tasksVM;

                    _tasksVM.GoToTask1 += VM_TasksToTask1;
                    _tasksVM.GoToTask2 += VM_TasksToTask2;
                    _tasksVM.GoToTask3 += VM_TasksToTask3;
                    _tasksVM.TasksToMain += VM_TasksToMain;
                }


                _tasksWindowView.Show();
                _task3WindowView.Close();
                _task3WindowView = null;
                _task3VM = null;
            }
            else return;

        }
        #endregion


        /// <summary>
        /// Képként mentés eseménykezelője.
        /// </summary>
        private void VM_SavePictureClicked(object sender, EventArgs e)
        {
            FrameworkElement element = _buildTreeWindowView.CanvasTree;
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)element.ActualWidth, (int)element.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(element);

            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bmp));

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Image(*.jpg)|*.jpg|(*.*|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                FileStream fs = new FileStream(saveFileDialog.FileName, FileMode.Create);
                encoder.Save(fs);
                fs.Close();
            }
        }

        /// <summary>
        /// Visszajelzés a megoldásról eseménykezelője.
        /// </summary>
        private void VM_TasksSolution(object sender, String Solution)
        {
            MessageBox.Show(Solution,
                "", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        /// <summary>
        /// Tipp helyességének visszajelzése eseménykezelője.
        /// </summary>
        private void VM_TasksTip(object sender, Boolean TipIsOk)
        {
            if (!TipIsOk)
            {
                MessageBox.Show("A válasz helytelen!",
                    "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Gratulálok, a válasz helyes!",
                    "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        /// <summary>
        /// Új fa eseménykezelője.
        /// </summary>
        private void VM_NewTreClicked(object sender, EventArgs e)
        {
            String msg = "Biztosan új fát szeretne?";
            MessageBoxResult result = MessageBox.Show(msg, "Kilépés", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                if (_orderVM == null)
                {
                    _orderWindowView = new OrderWindow();
                    _orderVM = new OrderViewModel();
                    _orderWindowView.DataContext = _orderVM;

                    _orderVM.OKClicked += VM_OrderToBuildTree;
                    _orderVM.CancelClicked += VM_OrderCancelClicked;

                }

                _orderWindowView.Show();
                _buildTreeWindowView.Close();
                _buildTreeWindowView = null;
                _buildTreeVM = null;
            }
            else return;
        }

        /// <summary>
        /// Vége a példának eseménykezelője.
        /// </summary>
        private void VM_ExampleEnd(object sender, EventArgs e)
        {
            MessageBox.Show("A példának vége van",
                    "", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        /// <summary>
        /// Rossz kulcs megadásának eseménykezelője.
        /// </summary>
        private void VM_WrongKey(object sender, String s)
        {
            MessageBox.Show(s,
                    "", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        /// <summary>
        /// Hibás kulcs megadásának eseménykezelője.
        /// </summary>
        private void VM_WrongInput(object sender, EventArgs e)
        {
            MessageBox.Show("A kulcs 1 és 9999 között szám lehet!",
                    "", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// Mentés eseménykezelője.
        /// </summary>
        private async void VM_SaveClicked(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Title = "B+fa mentése";
                saveFileDialog.Filter = "bptree|*.bptree";
                if (saveFileDialog.ShowDialog() == true)
                {
                    try
                    {
                        await _dataAccess.SaveTo(saveFileDialog.FileName, _buildTreeVM.GetRoot(), _buildTreeVM.GetOrder());
                    }
                    catch (DataException)
                    {
                        MessageBox.Show("A fa mentése sikertelen!\nHibás az elérési út, vagy a könyvtár nem írható.", "Hiba!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "B+Tree", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Betöltés eseménykezelője.
        /// </summary>
        private void VM_LoadClicked(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Title = "B+ fa betöltése";
                openFileDialog.Filter = "bptree|*.bptree";

                if (openFileDialog.ShowDialog() == true)
                {
                    String exep = _buildTreeVM.OpenFile(openFileDialog.FileName);
                    if (exep != "")
                    {
                        MessageBox.Show(exep, "Sikertelen betöltés",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "B+ fa", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Túl sok kulcs eseménykezelője
        /// </summary>
        private void VM_TooManyKeys(object sender, EventArgs e)
        {
            MessageBox.Show("A program esetében limitálva van a kulcsok száma, ennél több kulcsot nem szúrhat be.",
                    "", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }

}
