﻿using System;

namespace B_tree.Persistence
{
    public class LoadTree
    {

        /// <summary>
        /// A gyökér lekérdezése, vagy beállítása.
        /// </summary>
        public Node Root { get; set; }

        /// <summary>
        /// A fokszám lekérdezése, vagy beállítása.
        /// </summary>
        public Int32 Order { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <param name="order"></param>
        public LoadTree(Node root, Int32 order)
        {
            Root = root;
            Order = order;
        }
    }
}
