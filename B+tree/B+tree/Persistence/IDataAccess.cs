﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace B_tree.Persistence
{
    /// <summary>
    /// Fájl kezelő felület
    /// </summary>
    public interface IDataAccess
    {
        /// <summary>
        /// Fájl betöltésa
        /// </summary>
        /// <param name="path">A fájl elérési útvonala</param>
        /// <returns>A fájlból beolvasott fa</returns>
        Task<LoadTree> ReadFrom(String path);


        /// <summary>
        /// Fájlba mentés
        /// </summary>
        /// <param name="path">Elérési útvonal</param>
        /// <param name="root">A fa gyökere, amit majd aztán mentünk</param>
        /// <param name="order">Fokszám</param>
        Task SaveTo(String path, Node root, Int32 order);
    }
}
