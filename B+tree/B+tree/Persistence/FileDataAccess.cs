﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace B_tree.Persistence
{
    /// <summary>
    /// Fájlkezelő típusa
    /// </summary>
    public class FileDataAccess : IDataAccess
    {
        
        private String _bracketRepresentation; // Zárójeles alak
        private Int32 _keyCount; // kulcsok száma a fában


        /// <summary>
        /// Fájlból betöltés
        /// </summary>
        /// <param name="path"> Elérési útvonal</param>
        /// <returns>A fa betöltéséhez szükséges adatok</returns>
        public async Task<LoadTree> ReadFrom(String path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    _keyCount = 0;
                    String line = await reader.ReadLineAsync();
                    Int32[] initData = Array.ConvertAll(line.Split(' '), Int32.Parse);

                    if (initData.Length != 1)
                        throw new IOException("A fájl szerkezete nem megfelelő!");
                    if (initData[0] < 4 || initData[0] > 10)
                        throw new IOException("A fokszám nem megfelelő");

                    Int32 order = initData[0];

                    Node root = new Node();
                    List<char> bracket = new List<char>();

                    line = await reader.ReadLineAsync();
                    if (line == null)
                        throw new IOException("A zárójeles alak hiányzik");

                    Int32 currentHeight = 1;
                    Int32 key = 0;
                    Int32 commaCount = 0;
                    String number ="";
                    Node curNode = root;
                    for(Int32 i = 0; i< line.Length;i++)
                    {
                        char c = line[i];
                        if( c != '(' && i == 0)
                            throw new IOException("A zárójeles alak hibás");
                        if (c != ')' && i == line.Length-1)
                            throw new IOException("A zárójeles alak hibás");
                        if (c == '(')
                            bracket.Add(c);
                        if (c == ')')
                        {
                            if (bracket.Count == 0)
                                throw new IOException("A zárójeles alak hibás");
                            bracket.RemoveAt(0);
                        }
                        if(i == line.Length-1 && bracket.Count != 0)
                            throw new IOException("A zárójeles alak hibás");
                        if(c == '(' && i != 0)
                        {
                            if (number.Length != 0)
                            {
                                key = Int32.Parse(number);
                                if (key <= 0)
                                    throw new IOException("A kulcsoknak 0-nál nagyobbaknak kell lenniük");
                                curNode.Keys.Add(key);
                                number = "";
                            }
                            Node tempN = new Node();
                            tempN.Parent = curNode;
                            tempN.Height = ++currentHeight;
                            curNode.IsLeaf = false;
                            curNode.Children.Add(tempN);
                            curNode = tempN;
                        }
                        else if (c == ')' && i != line.Length)
                        {
                            if (number.Length != 0)
                            {
                                key = Int32.Parse(number);
                                if(key <=0)
                                    throw new IOException("A kulcsoknak 0-nál nagyobbaknak kell lenniük");
                                curNode.Keys.Add(key);
                                number = "";
                            }
                            if(!root.IsLeaf)
                            {
                                if (curNode.Keys.Count < (curNode.IsLeaf ? order / 2 : (order / 2) - 1) || curNode.Keys.Count > order - 1)
                                    throw new IOException("Nem megfelelő a kulcsok száma");
                                if (!curNode.IsLeaf && (curNode.Children.Count < order / 2 || curNode.Children.Count > order))
                                    throw new IOException("Nem megfelelő a mutatók száma");
                                if (commaCount >= curNode.Keys.Count)
                                    throw new IOException("Nem megfelelő a vesszők száma");
                            }
                            commaCount = 0;
                            curNode = curNode.Parent;
                            currentHeight--;
                        }
                        else if(c != '(' && c != ')' && c != ',')
                        {
                            number += c.ToString();
                        }
                        else if (c ==',')
                        {
                            commaCount++;
                            key = Int32.Parse(number);
                            if (key <= 0)
                                throw new IOException("A kulcsoknak 0-nál nagyobbaknak kell lenniük");
                            curNode.Keys.Add(key);
                            number = "";
                        }
                    }
                    if(!CheckTree(root))
                        throw new IOException("Nem szigorúan növekvően követik egymást a kulcsok vagy hasítókulcsok");

                    SetKeyCount(root);
                    if (_keyCount >= 200)
                        throw new IOException("A program esetében limitálva van a kulcsok száma, 200-nál több kulcs nem lehet a fában.");

                    LoadTree tree = new LoadTree(root, order);

                    return tree;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Fájlba mentés
        /// </summary>
        /// <param name="path">Elérési  útvonal</param>
        /// <param name="root">A fa gyökere</param>
        /// <param name="order">Fokszám</param>
        public async Task SaveTo(String path, Node root, Int32 order)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(path))
                {
                    String firstLine = order.ToString();
                    await writer.WriteLineAsync(firstLine);
                    await writer.WriteLineAsync(Bracketed(root));
                }
            }
            catch
            {
                throw new IOException("Hiba történt a fájl írása során.");

            }
        }

        /// <summary>
        /// Zárójeles alak beállítása
        /// </summary>
        /// <param name="Root"> A fa gyökere</param>
        /// <returns>Zárójeles alakkal</returns>
        private String Bracketed(Node Root)
        {
            _bracketRepresentation = "";
            ProcessTree(Root);
            return _bracketRepresentation;
        }

        /// <summary>
        /// Rekurzió, feldolgozza a fát
        /// </summary>
        /// <param name="n">Aktuális node </param>
        private void ProcessTree(Node n)
        {
            if (!n.IsLeaf)
            {
                _bracketRepresentation += "(";
                for (Int32 i = 0; i < n.Children.Count; i++)
                {
                    ProcessTree(n.Children[i]);
                    if (i != n.Children.Count - 1)
                        _bracketRepresentation += n.Keys[i].ToString();
                }
                _bracketRepresentation += ")";
                return;
            }
            _bracketRepresentation += "(";
            for (Int32 i = 0; i < n.Keys.Count; i++)
            {
                _bracketRepresentation += n.Keys[i].ToString();
                if (i != n.Keys.Count - 1)
                    _bracketRepresentation += ",";
            }
            _bracketRepresentation += ")";

        }

        /// <summary>
        /// Beállítja a kulcsokat számláló változó értékét.
        /// </summary>
        private void SetKeyCount(Node n)
        {
            if (!n.IsLeaf)
                foreach (Node helpN in n.Children)
                {
                    SetKeyCount(helpN);
                }
            else
                _keyCount += n.Keys.Count;
        }

        /// <summary>
        /// Betöltésnél ellenőrzi, hogy megfelelő-e a fa
        /// </summary>
        /// <param name="n">Az adott node amit vizsgál</param>
        /// <returns>Ha minden rendben true, ha rosszul van valamelyik kulcs akkor false</returns>
        private Boolean CheckTree(Node n)
        {
            Int32 c = 0;
            foreach(Int32 k in n.Keys)
            {
                if (k <= c)
                    return false;
                c = k;
            }
            if(n.Parent != null)
            {
                Node pNode = n.Parent;
                Int32 ind = pNode.Children.IndexOf(n);
                if(pNode.Keys.Count -1 >= ind)
                {
                    foreach (Int32 k in n.Keys)
                    {
                        if (k >= pNode.Keys[ind])
                            return false;
                    }
                }
                if (0 < ind)
                {
                    foreach (Int32 k in n.Keys)
                    {
                        if (k < pNode.Keys[ind-1])
                            return false;
                    }
                }
            }
            if(n.Children.Count != 0)
            {
                foreach(Node cn in n.Children)
                {
                    return CheckTree(cn);
                }
            }
            return true;
        }
    }
}
