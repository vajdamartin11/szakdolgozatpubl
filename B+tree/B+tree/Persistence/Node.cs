﻿using System;
using System.Collections.Generic;

namespace B_tree.Persistence
{
    public class Node
    {
        /// <summary>
        /// A noda levél-e lekérdezése, vagy beállítása
        /// </summary>
        public Boolean IsLeaf { get; set; }

        /// <summary>
        /// A node-t melyik indexen kell színezni lekérdezése, vagy beállítása
        /// </summary>
        public Int32 ColoredIndex { get; set; }

        /// <summary>
        /// A node kulcsainak lekérdezése, vagy beállítása
        /// </summary>
        public List<Int32> Keys { get; set; }

        /// <summary>
        /// A node szülőjének lekérdezése, vagy beállítása
        /// </summary>
        public Node Parent { get; set; }

        /// <summary>
        /// A node gyerekeinek lekérdezése, vagy beállítása
        /// </summary>
        public List<Node> Children { get; set; }

        /// <summary>
        /// A magasságának lekérdezése, vagy beállítása
        /// </summary>
        public Int32 Height { get; set; }

        /// <summary>
        /// A nodehoz aktuálisan kell-e "animációt" végezni lekérdezése, vagy beállítása
        /// </summary>
        public Boolean Animation { get; set; }

        /// <summary>
        /// Node példányosítása
        /// </summary>
        public Node()
        {
            IsLeaf = true ;
            ColoredIndex = -1;
            Keys = new List<int>();
            Parent = null;
            Children = new List<Node>();
            Height = 1;
            Animation = false;
        }
        /// <summary>
        /// Node másolása
        /// </summary>
        /// <param name="node">lemásolandó node</param>
        public Node(Node node)
        { 
            Keys = node.Keys == null ? new List<Int32>() : new List<Int32>(node.Keys); 
            Parent = node.Parent == null ? null : new Node(node.Parent); 
            Children = node.Children == null ? new List<Node>() : new List<Node>(node.Children);
            IsLeaf = node.IsLeaf;
            Height = node.Height;
            ColoredIndex = node.ColoredIndex;
            Animation = node.Animation;
        }
    }
}