﻿using System;
using System.Collections.Generic;
using B_tree.Persistence;

namespace B_tree.Model
{
    public enum Action { DoAction , Insert, Delete, Split, Merge, Search }
    public class BpTree
    {
        #region Private data members
        private Int32 _maximumKeys; // maximális kulcs mennyiség ami lehet egy node-n
        private Int32 _minimumKeys; // minimális kulcs mennyiség, aminek lennie kell egy node-n
        private Int32 _key; // Kulcs amivel dolgozzunk adott esetben
        private Node _helpNode; // segéd Node, beszúrás, törlések után, hogy melyik node-nál tartottunk
        private Node _helpRoot; // Ezen a gyökeren végezzük el előbb a műveletek
        private Int32 _maxHeightHelp; // A 2. fa magassága
        private String _bracketRepresentation; // zárójeles alak
        private Int32 _countSplit; // A vágások száma
        private List<Action> _actions; // műveletek/akciók sorrendje
        private Int32 _nodeIndex; // segéd index
        private List<Node> _leavesList; // levelek listája
        private Int32 _keyCount; // kulcsok száma a fában

        #endregion

        #region Properties
        /// <summary>
        /// Gyökér lekérdezése, vagy beállítása
        /// </summary>
        public Node Root { get; private set; }
        /// <summary>
        /// Fa magasságnak lekérdezése, vagy beállítása
        /// </summary>
        public Int32 MaxHeight { get; private set; }
        #endregion

        #region Constructors
        /// <summary>
        /// BpTree példányosítása
        /// </summary>
        /// <param name="order">Fokszám</param>
        public BpTree(Int32 order)
        {
            _countSplit = 0;
            _actions = new List<Action>();
            MaxHeight = 1;
            _maximumKeys = order - 1;
            _minimumKeys = order / 2;
            Root = new Node();
            _helpNode = new Node();
            _helpRoot = new Node(Root);
            _maxHeightHelp = 1;
            _leavesList = new List<Node>();
        }

        #endregion

        #region Public methods
        /// <summary>
        /// _nodeIndex lekérdezése, amivel a csúcsok, levelek számát kapjuk meg
        /// </summary>
        /// <returns>_nodeIndex</returns>
        public Int32 GetNodeIndex()
        {
            _nodeIndex = 1;
            SetNodeIndex(Root);
            return _nodeIndex;
        }

        /// <summary>
        /// index alapján visszatérünk egy node kulcsainak zárójeles alakjával
        /// </summary>
        /// <param name="index">Node indexe amit keresünk</param>
        /// <returns>Zárójeles alak</returns>
        public String GetNodeWithIndex(Int32 index)
        {
            _nodeIndex = 0;
            Node n = SearchNodeWithIndex(index, Root);
            if (n != null)
            {
                return GetNodeBracketedFormWithNode(n);
            }
            return "()";
        }

        public List<Node> GetLeaves()
        {
            SetLeavesList(Root);
            return _leavesList;
        }
        
        public Int32 GetLeafIndexAfterInsert(Int32 _key)
        {
            SetLeavesList(Root);
            Node n = SearchKey(_key);
            Int32 ind =-1;

            if(n != null)
            {
                ind = _leavesList.IndexOf(n);
            }

            return ind+1;
        }
        
        /// <summary>
        /// Beállítja a fát, egy feladathoz, hogy csak egy másik fa indexei legyen láthatóak rajta
        /// </summary>
        /// <param name="height">A másik fa magassága</param>
        /// <param name="copyTree">A fa gyökere amit másolni szeretnénk</param>
        public void TaskTree(Int32 height, Node copyTree, Int32 ind)
        {
            Root = new Node();
            _nodeIndex = 1;
            if (ind == _nodeIndex)
            {
                Root.ColoredIndex = -3;
            }
            _nodeIndex++;
            if (height == 1)
                return;
            BuildTaskTree(height, 1, Root, copyTree,ind);
        }

        /// <summary>
        /// Adott kulcs szerepel-e a fában
        /// </summary>
        /// <param name="key">Kulcs amit keresünk</param>
        /// <returns>Ha megtalálja true, amúgy false</returns>
        public Boolean CheckKey(Int32 key)
        {
            Node helpNode = SearchKey(key);
            return helpNode == null ? false : true;
        }

        /// <summary>
        /// Zárójeles alak lekérdezése
        /// </summary>
        /// <returns>Zárójeles alak</returns>
        public String Bracketed()
        {
            _bracketRepresentation = "";
            ProcessNode(Root);
            return _bracketRepresentation;
        }


        /// <summary>
        /// A vágások számának lekérdezése
        /// </summary>
        /// <returns>Vágások száma</returns>
        public Int32 GetSplitCount()
        {
            return _countSplit;
        }

        /// <summary>
        /// Levelek zárójeles alakjai balról jobbra
        /// </summary>
        /// <returns>zárójeles alak</returns>
        public String GetLeavesBracketedForm()
        {
            _bracketRepresentation = "";

            SetLeavesBracketedForm(Root);

            return _bracketRepresentation;
        }

        /// <summary>
        /// Visszatér, hogy mennyi kulcs van a fában.
        /// </summary>
        public Int32 GetKeyCount()
        {
            _keyCount = 0;
            SetKeyCount(Root);
            return _keyCount;
        }

        /// <summary>
        /// Node alapján kulcsok zárójeles alakjának lekérdezése
        /// </summary>
        /// <param name="n">Adott node ami érdekel minket</param>
        /// <returns>Zárójeles alak</returns>
        public String GetNodeBracketedFormWithNode(Node n)
        {
            String _bracketRepresentationNode = "";
            _bracketRepresentationNode = "";
            _bracketRepresentationNode += "(";
            for (Int32 i = 0; i < n.Keys.Count; i++)
            {
                _bracketRepresentationNode += n.Keys[i].ToString();
                if (i != n.Keys.Count - 1)
                    _bracketRepresentationNode += ",";
            }
            _bracketRepresentationNode += ")";
            return _bracketRepresentationNode;
        }
        /// <summary>
        /// Fájl betöltés esetén beállítja az értékeket
        /// </summary>
        /// <param name="NewRoot">Az új gyökér</param>
        public void Load(Node NewRoot)
        {
            Root = NewRoot;
            _helpRoot = new Node();
            SetHelpRoot(Root, _helpRoot);
            parentChange(_helpRoot);
            SetMaxHegiht(Root);
        }
        
        /// <summary>
        /// A kapott akció szerint hívja a megfelelő metódusokat
        /// </summary>
        /// <param name="action">Akció</param>
        /// <param name="key">kulcs, amivel a műveletet végeznénk</param>
        /// <returns>Trueval tér vissza, ha van még action, false ha már nincs</returns>
        public Boolean ActionController(Action action, Int32 key)
        {
            ResetAnimation(Root);
            if (key != 0)
                _key = key;
            
            switch (action)
            {
                case Action.Insert:
                    ColoredIndexReset(Root);
                    ActionsWhenInsert(key);
                    if (_actions.Count == 0)
                        return false;
                    break;
                case Action.Delete:
                    ColoredIndexReset(Root);
                    ActionsWhenDelete(key);
                    if (_actions.Count == 0)
                        return false;
                    break;
                case Action.Search:
                    ColoredIndexReset(Root);
                    SearchKey(key);
                    return false;
                case Action.DoAction:
                    if (_actions.Count != 0)
                        DoAnAction();
                    else
                        return false;
                    break;
            }
            return true;
        }
        #endregion

        #region Action methods
        /// <summary>
        /// Beszúráshoz végzi el a műveleteket és adja hozzá az akciókat
        /// </summary>
        /// <param name="key">Kulcs amivel a műveletet végezzük</param>
        private void ActionsWhenInsert(Int32 key)
        {
            Node tempNode = Insert(key, _helpRoot);
            if (tempNode != null)
            {
                _actions.Add(Action.Insert);
                while(tempNode.Keys.Count > _maximumKeys)
                {
                    _countSplit++;
                    if (tempNode == _helpRoot)
                    {
                        Node newRoot = new Node();
                        newRoot.IsLeaf = false;
                        newRoot.Height = 1;
                        _helpRoot.Parent = newRoot;
                        _actions.Add(Action.Split);
                        tempNode = Split(tempNode);
                        _helpRoot = newRoot;
                        _maxHeightHelp += 1;
                        HeightModify(_helpRoot, 1);
                        parentChange(_helpRoot);
                    }
                    else
                    {
                        _actions.Add(Action.Split);
                        tempNode = Split(tempNode);
                        parentChange(_helpRoot);
                    }
                }
                DoAnAction();
                return;
            }
        }

        /// <summary>
        /// Törléshez végzi el a műveleteket és adja hozzá az akciókat
        /// </summary>
        /// <param name="key">Kulcs amivel a műveletet végezzük</param>
        private void ActionsWhenDelete(Int32 key)
        {
            Node tempNode = Delete(key, _helpRoot);

            if (tempNode != null)
            {
                _actions.Add(Action.Delete);

                while((tempNode.IsLeaf ? tempNode.Keys.Count : tempNode.Children.Count) < _minimumKeys && _maxHeightHelp != 1)
                {
                    parentChange(_helpRoot);
                    Boolean merged = false;
                    if(tempNode.Parent != null)
                    {
                        List<Node> siblings = tempNode.Parent.Children;
                        Int32 index = siblings.IndexOf(tempNode);

                        merged = GiveKeyToSiblings(siblings, index);

                        if(!merged)
                        {
                            if(siblings.Count-1 == index)
                                Merge(siblings[index-1], siblings[index]);
                            else
                                Merge(siblings[index], siblings[index + 1]);
                        }
                        _actions.Add(Action.Merge);
                        tempNode = tempNode.Parent;
                    }
                    if(tempNode == _helpRoot)
                    {
                        if(_helpRoot.Keys.Count == 0)
                        {
                            HeightModify(_helpRoot, -1);
                            _helpRoot = tempNode.Children[0];
                            _helpRoot.Parent = null;
                            _helpRoot.Height = 1;
                            _maxHeightHelp -= 1;
                        }
                        parentChange(_helpRoot);
                        DoAnAction();
                        return;
                    }
                }
                DoAnAction();
                return;
            }
        }

        
        /// <summary>
        /// _actions-ből veszi ki az első elemet és végzi el az adott műveletet.
        /// </summary>
        private void DoAnAction()
        {
            Action currentAction = _actions[0];
            
            switch (currentAction)
            {
                case Action.Insert:                    
                    _helpNode = Insert(_key, Root);
                    break;
                case Action.Delete:
                    _helpNode = Delete(_key, Root);
                    break;
                case Action.Merge:
                    List<Node> siblings = _helpNode.Parent.Children;
                    Int32 index = siblings.IndexOf(_helpNode);
                    Boolean merged = false;

                    merged = GiveKeyToSiblings(siblings, index);

                    if(!merged)
                    {
                        if (siblings.Count - 1 == index)
                            Merge(siblings[index - 1], siblings[index]);
                        else
                            Merge(siblings[index], siblings[index + 1]);
                    }
                    _helpNode = _helpNode.Parent;
                    if (_helpNode == Root && Root.Keys.Count == 0)
                    {
                        HeightModify(Root, -1);
                        Root = _helpNode.Children[0];
                        Root.Parent = null;
                        Root.Height = 1;
                        MaxHeight -= 1;
                    }
                    parentChange(Root);
                    break;
                case Action.Split:
                    if (_helpNode == Root)
                    {
                        Node newRoot = new Node();
                        newRoot.IsLeaf = false;
                        newRoot.Height = 1;
                        Root.Parent = newRoot;
                        _helpNode = Split(_helpNode);
                        Root = newRoot;
                        MaxHeight += 1;
                        HeightModify(Root, 1);
                        parentChange(Root);
                    }
                    else
                    {
                        _helpNode = Split(_helpNode);
                        parentChange(Root);
                    }
                    break;
                case Action.Search:
                    SearchKey(_key);
                    break;
            }
            _actions.RemoveAt(0);
        }
        #endregion

        #region Operations
        /// <summary>
        /// Visszaadja a node-t amiben a kulcs szerepel
        /// </summary>
        /// <param name="key">Kulcs amit keresünk</param>
        /// <returns>Ha benne van az adott kulcs akkor visszatér a megfelelő node-dal, ha nincs null-lal tér vissza</returns>
        private Node SearchKey(Int32 key)
        {
            if (_key <= 0)
                return null;
            Node tempNode = Root;
            
            while (!tempNode.IsLeaf)
            {
                for(Int32 i = 0; i < tempNode.Keys.Count; i++)
                {
                    if(key < tempNode.Keys[i])
                    {
                        tempNode.ColoredIndex = i;
                        tempNode = tempNode.Children[i];
                        break;
                    }
                    if(i == tempNode.Keys.Count - 1)
                    {
                        tempNode.ColoredIndex = i + 1;
                        tempNode = tempNode.Children[i + 1];
                        break;
                    }
                }
            }
            for (Int32 i = 0; i < tempNode.Keys.Count; i++)
            {
                if (key == tempNode.Keys[i])
                {
                    tempNode.ColoredIndex = i;
                    return tempNode;
                }
            }
            return null;
        }

        /// <summary>
        /// Beszúrja a kulcsot a node-ba
        /// </summary>
        /// <param name="key">Kulcs amit beszúrunk</param>
        /// <param name="root">végig megy a fán és a megfelelő helyre szújra be</param>
        /// <returns>Azzal a Node-dal tér vissza amibe beszúrtuk a kulcsot</returns>
        private Node Insert(Int32 key, Node root)
        {
            if(_key <= 0)
                return null; 
            Node tempNode = root;

            while (!tempNode.IsLeaf)
            {
                for (Int32 i = 0; i < tempNode.Keys.Count; i++)
                {
                    if (key < tempNode.Keys[i])
                    {
                        tempNode = tempNode.Children[i];
                        break;
                    }
                    if (i == tempNode.Keys.Count - 1)
                    {
                        tempNode = tempNode.Children[i + 1];
                        break;
                    }
                }
            }
            if(tempNode.Keys.Contains(key))
                return null;
            tempNode.Keys.Add(key);
            tempNode.Keys.Sort();
            return tempNode;
        }

        private Node Delete(Int32 key, Node root)
        {
            if (_key <= 0)
                return null;
            Node tempNode = root;

            while (!tempNode.IsLeaf)
            {
                for (Int32 i = 0; i < tempNode.Keys.Count; i++)
                {
                    
                    if (key < tempNode.Keys[i])
                    {
                        tempNode = tempNode.Children[i];
                        break;
                    }
                    if (i == tempNode.Keys.Count - 1)
                    {
                        tempNode = tempNode.Children[i + 1];
                        break;
                    }
                }
            }
            if (!tempNode.Keys.Contains(key))
            {
                return null;
            }
            tempNode.Keys.Remove(key);
            return tempNode;
        }

        /// <summary>
        /// Vágja a node-t
        /// </summary>
        /// <param name="n">Az a node amit vágni szeretnénk</param>
        /// <returns>A szülővel tér vissza</returns>
        private Node Split(Node n)
        {
            Node tempNode = n.Parent;
            Node leftNode = new Node();
            Node rightNode = new Node();
            SetupNodesForSplit(n, tempNode, rightNode, leftNode);

            SetKeysInSplit(n, tempNode, rightNode, leftNode);

            SetChildrenInSplit(n, rightNode, leftNode);

            return tempNode;
        }
        /// <summary>
        /// Egyesíti a nodekat
        /// </summary>
        /// <param name="n1">Ebbe a nodeba másoljuk át az értékeket</param>
        /// <param name="n2">Ebből másoljuk át</param>
        /// <returns>A szülővel tér vissza</returns>
        private Node Merge(Node n1, Node n2)
        {
            n1.Animation = true;
            n1.Parent.Animation = true;
            if (n1.IsLeaf && n2.IsLeaf)
            {
                n1.Keys.AddRange(n2.Keys);
                n1.Parent.Keys.RemoveAt(n1.Parent.Children.IndexOf(n1));
                n1.Parent.Children.Remove(n2);
                return n1.Parent;
            }
            n1.Keys.Add(n1.Parent.Keys[n1.Parent.Children.IndexOf(n1)]);
            n1.Parent.Keys.RemoveAt(n1.Parent.Children.IndexOf(n1));
            n1.Keys.AddRange(n2.Keys);
            n1.Children.AddRange(n2.Children);
            n1.Parent.Children.Remove(n2);

            return n1.Parent;
        }
        #endregion

        #region Split help methods

        /// <summary>
        /// Vágás esetén a kezdő beállítások
        /// </summary>
        /// <param name="n"> Amit vágunk</param>
        /// <param name="tempNode">A szülő</param>
        /// <param name="rightNode">Jobb oldali node ami vágás során létrejön</param>
        /// <param name="leftNode">Bal oldali node ami vágás során létrejön</param>
        private void SetupNodesForSplit(Node n, Node tempNode, Node rightNode, Node leftNode)
        {
            tempNode.Keys.Add(n.Keys[n.Keys.Count / 2]);
            tempNode.Keys.Sort();
            leftNode.Parent = tempNode;
            leftNode.IsLeaf = n.IsLeaf;
            leftNode.Height = n.Height;
            leftNode.Animation = true;
            rightNode.Parent = tempNode;
            rightNode.IsLeaf = n.IsLeaf;
            rightNode.Height = n.Height;
            rightNode.Animation = true;
            tempNode.Animation = true;
            tempNode.Children.Remove(n);
        }

        /// <summary>
        /// Szétoszja a kulcsokat
        /// </summary>
        /// <param name="n">Amit vágunk</param>
        /// <param name="tempNode">A szülő</param>
        /// <param name="rightNode">Jobb oldali node ami vágás során létrejön</param>
        /// <param name="leftNode">Bal oldali node ami vágás során létrejön</param>
        private void SetKeysInSplit(Node n, Node tempNode, Node rightNode, Node leftNode)
        {
            List<Node> sortNodes = new List<Node>();
            sortNodes.AddRange(tempNode.Children);
            sortNodes.Add(leftNode);
            sortNodes.Add(rightNode);
            tempNode.Children.Clear();
            Node addNode = new Node();
            for (Int32 i = 0; i < n.Keys.Count / 2; i++)
                leftNode.Keys.Add(n.Keys[i]);

            if (n.Keys.Count % 2 == 0)
            {
                for (Int32 i = 0; i < n.Keys.Count / 2; i++)
                    if (n.IsLeaf || (!n.IsLeaf && i != 0))
                        rightNode.Keys.Add(n.Keys[i + n.Keys.Count / 2]);
            }
            else
            {
                for (Int32 i = 0; i <= n.Keys.Count / 2; i++)
                    if (n.IsLeaf || (!n.IsLeaf && i != 0))
                        rightNode.Keys.Add(n.Keys[i + n.Keys.Count / 2]);
            }
            while (sortNodes.Count != 0)
            {
                addNode = sortNodes[0];
                for (Int32 i = 1; i < sortNodes.Count; i++)
                    if (addNode.Keys[0] > sortNodes[i].Keys[0])
                        addNode = sortNodes[i];

                tempNode.Children.Add(addNode);
                sortNodes.Remove(addNode);
            }
        }

        /// <summary>
        /// ha nem levél a gyerekek elosztása
        /// </summary>
        /// <param name="n">Amit vágunk</param>
        /// <param name="rightNode">Jobb oldali node ami vágás során létrejön</param>
        /// <param name="leftNode">Bal oldali node ami vágás során létrejön</param>
        private void SetChildrenInSplit(Node n, Node rightNode, Node leftNode)
        {
            if (!n.IsLeaf)
            {
                if (n.Children.Count % 2 == 0)
                    for (Int32 i = 0; i < n.Children.Count / 2; i++)
                    {
                        leftNode.Children.Add(n.Children[i]);
                        rightNode.Children.Add(n.Children[i + n.Children.Count / 2]);
                    }
                else
                {
                    for (Int32 i = 0; i <= n.Children.Count / 2; i++)
                        leftNode.Children.Add(n.Children[i]);
                    for (Int32 i = 0; i < n.Children.Count / 2; i++)
                        rightNode.Children.Add(n.Children[i + n.Children.Count / 2 + 1]);
                }
            }
        }
        #endregion

        #region SplitWhenOddChildren

        /// <summary>
        /// A testvér átadásnál használjuk páratlan gyerek esetén, indexelési eltérések vannak az előzőhöz képest.
        /// </summary>
        private Node SplitWhenOddChildren(Node n)
        {
            Node tempNode = n.Parent;
            Node leftNode = new Node();
            Node rightNode = new Node();
            SetupNodesForSplitWhenOddChildren(n, tempNode, rightNode, leftNode);

            SetKeysInSplitWhenOddChildren(n, tempNode, rightNode, leftNode);

            SetChildrenInSplitWhenOddChildren(n, rightNode, leftNode);

            return tempNode;
        }

        private void SetupNodesForSplitWhenOddChildren(Node n, Node tempNode, Node rightNode, Node leftNode)
        {
            tempNode.Keys.Add(n.Keys[(n.Keys.Count / 2)-1]);
            tempNode.Keys.Sort();
            leftNode.Parent = tempNode;
            leftNode.IsLeaf = n.IsLeaf;
            leftNode.Height = n.Height;
            leftNode.Animation = true;
            rightNode.Parent = tempNode;
            rightNode.IsLeaf = n.IsLeaf;
            rightNode.Height = n.Height;
            rightNode.Animation = true;
            tempNode.Animation = true;
            tempNode.Children.Remove(n);
        }

        private void SetKeysInSplitWhenOddChildren(Node n, Node tempNode, Node rightNode, Node leftNode)
        {
            List<Node> sortNodes = new List<Node>();
            sortNodes.AddRange(tempNode.Children);
            sortNodes.Add(leftNode);
            sortNodes.Add(rightNode);
            tempNode.Children.Clear();
            Node addNode = new Node();
            for (Int32 i = 0; i < (n.Keys.Count / 2)-1; i++)
                leftNode.Keys.Add(n.Keys[i]);

            for (Int32 i = 0; i <= n.Keys.Count / 2; i++)
                if (i != 0)
                    rightNode.Keys.Add(n.Keys[i + (n.Keys.Count / 2)-1]);
            
            while (sortNodes.Count != 0)
            {
                addNode = sortNodes[0];
                for (Int32 i = 1; i < sortNodes.Count; i++)
                    if (addNode.Keys[0] > sortNodes[i].Keys[0])
                        addNode = sortNodes[i];

                tempNode.Children.Add(addNode);
                sortNodes.Remove(addNode);
            }
        }

        private void SetChildrenInSplitWhenOddChildren(Node n, Node rightNode, Node leftNode)
        {
            for (Int32 i = 0; i < n.Children.Count / 2; i++)
                leftNode.Children.Add(n.Children[i]);
            for (Int32 i = 0; i <= n.Children.Count / 2; i++)
                rightNode.Children.Add(n.Children[i + n.Children.Count / 2]);
        }
        #endregion

        #region Helper methods

        /// <summary>
        /// Index alapján tér vissza a node-dal
        /// </summary>
        /// <param name="index"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        private Node SearchNodeWithIndex(Int32 index, Node n)
        {
            Node tempN = null;
            _nodeIndex++;
            if (index == _nodeIndex)
            {
                return n;
            }
            for (Int32 i = 0; i < n.Children.Count; i++)
            {
                if (index > _nodeIndex)
                    tempN = SearchNodeWithIndex(index, n.Children[i]);
                if (tempN != null)
                    return tempN;
            }
            return null;
        }

        /// <summary>
        /// Betöltésnél a segéd gyökér beállítása
        /// </summary>
        /// <param name="n1">Az eredeti noda</param>
        /// <param name="n2">A segéd node</param>
        private void SetHelpRoot(Node n1, Node n2)
        {
            n2.Keys.AddRange(n1.Keys);
            n2.Height = n1.Height;
            n2.IsLeaf = n1.IsLeaf;
            for (Int32 i = 0; i < n1.Children.Count; i++)
            {
                n2.Children.Add(new Node());
                SetHelpRoot(n1.Children[i], n2.Children[i]);
            }
        }

        /// <summary>
        /// Zárójeles alak során járja be a fát.
        /// </summary>
        private void ProcessNode(Node n)
        {
            if (!n.IsLeaf)
            {
                _bracketRepresentation += "(";
                for (Int32 i = 0; i < n.Children.Count; i++)
                {
                    ProcessNode(n.Children[i]);
                    if (i != n.Children.Count - 1)
                        _bracketRepresentation += n.Keys[i].ToString();
                }
                _bracketRepresentation += ")";
                return;
            }
            _bracketRepresentation += "(";
            for (Int32 i = 0; i < n.Keys.Count; i++)
            {
                _bracketRepresentation += n.Keys[i].ToString();
                if (i != n.Keys.Count - 1)
                    _bracketRepresentation += ",";
            }
            _bracketRepresentation += ")";

        }

        /// <summary>
        /// Egy feladathoz fa beállítás, a csúcsokon, az indexük jelenik meg
        /// </summary>
        private void BuildTaskTree(Int32 height, Int32 currentHeight, Node n, Node copyTree, Int32 ind)
        {
            currentHeight++;
            n.IsLeaf = false;
            MaxHeight++;
            
            for (Int32 i = 0; i < (copyTree.IsLeaf ? copyTree.Keys.Count : copyTree.Children.Count); i++)
            {
                Node newN = new Node();
                if (height == currentHeight)
                {
                    newN.IsLeaf = true;
                }
                newN.Height = currentHeight;
                newN.Parent = n;
                if (ind == _nodeIndex)
                {
                    newN.ColoredIndex = -3;
                }
                _nodeIndex++;
                
                n.Children.Add(newN);

                if (height > currentHeight)
                    BuildTaskTree(height, currentHeight, newN, copyTree.Children[i], ind);
            }
        }

        private void SetNodeIndex(Node n)
        {
            _nodeIndex++;
            if(!n.IsLeaf)
            {
                foreach (Node helpN in n.Children)
                    SetNodeIndex(helpN);
            }
        }

        /// <summary>
        /// A levelek zárójeles alakját adja vissza
        /// </summary>
        private void SetLeavesBracketedForm(Node n)
        {
            if (!n.IsLeaf)
                foreach (Node helpN in n.Children)
                {
                    SetLeavesBracketedForm(helpN);
                }
            else
                _bracketRepresentation += GetNodeBracketedFormWithNode(n);

        }
        
        /// <summary>
        /// Levelek listáját állítja be
        /// </summary>
        /// <param name="n">Node amin dolgozunk</param>
        private void SetLeavesList(Node n)
        {
            if (!n.IsLeaf)
                foreach (Node helpN in n.Children)
                {
                    SetLeavesList(helpN);
                }
            else
                _leavesList.Add(n);
        }

        /// <summary>
        /// Beállítja a kulcsokat számláló változó értékét.
        /// </summary>
        private void SetKeyCount(Node n)
        {
            if (!n.IsLeaf)
                foreach (Node helpN in n.Children)
                {
                    SetKeyCount(helpN);
                }
            else
                _keyCount += n.Keys.Count;
        }

        /// <summary>
        /// Betöltésnél a fa magasság beállítása
        /// </summary>
        private void SetMaxHegiht(Node n)
        {
            if (!n.IsLeaf)
            {
                SetMaxHegiht(n.Children[0]);
            }
            else
            {
                MaxHeight = n.Height;
                _maxHeightHelp = n.Height;
            }
        }
        /// <summary>
        /// Szülők beállítása a gyerekek alapján
        /// </summary>
        private void parentChange(Node node)
        {
            for (Int32 i = 0; i < node.Children.Count; i++)
            {
                node.Children[i].Parent = node;
                parentChange(node.Children[i]);
            }
        }
        
        /// <summary>
        /// Keresés után reseteli, hogy melyik indexet kellett színezni
        /// </summary>
        /// <param name="node"></param>
        private void ColoredIndexReset(Node node)
        {
            node.ColoredIndex = -1;
            if (node.Children.Count != 0)
            {
                for (Int32 i = 0; i < node.Children.Count; i++)
                {
                    ColoredIndexReset(node.Children[i]);
                }
            }
        }

        /// <summary>
        /// Mikor a magasság csökken vagy nő, az össze node átállítása
        /// </summary>
        private void HeightModify(Node node, Int32 sign)
        {
            if (node.Parent != null)
            {
                node.Height += sign;
            }
            if (node.Children.Count != 0)
            {
                for (Int32 i = 0; i < node.Children.Count; i++)
                {
                    HeightModify(node.Children[i], sign);
                }
            }
        }

        /// <summary>
        /// Éppen kell-e animációt végezni az adott node-ra resetelése
        /// </summary>
        private void ResetAnimation(Node node)
        {
            node.Animation = false;
            if (node.Children.Count != 0)
            {
                for (Int32 i = 0; i < node.Children.Count; i++)
                {
                    ResetAnimation(node.Children[i]);
                }
            }
        }

        /// <summary>
        /// A törlés esetén kulcs átadás a testvérnek.
        /// </summary>
        /// <param name="siblings">Testvérek</param>
        /// <param name="index">testvérek közül melyikben történt a változás</param>
        /// <returns>Tudott-e átadni.</returns>
        private Boolean GiveKeyToSiblings(List<Node> siblings, Int32 index)
        {
            Boolean merged = false;
            if (index == siblings.Count - 1 && (siblings[siblings.Count - 2].IsLeaf ? siblings[siblings.Count - 2].Keys.Count : siblings[siblings.Count - 2].Children.Count) > _minimumKeys)
            {
                Merge(siblings[index - 1], siblings[index]);
                if (siblings[index - 1].Children.Count % 2 == 0 || siblings[index - 1].IsLeaf)
                    Split(siblings[index - 1]);
                else
                    SplitWhenOddChildren(siblings[index - 1]);
                merged = true;
            }

            else if (index == 0 && siblings.Count > 1 && (siblings[1].IsLeaf ? siblings[1].Keys.Count : siblings[1].Children.Count) > _minimumKeys)
            {
                Merge(siblings[0], siblings[1]);
                if (siblings[0].Children.Count % 2 == 0 || siblings[0].IsLeaf)
                    Split(siblings[0]);
                else
                    SplitWhenOddChildren(siblings[0]);
                merged = true;
            }
            else if (siblings.Count >= 3 && index != siblings.Count - 1 && index != 0)
            {
                if ((siblings[index + 1].IsLeaf ? siblings[index + 1].Keys.Count : siblings[index + 1].Children.Count) > _minimumKeys)
                {
                    Merge(siblings[index], siblings[index + 1]);
                    if (siblings[index].Children.Count % 2 == 0 || siblings[index].IsLeaf)
                        Split(siblings[index]);
                    else
                        SplitWhenOddChildren(siblings[index]);
                    merged = true;
                }

                else if ((siblings[index - 1].IsLeaf ? siblings[index - 1].Keys.Count : siblings[index - 1].Children.Count) > _minimumKeys)
                {
                    Merge(siblings[index - 1], siblings[index]);
                    if (siblings[index - 1].Children.Count % 2 == 0 || siblings[index - 1].IsLeaf)
                        Split(siblings[index - 1]);
                    else
                        SplitWhenOddChildren(siblings[index - 1]);
                    merged = true;
                }
            }
            return merged;
        }
        #endregion

    }
}
