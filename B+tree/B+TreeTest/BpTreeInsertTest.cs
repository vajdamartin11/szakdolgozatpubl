﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using B_tree.Model;
using B_tree.Persistence;

namespace B_TreeTest
{

    [TestClass]
    public class BpTreeInsertTest
    {
        BpTree _model;
        List<Int32> _keyList;
        Int32 _key;
        B_tree.Model.Action _currentAction;

        /// <summary>
        /// Beszúrás üres 4 fokú fába
        /// </summary>
        [TestMethod]
        public void InsertKeyEmptyTreeOrder4()
        {
            _model = new BpTree(4);

            Assert.AreEqual(0, _model.GetKeyCount()); // Beszúrás előtt 0 kulcs van a fában
            _currentAction = B_tree.Model.Action.Insert;
            _key = 5;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Beszúrjuk az 5-t
            Assert.AreEqual(1, _model.GetKeyCount()); // Beszúrás után 1 kulcs van a fában
            Assert.AreEqual(5, _model.Root.Keys[0]); // Beszúrás után a megfelelő kulcs van a gyökérben 
            Assert.AreEqual(true, _model.Root.IsLeaf); // A gyökér egyben levél is.
        }


        /// <summary>
        /// Beszúrás üres 5 fokú fába
        /// </summary>
        [TestMethod]
        public void InsertKeyEmptyTreeOrder5()
        {
            _model = new BpTree(5);
             
            Assert.AreEqual(0, _model.GetKeyCount()); // Beszúrás előtt 0 kulcs van a fában
            _currentAction = B_tree.Model.Action.Insert;
            _key = 5;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Beszúrjuk az 5-t
            Assert.AreEqual(1, _model.GetKeyCount()); // Beszúrás után 1 kulcs van a fában
            Assert.AreEqual(5, _model.Root.Keys[0]); // Beszúrás után a megfelelő kulcs van a gyökérben
            Assert.AreEqual(true, _model.Root.IsLeaf); // A gyökér egyben levél is.
        }


        /// <summary>
        /// Negatív szám beszúrása 4 fokú fába
        /// </summary>
        [TestMethod]
        public void InsertNegativeKeyTreeOrder4()
        {
            _model = new BpTree(4);

            Assert.AreEqual(0, _model.GetKeyCount()); // Beszúrás előtt 0 kulcs van a fában
            _currentAction = B_tree.Model.Action.Insert;
            _key = -2;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Megpróbáljuk beszúrni a -2-t
            Assert.AreEqual(0, _model.GetKeyCount()); // Beszúrás után 0 kulcs van a fában
        }


        /// <summary>
        /// Negatív szám beszúrása 5 fokú fába
        /// </summary>
        [TestMethod]
        public void InsertNegativeKeyTreeOrder5()
        {
            _model = new BpTree(5);

            Assert.AreEqual(0, _model.GetKeyCount()); // Beszúrás előtt 0 kulcs van a fában
            _currentAction = B_tree.Model.Action.Insert;
            _key = -2;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Megpróbáljuk beszúrni a -2-t
            Assert.AreEqual(0, _model.GetKeyCount()); // Beszúrás után 0 kulcs van a fában
        }


        /// <summary>
        /// 0 beszúrása 4 fokú fába
        /// </summary>
        [TestMethod]
        public void InsertZeroKeyTreeOrder4()
        {
            _model = new BpTree(4);

            Assert.AreEqual(0, _model.GetKeyCount()); // Beszúrás előtt 0 kulcs van a fában
            _currentAction = B_tree.Model.Action.Insert;
            _key = 0;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Megpróbáljuk beszúrni a 0-t
            Assert.AreEqual(0, _model.GetKeyCount()); // Beszúrás után 0 kulcs van a fában
        }


        /// <summary>
        /// 0 beszúrása 5 fokú fába
        /// </summary>
        [TestMethod]
        public void InsertZeroKeyTreeOrder5()
        {
            _model = new BpTree(5);

            Assert.AreEqual(0, _model.GetKeyCount()); // Beszúrás előtt 0 kulcs van a fában
            _currentAction = B_tree.Model.Action.Insert;
            _key = 0;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Megpróbáljuk beszúrni a 0-t
            Assert.AreEqual(0, _model.GetKeyCount()); // Beszúrás után 0 kulcs van a fában
        }


        /// <summary>
        /// Ugyanaz a kulcs újra beszúrása 4 fokú fába
        /// </summary>
        [TestMethod]
        public void InsertSameKeyTreeOrder4()
        {
            _model = new BpTree(4);

            Assert.AreEqual(0, _model.GetKeyCount()); // Beszúrás előtt 0 kulcs van a fában
            _currentAction = B_tree.Model.Action.Insert;
            _key = 5;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Beszúrjuk az 5-t
            Assert.AreEqual(1, _model.GetKeyCount()); // Beszúrás után 1 kulcs van a fában
            Assert.AreEqual(5, _model.Root.Keys[0]); // Beszúrás után a megfelelő kulcs van a gyökérben 

            _currentAction = B_tree.Model.Action.Insert;
            _key = 5;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Megpróbáljuk az 5-t újra beszúrni
            Assert.AreEqual(1, _model.GetKeyCount()); // Beszúrás után 1 kulcs van a fában
        }


        /// <summary>
        /// Ugyanaz a kulcs újra beszúrása 5 fokú fába
        /// </summary>
        [TestMethod]
        public void InsertSameKeyTreeOrder5()
        {
            _model = new BpTree(5);

            Assert.AreEqual(0, _model.GetKeyCount()); // Beszúrás előtt 0 kulcs van a fában
            _currentAction = B_tree.Model.Action.Insert;
            _key = 5;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Beszúrjuk az 5-t
            Assert.AreEqual(1, _model.GetKeyCount()); // Beszúrás után 1 kulcs van a fában
            Assert.AreEqual(5, _model.Root.Keys[0]); // Beszúrás után a megfelelő kulcs van a gyökérben

            _currentAction = B_tree.Model.Action.Insert;
            _key = 5;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Megpróbáljuk az 5-t újra beszúrni
            Assert.AreEqual(1, _model.GetKeyCount()); // Beszúrás után 1 kulcs van a fában
        }
        

        /// <summary>
        /// Beszúrás már elemekkel rendelkező 4 fokú fába
        /// </summary>
        [TestMethod]
        public void InsertKeyTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 1,5 };
            
            
            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1,5-t

            Assert.AreEqual(2, _model.GetKeyCount()); // 2 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Insert;
            _key = 4;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // beszúrjuk a 4-et
            Assert.AreEqual(3, _model.GetKeyCount()); // 3 kulcs van ekkor a fában
            Assert.AreEqual(4, _model.Root.Keys[1]); // Beszúrás sorrendje 1,5,4 , a 2. helyen a 4 van, rendezett a fa
        }


        /// <summary>
        /// Beszúrás már elemekkel rendelkező 5 fokú fába
        /// </summary>
        [TestMethod]
        public void InsertKeyTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 1, 5, 8 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            }// Előre beszúrtuk a 1,5,8-t

            Assert.AreEqual(3, _model.GetKeyCount()); // 3 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Insert;
            _key = 4;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } //Beszúrjuk a 4-et
            Assert.AreEqual(4, _model.GetKeyCount()); // 4 kulcs van ekkor a fában
            Assert.AreEqual(4, _model.Root.Keys[1]); // Beszúrás sorrendje 1,5,8,4 , a 2. helyen a 4 van, rendezett a fa
        }


        /// <summary>
        /// Beszúrás már elemekkel rendelkező 4 fokú fába, majd vágás (új gyökér)
        /// </summary>
        [TestMethod]
        public void InsertKeyAndSplitNewRootTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 1, 5 , 7 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1,5,7-t

            Assert.AreEqual(1, _model.MaxHeight); // a fa magassága ekkor 1 
            Assert.AreEqual(0, _model.Root.Children.Count); // A gyerekek száma 0 
            Assert.AreEqual(3, _model.GetKeyCount()); // 3 kulcs van a fában
            _currentAction = B_tree.Model.Action.Insert;
            _key = 4;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Beszúrjuk a 4-et
            Assert.AreEqual(4, _model.GetKeyCount()); // kulcsok száma már 4
            Assert.AreEqual(2, _model.Root.Children.Count); // a vágás során létrejött a 2 gyerek
            Assert.AreEqual(2, _model.MaxHeight); // a fa magassága nőt
            // megfelelő sorrendben vannak
            Assert.AreEqual(1, _model.Root.Children[0].Keys[0]); 
            Assert.AreEqual(4, _model.Root.Children[0].Keys[1]);
            Assert.AreEqual(5, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(7, _model.Root.Children[1].Keys[1]);
        }


        /// <summary>
        /// Beszúrás már elemekkel rendelkező 5 fokú fába, majd vágás (új gyökér)
        /// </summary>
        [TestMethod]
        public void InsertKeyAndSplitNewRootTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 1, 5, 7 ,9 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1,5,7,9-t

            Assert.AreEqual(1, _model.MaxHeight); // a fa magassága ekkor 1 
            Assert.AreEqual(0, _model.Root.Children.Count); // A gyerekek száma 0 
            Assert.AreEqual(4, _model.GetKeyCount()); // 4 kulcs van a fában
            _currentAction = B_tree.Model.Action.Insert;
            _key = 4;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Beszúrjuk a 4-et
            Assert.AreEqual(5, _model.GetKeyCount()); // kulcsok száma már 5
            Assert.AreEqual(2, _model.Root.Children.Count); // a vágás során létrejött a 2 gyerek
            Assert.AreEqual(2, _model.MaxHeight); // a fa magassága nőt
            // megfelelő sorrendben vannak
            Assert.AreEqual(1, _model.Root.Children[0].Keys[0]);
            Assert.AreEqual(4, _model.Root.Children[0].Keys[1]);
            Assert.AreEqual(5, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(7, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(9, _model.Root.Children[1].Keys[2]);
        }


        /// <summary>
        /// Beszúrás már elemekkel rendelkező 4 fokú fába, majd vágás
        /// </summary>
        [TestMethod]
        public void InsertKeyAndSplitTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 1, 3, 5, 7,11 ,15, 6};


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1, 3, 5, 7,11 ,15,6-t

            Assert.AreEqual(3, _model.Root.Children.Count); // A gyerekek száma 3
            _currentAction = B_tree.Model.Action.Insert;
            _key = 8;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Beszúrjuk a 8-at
            Assert.AreEqual(4, _model.Root.Children.Count); // a vágás során létrejött még1 gyerek
            // megfelelő sorrendben vannak
            Assert.AreEqual(5, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(6, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(7, _model.Root.Children[2].Keys[0]);
            Assert.AreEqual(8, _model.Root.Children[2].Keys[1]);

            //Hasítókulcsok
            Assert.AreEqual(5, _model.Root.Keys[0]);
            Assert.AreEqual(7, _model.Root.Keys[1]);
            Assert.AreEqual(11, _model.Root.Keys[2]);
            
        }


        /// <summary>
        /// Beszúrás már elemekkel rendelkező 5 fokú fába, majd vágás
        /// </summary>
        [TestMethod]
        public void InsertKeyAndSplitTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 1, 3, 5, 7, 11, 15, 19,6 ,8};


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1, 3, 5, 7, 11, 15, 19,6 ,8-t

            Assert.AreEqual(3, _model.Root.Children.Count); // A gyerekek száma 3
            _currentAction = B_tree.Model.Action.Insert;
            _key = 9;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Beszúrjuk a 9-et
            Assert.AreEqual(4, _model.Root.Children.Count); // a vágás során létrejött még1 gyerek
            // megfelelő sorrendben vannak
            Assert.AreEqual(5, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(6, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(7, _model.Root.Children[2].Keys[0]);
            Assert.AreEqual(8, _model.Root.Children[2].Keys[1]);
            Assert.AreEqual(9, _model.Root.Children[2].Keys[2]);

            //Hasítókulcsok
            Assert.AreEqual(5, _model.Root.Keys[0]);
            Assert.AreEqual(7, _model.Root.Keys[1]);
            Assert.AreEqual(11, _model.Root.Keys[2]);

        }

        /// <summary>
        /// Sok érték beszúrása után rendezett a fa
        /// </summary>
        [TestMethod]
        public void InsertKeysSortedTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 1, 3, 5, 7, 11, 15, 6 , 10, 20, 13 , 31 , 25, 27, 2, 40, 32,28,14};
            List<Int32> LeavesKeysList = new List<int>();


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1, 3, 5, 7, 11, 15, 6 , 10, 20, 13 , 31 , 25, 27, 2, 40, 32,28,14-t

            foreach(Node n in _model.GetLeaves())
            {
                LeavesKeysList.AddRange(n.Keys); //Levelek kulcsainak lekérdezése, majd a listába gyűjtése
            }
            _keyList.Sort(); // A beszúrt kulcsokat rendezzük
            for(Int32 i = 0; i < _keyList.Count;i++)
            {
                Assert.AreEqual(LeavesKeysList[i], _keyList[i]); // megegyezik a kulcsok sorrendje
            }
        }

        /// <summary>
        /// 9999 kulcs beszúrása
        /// </summary>
        [TestMethod]
        public void Insert9999KeysTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>();
            for(Int32 i = 1; i < 10000; i++)
            {
                _keyList.Add(i);
            }


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            }
            Assert.AreEqual(9999, _model.GetKeyCount());
        }
    }
}
