﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using B_tree.Model;

namespace B_TreeTest
{
    [TestClass]
    public class BpTreeDeleteTest
    {
        BpTree _model;
        List<Int32> _keyList;
        Int32 _key;
        B_tree.Model.Action _currentAction;

        /// <summary>
        /// Törlés üres 4 fokú fából
        /// </summary>
        [TestMethod]
        public void DeleteKeyEmptyTreeOrder4()
        {
            _model = new BpTree(4);

            Assert.AreEqual(0, _model.GetKeyCount()); // Törlés előtt 0 kulcs van a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 5;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük az 5-t
            Assert.AreEqual(0, _model.GetKeyCount()); // Törlés után 0 kulcs van a fában
        }

        /// <summary>
        /// Olyan elem törlése ami nincs benne a fában
        /// </summary>
        [TestMethod]
        public void DeleteKeyNotInTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 1, 5 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1,5-t

            Assert.AreEqual(2, _model.GetKeyCount()); // 2 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 4;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 4-et
            Assert.AreEqual(2, _model.GetKeyCount()); // 2 kulcs van ekkor is a fában
        }

        /// <summary>
        /// Negatív kulcs törlés a fából ( nem is lehet negatív érték benne, de teszteljük mi történik ha negatív értéket kap)
        /// </summary>
        [TestMethod]
        public void DeleteNegativeTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 1, 5 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1,5-t

            Assert.AreEqual(2, _model.GetKeyCount()); // 2 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = -2;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a -2-öt
            Assert.AreEqual(2, _model.GetKeyCount()); // 2 kulcs van ekkor is a fában
        }

        /// <summary>
        /// 0 kulcs törlés a fából ( nem is lehet 0 érték benne, de teszteljük mi történik ha 0 értéket kap)
        /// </summary>
        [TestMethod]
        public void DeleteZeroKeyTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 1, 5 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1,5-t

            Assert.AreEqual(2, _model.GetKeyCount()); // 2 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 0;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 0-t
            Assert.AreEqual(2, _model.GetKeyCount()); // 2 kulcs van ekkor is a fában
        }

        /// <summary>
        /// 1 elemű fából törlés
        /// </summary>
        [TestMethod]
        public void Delete1KeyInTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 1 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1-t

            Assert.AreEqual(1, _model.GetKeyCount()); // 1 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 1;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük az 1-et
            Assert.AreEqual(0, _model.GetKeyCount()); // 0 kulcs van ekkor a fában
        }

        /// <summary>
        /// Törlés amikor levélen több, mint minimum elem szám van.
        /// </summary>
        [TestMethod]
        public void DeleteMoreThanMinimumKeyTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 1,2,3,4,5,6 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1,2,3,4,5,6-t

            Assert.AreEqual(6, _model.GetKeyCount()); // 6 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 4;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 4-et
            Assert.AreEqual(5, _model.GetKeyCount()); // 5 kulcs van ekkor a fában

            
            Assert.AreEqual(1, _model.Root.Children[0].Keys[0]);
            Assert.AreEqual(2, _model.Root.Children[0].Keys[1]);
            Assert.AreEqual(3, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(5, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(6, _model.Root.Children[1].Keys[2]);

            _currentAction = B_tree.Model.Action.Delete;
            _key = 5;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük az 5-öt

            Assert.AreEqual(4, _model.GetKeyCount()); // 4 kulcs van ekkor a fában

            Assert.AreEqual(1, _model.Root.Children[0].Keys[0]);
            Assert.AreEqual(2, _model.Root.Children[0].Keys[1]);
            Assert.AreEqual(3, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(6, _model.Root.Children[1].Keys[1]);
        }

        /// <summary>
        /// Törlés minimum levélről, jobb testvértől kulcsot kap. fokszám4
        /// </summary>
        [TestMethod]
        public void DeleteMinimumLeafGetKeyRightTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 1, 2, 3, 4, 5, 6 , 7};


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1,2,3,4,5,6,7-t


            //ekkor az adott levélen a (3,4) van
            Assert.AreEqual(3, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(4, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(3, _model.Root.Children[2].Keys.Count); // jobb testvérnek 3 kulcsa van
            Assert.AreEqual(5, _model.Root.Keys[1]); //hasítókulcs ekkor az 5


            Assert.AreEqual(7, _model.GetKeyCount()); // 7 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 4;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 4-et
            Assert.AreEqual(6, _model.GetKeyCount()); // 6 kulcs van ekkor a fában

            //ekkor a levélen a (3,5) van
            Assert.AreEqual(3, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(5, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(2, _model.Root.Children[2].Keys.Count); // jobb testvérnek 2 kulcsa van
            Assert.AreEqual(6, _model.Root.Keys[1]); //hasítókulcs ekkor a 6


        }


        /// <summary>
        /// Törlés minimum levélről, jobb testvértől kulcsot kap. fokszám5
        /// </summary>
        [TestMethod]
        public void DeleteMinimumLeafGetKeyRightTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 1, 2, 3, 4, 5, 6, 7 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1,2,3,4,5,6,7-t


            //ekkor az adott levélen a (3,4) van
            Assert.AreEqual(3, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(4, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(3, _model.Root.Children[2].Keys.Count); // jobb testvérnek 3 kulcsa van
            Assert.AreEqual(5, _model.Root.Keys[1]); //hasítókulcs ekkor az 5


            Assert.AreEqual(7, _model.GetKeyCount()); // 7 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 4;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 4-et
            Assert.AreEqual(6, _model.GetKeyCount()); // 6 kulcs van ekkor a fában

            //ekkor a levélen a (3,5) van
            Assert.AreEqual(3, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(5, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(2, _model.Root.Children[2].Keys.Count); // jobb testvérnek 2 kulcsa van
            Assert.AreEqual(6, _model.Root.Keys[1]); //hasítókulcs ekkor a 6


        }
        /// <summary>
        /// Törlés minimum levélről, bal testvértől kulcsot kap. fokszám 4
        /// </summary>
        [TestMethod]
        public void DeleteMinimumLeafGetKeyLeftTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 1, 3, 5, 7, 9, 10, 2 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1, 3, 5, 7, 9, 10, 2-t


            //ekkor az adott levélen a (5,7) van
            Assert.AreEqual(5, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(7, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(3, _model.Root.Children[0].Keys.Count); // bal testvérnek 3 kulcsa van
            Assert.AreEqual(5, _model.Root.Keys[0]); //hasítókulcs ekkor az 5


            Assert.AreEqual(7, _model.GetKeyCount()); // 7 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 5;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük az 5-öt
            Assert.AreEqual(6, _model.GetKeyCount()); // 6 kulcs van ekkor a fában

            //ekkor a levélen a (3,7) van
            Assert.AreEqual(3, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(7, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(2, _model.Root.Children[2].Keys.Count); // jobb testvérnek 2 kulcsa van
            Assert.AreEqual(3, _model.Root.Keys[0]); //hasítókulcs ekkor a 3


        }

        /// <summary>
        /// Törlés minimum levélről, bal testvértől kulcsot kap. fokszám 5
        /// </summary>
        [TestMethod]
        public void DeleteMinimumLeafGetKeyLeftTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 1, 3, 5, 7, 9, 10, 11, 2 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1, 3, 5, 7, 9, 10,11, 2-t
            _currentAction = B_tree.Model.Action.Delete;
            _key = 9;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük az 9-et


            //ekkor az adott levélen a (5,7) van
            Assert.AreEqual(5, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(7, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(3, _model.Root.Children[0].Keys.Count); // bal testvérnek 3 kulcsa van
            Assert.AreEqual(5, _model.Root.Keys[0]); //hasítókulcs ekkor az 5


            Assert.AreEqual(7, _model.GetKeyCount()); // 7 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 5;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük az 5-öt
            Assert.AreEqual(6, _model.GetKeyCount()); // 6 kulcs van ekkor a fában

            //ekkor a levélen a (3,7) van
            Assert.AreEqual(3, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(7, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(2, _model.Root.Children[2].Keys.Count); // jobb testvérnek 2 kulcsa van
            Assert.AreEqual(3, _model.Root.Keys[0]); //hasítókulcs ekkor a 3


        }

        /// <summary>
        /// Törlés minimum levélről, összeolvad jobb testvérrel. Fokszám 4
        /// </summary>
        [TestMethod]
        public void DeleteMinimumLeafMergeRightTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 1, 3, 5, 7, 9, 10 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1, 3, 5, 7, 9, 10-t


            //ekkor az adott levélen a (5,7) van
            Assert.AreEqual(5, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(7, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(3, _model.Root.Children.Count); // 3 gyerek van
            Assert.AreEqual(2, _model.Root.Keys.Count); //2 hasító kulcs van


            Assert.AreEqual(6, _model.GetKeyCount()); // 6 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 5;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük az 5-öt
            Assert.AreEqual(5, _model.GetKeyCount()); // 5 kulcs van ekkor a fában

            //ekkor a levélen a (7,9,10) van
            Assert.AreEqual(7, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(9, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(10, _model.Root.Children[1].Keys[2]);
            Assert.AreEqual(2, _model.Root.Children.Count); // 2 gyerek van már csak
            Assert.AreEqual(1, _model.Root.Keys.Count); //1 hasítókulcs
        }

        /// <summary>
        /// Törlés minimum levélről, összeolvad jobb testvérrel. Fokszám 5
        /// </summary>
        [TestMethod]
        public void DeleteMinimumLeafMergeRightTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 1, 3, 5, 7, 9, 10, 11 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1, 3, 5, 7, 9, 10,11-t

            _currentAction = B_tree.Model.Action.Delete;
            _key = 10;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük az 10-et

            //ekkor az adott levélen a (5,7) van
            Assert.AreEqual(5, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(7, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(3, _model.Root.Children.Count); // 3 gyerek van
            Assert.AreEqual(2, _model.Root.Keys.Count); //2 hasító kulcs van


            Assert.AreEqual(6, _model.GetKeyCount()); // 6 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 5;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük az 5-öt
            Assert.AreEqual(5, _model.GetKeyCount()); // 5 kulcs van ekkor a fában

            //ekkor a levélen a (7,9,10) van
            Assert.AreEqual(7, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(9, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(11, _model.Root.Children[1].Keys[2]);
            Assert.AreEqual(2, _model.Root.Children.Count); // 2 gyerek van már csak
            Assert.AreEqual(1, _model.Root.Keys.Count); //1 hasítókulcs
        }

        /// <summary>
        /// Törlés minimum levélről, összeolvad bal testvérrel. Fokszám 4
        /// </summary>
        [TestMethod]
        public void DeleteMinimumLeafMergeLeftTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 1, 3, 5, 7, 9, 10 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1, 3, 5, 7, 9, 10-t


            //ekkor az adott levélen a (9,10) van
            Assert.AreEqual(9, _model.Root.Children[2].Keys[0]);
            Assert.AreEqual(10, _model.Root.Children[2].Keys[1]);
            Assert.AreEqual(3, _model.Root.Children.Count); // 3 gyerek van
            Assert.AreEqual(2, _model.Root.Keys.Count); //2 hasító kulcs van


            Assert.AreEqual(6, _model.GetKeyCount()); // 6 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 9;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük az 5-öt
            Assert.AreEqual(5, _model.GetKeyCount()); // 5 kulcs van ekkor a fában

            //ekkor a levélen a (7,9,10) van
            Assert.AreEqual(5, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(7, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(10, _model.Root.Children[1].Keys[2]);
            Assert.AreEqual(2, _model.Root.Children.Count); // 2 gyerek van már csak
            Assert.AreEqual(1, _model.Root.Keys.Count); //1 hasítókulcs
        }

        /// <summary>
        /// Törlés minimum levélről, összeolvad bal testvérrel. Fokszám 5
        /// </summary>
        [TestMethod]
        public void DeleteMinimumLeafMergeLeftTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 1, 3, 5, 7, 9, 10, 11 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 1, 3, 5, 7, 9, 10,11-t

            _currentAction = B_tree.Model.Action.Delete;
            _key = 11;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 11-et


            //ekkor az adott levélen a (9,10) van
            Assert.AreEqual(9, _model.Root.Children[2].Keys[0]);
            Assert.AreEqual(10, _model.Root.Children[2].Keys[1]);
            Assert.AreEqual(3, _model.Root.Children.Count); // 3 gyerek van
            Assert.AreEqual(2, _model.Root.Keys.Count); //2 hasító kulcs van


            Assert.AreEqual(6, _model.GetKeyCount()); // 6 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 9;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük az 9-t
            Assert.AreEqual(5, _model.GetKeyCount()); // 5 kulcs van ekkor a fában

            //ekkor a levélen a (7,9,10) van
            Assert.AreEqual(5, _model.Root.Children[1].Keys[0]);
            Assert.AreEqual(7, _model.Root.Children[1].Keys[1]);
            Assert.AreEqual(10, _model.Root.Children[1].Keys[2]);
            Assert.AreEqual(2, _model.Root.Children.Count); // 2 gyerek van már csak
            Assert.AreEqual(1, _model.Root.Keys.Count); //1 hasítókulcs
        }


        /// <summary>
        /// Törlés minimum csúcsról, bal testvértől kap gyereket. fokszám 4
        /// </summary>
        [TestMethod]
        public void DeleteMinimumNodeGetLeftTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 4,8,12,16,20,24,28,32,36,40};


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 4,8,12,16,20,24,28,32,36,40-t


            Assert.AreEqual(3, _model.Root.Children[0].Children.Count); // bal testvérnek 3 gyereke van
            Assert.AreEqual(28, _model.Root.Keys[0]); //hasítókulcs ekkor a 28 a gyökérben (nagyszülőben)

            Assert.AreEqual(10, _model.GetKeyCount()); // 10 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 28;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 28-at
            Assert.AreEqual(9, _model.GetKeyCount()); // 9 kulcs van ekkor a fában

            Assert.AreEqual(2, _model.Root.Children[0].Children.Count); // bal testvérnek 2 gyereke van már
            Assert.AreEqual(20, _model.Root.Keys[0]); //hasítókulcs ekkor a 20 már a gyökérben (nagyszülőben)

        }

        /// <summary>
        /// Törlés minimum csúcsról, bal testvértől kap gyereket. fokszám 5
        /// </summary>
        [TestMethod]
        public void DeleteMinimumNodeGetLeftTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 4, 8, 12, 16, 20, 24, 28, 32, 36, 40,44,48,52 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a  4, 8, 12, 16, 20, 24, 28, 32, 36, 40,44,48,52-t
            _keyList = new List<Int32>() { 36,40,44 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Delete;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // TÖröljük  36,40,44 -t

            Assert.AreEqual(3, _model.Root.Children[0].Children.Count); // bal testvérnek 3 gyereke van
            Assert.AreEqual(28, _model.Root.Keys[0]); //hasítókulcs ekkor a 28 a gyökérben (nagyszülőben)

            Assert.AreEqual(10, _model.GetKeyCount()); // 10 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 48;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 48-at
            Assert.AreEqual(9, _model.GetKeyCount()); // 9 kulcs van ekkor a fában

            Assert.AreEqual(2, _model.Root.Children[0].Children.Count); // bal testvérnek 2 gyereke van már
            Assert.AreEqual(20, _model.Root.Keys[0]); //hasítókulcs ekkor a 20 már a gyökérben (nagyszülőben)

        }

        /// <summary>
        /// Törlés minimum csúcsról, jobb testvértől kap gyereket. fokszám 4
        /// </summary>
        [TestMethod]
        public void DeleteMinimumNodeGetRightTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 4, 8, 12, 16, 20, 24, 28, 32, 36, 40 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 4,8,12,16,20,24,28,32,36,40-t

            _currentAction = B_tree.Model.Action.Delete;
            _key = 28;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 28-at

            _currentAction = B_tree.Model.Action.Insert;
            _key = 44;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Beszúrjuk a 44-et


            Assert.AreEqual(3, _model.Root.Children[1].Children.Count); // jobb testvérnek 3 gyereke van
            Assert.AreEqual(20, _model.Root.Keys[0]); //hasítókulcs ekkor a 20 a gyökérben (nagyszülőben)

            Assert.AreEqual(10, _model.GetKeyCount()); // 10 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 12;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 12-öt
            Assert.AreEqual(9, _model.GetKeyCount()); // 9 kulcs van ekkor a fában

            Assert.AreEqual(2, _model.Root.Children[1].Children.Count); // jobb testvérnek 2 gyereke van már
            Assert.AreEqual(28, _model.Root.Keys[0]); //hasítókulcs ekkor a 28 már a gyökérben (nagyszülőben)

        }

        /// <summary>
        /// Törlés minimum csúcsról, jobb testvértől kap gyereket. fokszám 5
        /// </summary>
        [TestMethod]
        public void DeleteMinimumNodeGetRightTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 4, 8, 12, 16, 20, 24, 28, 32, 36, 40 ,44 ,48,52 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 4, 8, 12, 16, 20, 24, 28, 32, 36, 40 ,44 ,48,52-t
            _keyList = new List<Int32>() { 36,40,44,48 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Delete;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Töröltük a 36,40,44,48-at

            _keyList = new List<Int32>() { 36, 40};


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Töröltük a 36,40,44,48-at


            Assert.AreEqual(3, _model.Root.Children[1].Children.Count); // jobb testvérnek 3 gyereke van
            Assert.AreEqual(20, _model.Root.Keys[0]); //hasítókulcs ekkor a 20 a gyökérben (nagyszülőben)

            Assert.AreEqual(11, _model.GetKeyCount()); // 11 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 12;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 12-t
            Assert.AreEqual(10, _model.GetKeyCount()); // 10 kulcs van ekkor a fában

            Assert.AreEqual(2, _model.Root.Children[1].Children.Count); // jobb testvérnek 2 gyereke van már
            Assert.AreEqual(28, _model.Root.Keys[0]); //hasítókulcs ekkor a 28 már a gyökérben (nagyszülőben)

        }

        /// <summary>
        /// Törlés minimum csúcsról, Összeolvad jobbra. fokszám 4
        /// </summary>
        [TestMethod]
        public void DeleteMinimumNodeMergeRightTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 4, 8, 12, 16, 20, 24, 28, 32, 36, 40,44,48,52,56,60,64 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 4, 8, 12, 16, 20, 24, 28, 32, 36, 40,44,48,52,56,60,64-t

            _keyList = new List<Int32>() { 36,40, 20, 24 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Delete;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } //Töröljük 36,40, 20, 24 

            Assert.AreEqual(3, _model.Root.Children.Count); //Gyökérnek (nagyszülő) 3 gyereke van ekkor

            Assert.AreEqual(12, _model.GetKeyCount()); // 12 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 44;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 44-et
            Assert.AreEqual(11, _model.GetKeyCount()); // 11 kulcs van ekkor a fában

            Assert.AreEqual(2, _model.Root.Children.Count); //Gyökérnek (nagyszülő) 2 gyereke van ekkor csak

        }

        /// <summary>
        /// Törlés minimum csúcsról, Összeolvad balra. fokszám 4
        /// </summary>
        [TestMethod]
        public void DeleteMinimumNodeMergeLeftTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 64 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 4, 8, 12, 16, 20, 24, 28, 32, 36, 40,44,48,52,56,60,64-t

            _keyList = new List<Int32>() { 36, 40, 20, 24 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Delete;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } //Töröljük 36,40, 20, 24 

            Assert.AreEqual(3, _model.Root.Children.Count); //Gyökérnek (nagyszülő) 3 gyereke van ekkor

            Assert.AreEqual(12, _model.GetKeyCount()); // 12 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 52;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 52-t
            Assert.AreEqual(11, _model.GetKeyCount()); // 11 kulcs van ekkor a fában

            Assert.AreEqual(2, _model.Root.Children.Count); //Gyökérnek (nagyszülő) 2 gyereke van ekkor csak

        }

        /// <summary>
        /// Fa magassága csökken. fokszám 4
        /// </summary>
        [TestMethod]
        public void DeleteMinimumNodeHeightDecreaseTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>() { 4, 8, 12, 16, 20, 24, 28, 32, 36, 40  };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a  4, 8, 12, 16, 20, 24, 28, 32, 36, 40 -t

            _keyList = new List<Int32>() { 20, 24 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Delete;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } //Töröljük 20, 24 

            Assert.AreEqual(3, _model.MaxHeight); //A famagassága (mivel gyökért már 1-nek vesszük) 3

            Assert.AreEqual(8, _model.GetKeyCount()); // 12 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 12;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 12-t
            Assert.AreEqual(7, _model.GetKeyCount()); // 11 kulcs van ekkor a fában

            Assert.AreEqual(2, _model.MaxHeight); //A famagassága (mivel gyökért már 1-nek vesszük)2

        }

        /// <summary>
        /// Törlés minimum csúcsról, Összeolvad jobbra. fokszám 5 és a famagassága egyben csökken
        /// </summary>
        [TestMethod]
        public void DeleteMinimumNodeMergeRightTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48,52 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 4, 8, 12, 16, 20, 24, 28, 32, 36, 40,44,48,52-t

            _keyList = new List<Int32>() { 36, 40,44 , 20, 24 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Delete;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } //Töröljük 36, 40,44 , 20, 24

            Assert.AreEqual(3, _model.MaxHeight); //A famagassága (mivel gyökért már 1-nek vesszük)3

            Assert.AreEqual(8, _model.GetKeyCount()); // 8 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 12;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 12-t
            Assert.AreEqual(7, _model.GetKeyCount()); // 7 kulcs van ekkor a fában

            Assert.AreEqual(2, _model.MaxHeight); //A famagassága (mivel gyökért már 1-nek vesszük)2

        }

        /// <summary>
        /// Törlés minimum csúcsról, Összeolvad balra. fokszám 5 és a famagassága egyben csökken
        /// </summary>
        [TestMethod]
        public void DeleteMinimumNodeMergeLeftTreeOrder5()
        {
            _model = new BpTree(5);
            _keyList = new List<Int32>() { 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } // Előre beszúrtuk a 4, 8, 12, 16, 20, 24, 28, 32, 36, 40,44,48,52-t

            _keyList = new List<Int32>() { 36, 40, 44, 20, 24 };


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Delete;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            } //Töröljük 36, 40,44 , 20, 24

            Assert.AreEqual(3, _model.MaxHeight); //A famagassága (mivel gyökért már 1-nek vesszük)3

            Assert.AreEqual(8, _model.GetKeyCount()); // 8 kulcs van ekkor a fában
            _currentAction = B_tree.Model.Action.Delete;
            _key = 28;
            while (_model.ActionController(_currentAction, _key))
            {
                if (_currentAction != B_tree.Model.Action.DoAction)
                    _currentAction = B_tree.Model.Action.DoAction;
            } // Töröljük a 28-t
            Assert.AreEqual(7, _model.GetKeyCount()); // 7 kulcs van ekkor a fában

            Assert.AreEqual(2, _model.MaxHeight); //A famagassága (mivel gyökért már 1-nek vesszük)2

        }
        /// <summary>
        /// Sok kulcs törlése
        /// </summary>
        [TestMethod]
        public void Delete1000KeysTreeOrder4()
        {
            _model = new BpTree(4);
            _keyList = new List<Int32>();
            for (Int32 i = 1; i < 3000; i++)
            {
                _keyList.Add(i);
            }


            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Insert;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            }
            Assert.AreEqual(2999, _model.GetKeyCount());

            _keyList = new List<Int32>();
            for (Int32 i = 1; i < 1001; i++)
            {
                _keyList.Add(i);
            }

            for (Int32 i = 0; i < _keyList.Count; i++)
            {
                _currentAction = B_tree.Model.Action.Delete;
                _key = _keyList[i];
                while (_model.ActionController(_currentAction, _key))
                {
                    if (_currentAction != B_tree.Model.Action.DoAction)
                        _currentAction = B_tree.Model.Action.DoAction;
                }
            }
            Assert.AreEqual(1999, _model.GetKeyCount());
        }
    }
}
